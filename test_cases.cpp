/* .cpp

   Copyright (c) 2017 Grant Rostig. All rights reserved. Use by written permission only, evidenced with blue ink on paper or a verifyable (pgp) email interaction with Grant Rostig.
   Permission is granted to use these files during and in support of CppMSG meetup group events and projects.

   Inspired by: -

   Shows how to:
   -
   -
   -
*/
#include <random>
#include "test_cases.h"
#include "main_gnuplot.h"
#include "utils.h"
#include "d_ucs.h"
#include "ml_utils.h"
#include "ai_model.h"
#include "ivector_of_labels.h"
#include "ifeature_matrix.h"
#include "features_n_label.h"

void test_iai_model_data_procedure(Program_options& program_options) {
    Matrix_data  matrix_data;
    Vec_of_vectors  vec_of_vectors;

    Ivector_of_labels<double> * my_tfl_vect_of_double = Ivector_of_labels<double>::create(Numerical_library::eigen);


}

void test_graphing_procedure(Program_options& program_options) {
    assert (program_options.vector_size > 1); // don't think we want to do regression with only the constant feature?  todo
    std::random_device my_random_sample_device;
    std::mt19937 my_random_sample_engine(my_random_sample_device());

    Gnuplot plot_canvas;

    for ( size_t trial_number=1; trial_number <= program_options.num_desired_trials; ++trial_number ) {
        D_ucs d_ucs = prepare_data_samples(program_options);

//        std::vector<double> interval{program_options.sample_space_lower_bound, program_options.sample_space_upper_bound};
//        std::vector<double> density_weight{1, 0, 1}; // huh??? TODO
//        std::piecewise_constant_distribution<> my_integer_distribution (interval.begin(), interval.end(), density_weight.begin());
        std::uniform_int_distribution<int> my_integer_sample_distribution(0, d_ucs.size()-1);

        W_lcb f_coefficients(program_options.vector_size);  // must be set below! probably to ones.
        E_lcb f_exponents = VectorXd::Ones(program_options.vector_size); // typically ones.

        /* In the case of linear models without transformation.
           For our starting unknown f(), the Decision Boundary equation of the linear boundary hyperplane.
           We will do the initial marking of our samples with this hyperplane.
           y = f(x1,x2) = m0*x0 + m1*x1 + m2*x2, where x0=1 which makes the m0*x0 term the line intercept constant.
           Example 1 f() is:
           boundary line:       m2*y =          + -m1*x     + -m0*0.01  ie. the "slope intercept form" y = ax + C form
           Green region is:     m2*y >=         + -m1x      + -m0*0.01
                                0 >=    -m2*y   + -m1*x     + -m0*0.01
                                0 <=    +m2*y   - -m1*x     - -m0*0.01

                                + m0*x0 + m1*x1 + m2*x2 >= 0  where x0=1  ie. the MODIFIED "standard or general form", which is actually ax + by = c

                                ( (m0, m1, m2) dot (1, x1, x2) ) >= 0

           Example 2 f() is:
           boundary line:       y =  -+sqrt(- x**2 + 0.6)
           Green region is:     y >= -+sqrt(- x**2 + 0.6)
                                y**2 >=     - x**2 + 0.6
                                0 >= - y**2 - x**2 + 0.6
                                0 <= + y**2 + x**2 - 0.6
                                + -1*0.6 + 1*x**2 + 1*y**2 >=0     ** = "to the power of"
                                + m0*x0**e0 + m1*x1**e1 + m2*x2**e2 >= 0
           ( (m0, m1, m2) dot (1, x1, x2) ) >= 0

           Set characteristics of our unknown f() ie. coefficients and exponents
        */

        f_coefficients = select_random_unknown_boundary(program_options);
        std::for_each(d_ucs.begin(), d_ucs.end(), label_random_sample{f_coefficients, f_exponents});
        plot_points_and_boundary( plot_canvas, "Test Case of Labeled Points with unknown f() illustrated", d_ucs, true, f_coefficients, f_exponents, false, program_options );
    }
    return;
}
