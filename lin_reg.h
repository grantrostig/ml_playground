/* .h

   Copyright (c) 2017 Grant Rostig. All rights reserved. Use by written permission only, evidenced with blue ink on paper or a verifyable (pgp) email interaction with Grant Rostig.
   Permission is granted to use these files during and in support of CppMSG meetup group events and projects.

   Inspired by: Caltech LFD

   Shows how to:
   -
   -
   -

// #include "../gsl/gsl"   // Standard C++ Foundation: C++14 Guidelines Support Library

// Variable naming convention to correspond to mathematical use of fonts.
// lc=Lower Case, uc=Upper Case, s=script (ie.not block characters), b=bold
*/
#ifndef lin_reg_H
#define lin_reg_H
#include "main_eigen.h"
#include "d_ucs.h"
#include "data_samples_matrix.h"
#include "utils.h"

size_t inter_vector_label_discrepancies(D_ucs const& d_ucs,
                                        W_lcb const& w_lcb_weights,
                                        C_lcb const& f_coefficients,
                                        E_lcb const& f_exponents );

size_t inter_vector_label_discrepancies(D_ucs const& d_ucs,
                                        D_ucs const& d_ucs_transformed,
                                        C_lcb const& f_coefficients,
                                        E_lcb const& f_exponents,
                                        W_lcb const& w_lcb_weights );

size_t vector_vs_label_discrepancies(D_ucs & d_ucs, W_lcb const& w_lcb_weights);

double test_probability_of_errors(const C_lcb& f_coefficients,  // is used but probably incorrectly.
                                  const W_lcb& w_lcb_weights,
                                  const E_lcb& f_exponents,
                                  const Program_options& program_options );  // used by lin_reg_to_perceptron

D_ucs transform_exponentiate_x0_x1_x2( D_ucs const& d_ucs, E_lcb const& f_exponents, Program_options const& program_options);

D_ucs transform_2nd_ord_polynm_in_x1_x2( D_ucs const& d_ucs, Program_options const& program_options);

W_lcb ordinary_least_squares(const D_ucs& d_ucs,
                             const Program_options& program_options); // Linear regression by 'analytic solution' using 'ordinary least squares' algorithm AKA 'normal equation'

W_lcb ordinary_least_squares2(const D_ucs& d_ucs,
                             const Program_options& program_options); // Linear regression by 'analytic solution' using 'ordinary least squares' algorithm AKA 'normal equation'

//============= Various Linear Regression Procedure Variants ===========================

void lin_reg_least_sq_procedure(Program_options const& program_options);

void lin_reg_4class_transformed_procedure(const Program_options& program_options);

void lin_reg_4classification_procedure(const Program_options& program_options);

void lin_reg_4c_to_perceptron_procedure(const Program_options& program_options);

void lin_reg_gradient_descent_procedure(const Program_options& program_options);

void lin_reg_stoch_grad_descent_procedure(const Program_options& program_options);

#endif // lin_reg_H
