/* .cpp

   Copyright (c) 2017 Grant Rostig. All rights reserved. Use by written permission only, evidenced with blue ink on paper or a verifyable (pgp) email interaction with Grant Rostig.
   Permission is granted to use these files during and in support of CppMSG meetup group events and projects.

   Inspired by: -

   Shows how to:
   -
   -
   -

   https://stackoverflow.com/questions/148540/creating-my-own-iterators
   http://www.drdobbs.com/custom-containers-iterators-for-stl-fri/184401929
   https://accu.org/index.php/journals/1527
   https://accu.org/index.php/journals/389
*/
#include "d_ucs.h"
//#include "main_cppheaders.h"
#include "main_eigen.h"
#include "X_lcb_sample.h"
#include "random_toolkit.h"

D_ucs::D_ucs(Program_options const& program_options)
//                : x_lcb_samples(0)  // does this mess up my reserve? deleted function if used?? added this because of an effective c++ warning todo
{
    x_lcb_samples.reserve( program_options.n_uc_num_samples );
    for (size_t i=0; i < program_options.n_uc_num_samples; ++i) {
        x_lcb_samples.emplace_back(program_options);  // create and push_back one object of type X_lcb_sample constructed with arguments // c++14 placement new.
    };
}

size_t D_ucs::size() {
    return x_lcb_samples.size();
}

size_t D_ucs::size() const {
    return x_lcb_samples.size();
}

X_lcb_sample D_ucs::get(size_t& index) const {
    return x_lcb_samples[index];
}

void D_ucs::set(size_t& index, X_lcb_sample& value) {
    x_lcb_samples[index] = value;
}

X_lcb_sample& D_ucs::operator[](size_t index) {
    assert(index < x_lcb_samples.size());
    return x_lcb_samples[index];
}

const X_lcb_sample& D_ucs::operator[](size_t index) const {  // grostig const member function promises not to mutate its owning object. I always need both?? how should this be different because it is const? todo
    assert(index < x_lcb_samples.size());
    return x_lcb_samples[index];
}

//std::vector<X_lcb_sample>::iterator D_ucs2::add() {
//    return
//}

std::vector<X_lcb_sample>::iterator D_ucs::erase(std::vector<X_lcb_sample>::iterator begin) {
    return x_lcb_samples.erase(begin, x_lcb_samples.end());
}

std::vector<X_lcb_sample>::iterator D_ucs::erase(std::vector<X_lcb_sample>::iterator begin, std::vector<X_lcb_sample>::iterator end) {
    return x_lcb_samples.erase(begin, end);
}

std::vector<X_lcb_sample>::iterator D_ucs::begin() {
    return x_lcb_samples.begin();
}

std::vector<X_lcb_sample>::const_iterator D_ucs::begin() const {
    return x_lcb_samples.begin();
}

//std::vector< X_lcb_sample>::iterator D_ucs2::cbegin()  {
//    return x_lcb_samples.cbegin();  // todo ../ml_playground/d_ucs.cpp:75:32: error: could not convert ‘((D_ucs2*)this)->D_ucs2::x_lcb_samples.std::vector<X_lcb_sample>::cbegin()’ from ‘std::vector<X_lcb_sample>::const_iterator {aka __gnu_cxx::__normal_iterator<const X_lcb_sample*, std::vector<X_lcb_sample> >}’ to ‘std::vector<X_lcb_sample>::iterator {aka __gnu_cxx::__normal_iterator<X_lcb_sample*, std::vector<X_lcb_sample> >}’
//}

std::vector<X_lcb_sample>::iterator D_ucs::end() {
    return x_lcb_samples.end();
}

std::vector<X_lcb_sample>::const_iterator D_ucs::end() const {
    return x_lcb_samples.end();
}

void D_ucs::add_noise_11classifer(double const& per_cent_noise) {  // add noise where labels are +1/-1 by flipping their sign
    double change = static_cast<double>( x_lcb_samples.size() ) * (per_cent_noise/100.0);
    double num_to_make_noisy     = lround(change);
    std::set<size_t> random_indexes = {};
    do {        // add non duplicates to set.
        size_t sample = pick_a_number_t( 0ul, x_lcb_samples.size() );
        random_indexes.insert(sample);
        BOOST_LOG_TRIVIAL(trace) << "add_noise() past insert: " << random_indexes.size();
    } while ( random_indexes.size() < num_to_make_noisy );
    for (auto i:random_indexes ) {
        x_lcb_samples[i].y_true_sample_label *= -1; // flip the sign, presuming we are dealing with +1,-1  todo : generalize
    }
}

// *** D_ucs2::D_ucs_iterator *** methods ***

//D_ucs2::D_ucs_iterator& D_ucs2::D_ucs_iterator::operator=() {
//    current_iterator_pointer = current_iterator_pointer; return *this;
//}

//std::vector<X_lcb_sample>::iterator D_ucs2::D_ucs_iterator::operator*() const {
//    return current_d_ucs_iterator;
//}

//X_lcb_sample& D_ucs2::D_ucs_iterator::operator->() const {
// //    return current_iterator_pointer;  // compile error todo
//}

//D_ucs2::D_ucs_iterator& D_ucs2::D_ucs_iterator::operator++() {
//    current_d_ucs_iterator = current_d_ucs_iterator++; return *this;
//}

//D_ucs2::D_ucs_iterator D_ucs2::D_ucs_iterator::operator++(int rhs) {  // postfix  // is rhs a good name?
//    int temp = rhs;
//    current_d_ucs_iterator = current_d_ucs_iterator++; return *this;
//}

//D_ucs2::D_ucs_iterator& D_ucs2::D_ucs_iterator::operator--() {
//    current_d_ucs_iterator = current_d_ucs_iterator--; return *this;
//}

//Ducs2::D_ucs_iterator D_ucs2::D_ucs_iterator::operator--(int rhs) { // postfix
//    current_d_ucs_iterator = current_d_ucs_iterator--; return *this;
//}

//bool D_ucs2::D_ucs_iterator::operator==(const D_ucs2::D_ucs_iterator& rhs) const {
//    return current_d_ucs_iterator == rhs.current_d_ucs_iterator;
//}

//bool D_ucs2::D_ucs_iterator::operator!=(const D_ucs2::D_ucs_iterator& rhs) const {
//    return current_d_ucs_iterator != rhs.current_d_ucs_iterator;
//}

//bool D_ucs2::D_ucs_iterator::operator<(const D_ucs2::D_ucs_iterator& rhs) const {
//    return current_d_ucs_iterator != rhs.current_d_ucs_iterator;
//}

//bool D_ucs2::D_ucs_iterator::operator<=(const D_ucs2::D_ucs_iterator& rhs) const {
//    return current_d_ucs_iterator != rhs.current_d_ucs_iterator;
//}

//bool D_ucs2::D_ucs_iterator::operator>(const D_ucs2::D_ucs_iterator& rhs) const {
//    return current_d_ucs_iterator != rhs.current_d_ucs_iterator;
//}

//bool D_ucs2::D_ucs_iterator::operator>=(const D_ucs2::D_ucs_iterator& rhs) const {
//    return current_d_ucs_iterator != rhs.current_d_ucs_iterator;
//}

