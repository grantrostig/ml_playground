/* .cpp

   Copyright (c) 2017 Grant Rostig. All rights reserved. Use by written permission only, evidenced with blue ink on paper or a verifyable (pgp) email interaction with Grant Rostig.
   Permission is granted to use these files during and in support of CppMSG meetup group events and projects.

   Inspired by: -

   Shows how to:
   -
   -
   -
*/
#include "data_sample.h"
#include "utils.h"
#include "ml_utils.h"
#include "random_toolkit.h"

Data_sample::Data_sample(Program_options const& program_options)
{
    x_lcb.resize(program_options.vector_size);  // todo MAGIC NUMBER - basically we are only demonstrating single regression, document this elsewhere also.
    x_lcb << 1.0, random_sample_point(program_options) ;  // features of one sample with range [-1:+1] in double
}

//Data_sample_label::Data_sample_label(Program_options const& program_options)
//{
//    double y_true_sample_label = grostig::pick_a_number_t(program_options.sample_space_lower_bound, program_options.sample_space_upper_bound);
//    double h_temp_sample_label = 0;
//}


