/* .cpp

   Copyright (c) 2017 Grant Rostig. All rights reserved. Use by written permission only, evidenced with blue ink on paper or a verifyable (pgp) email interaction with Grant Rostig.
   Permission is granted to use these files during and in support of CppMSG meetup group events and projects.

   Inspired by: -

   Shows how to:
   -
   -
   -
*/

#include "ml_utils.h"
#include "main_boost.h"
#include "random_toolkit.h"
#include "main_eigen.h"
#include "data_samples_matrix.h"
#include "d_ucs.h"

//constexpr Sign_fn_t BLUE = +1.0;
//constexpr Sign_fn_t RED = -1.0;

/*
D_ucs2 hardcoded_data(const Program_options& program_options) { // better way to load data? todo
    D_ucs2 d_ucs();
    X_lcb_sample my_sample; // why does this variable survive when going out of scope, or does it? todo
    my_sample.x_lcb.resize(program_options.vector_size);
    my_sample.x_lcb << 1.0, 1.0, 1.0;
    d_ucs.push_back(my_sample);   // is this doing a copy? todo

    my_sample.x_lcb << 1.0, -1.0, -1.0;
    d_ucs.push_back(my_sample);

    my_sample.x_lcb << 1.0, 3.0/3.0, 1.0/3.0;
    d_ucs.push_back(my_sample);

    my_sample.x_lcb << 1.0, -1.0/3.0, -3.0/3.0;
    d_ucs.push_back(my_sample);

    my_sample.x_lcb << 1.0, 2.0/3.0, 3.0/3.0;
    d_ucs.push_back(my_sample);

    my_sample.x_lcb << 1.0, 3.0/3.0, 2.0/3.0;
    d_ucs.push_back(my_sample);

    my_sample.x_lcb << 1.0, 0.5, 0.99;  // These points: y=2x+.1
    d_ucs.push_back(my_sample);

    my_sample.x_lcb << 1.0, 0, -0.1;
    d_ucs.push_back(my_sample);

    my_sample.x_lcb << 1.0, -0.1, -0.3;  // These two points: y=2x+.1
    d_ucs.push_back(my_sample);

    my_sample.x_lcb << 1.0, 0.1, 0.1;
    d_ucs.push_back(my_sample);
    return d_ucs;  // c++17 will move it? todo
}
*/

Sample_point random_sample_point(Program_options const& program_options) {
    Sample_point p(program_options.vector_size - 1);  // 0 origin
    for (auto i=0; i < program_options.vector_size - 1; ++i) {
        p[i] = pick_a_number_t(program_options.sample_space_lower_bound, program_options.sample_space_upper_bound); // need to make this a loop for d dimensions. todo
        BOOST_LOG_TRIVIAL(trace) << "random_sample_point coordinate: " << p[i];
    }
    return p;
}

D_ucs prepare_random_data_samples(const Program_options& program_options) {
    D_ucs d_ucs(program_options);
    return d_ucs;  // c++17 will move it? todo
}

/*
D_ucs2 user_supplied_data(const Program_options& program_options)  {
    D_ucs2 d_ucs;
    X_lcb_sample my_sample;
    my_sample.x_lcb.resize(program_options.vector_size);
    my_sample.x_lcb << 1,2,3;
    d_ucs.push_back(my_sample);
    return d_ucs;  // c++17 will move it? todo
}
*/

D_ucs prepare_data_samples(const Program_options& program_options) {  // should I be using object factories to create these data structures instead of putting them on the stack and then hopefully moving them?  todo
    switch (program_options.data_source) {
//    case Data_source::hardcoded_data:
//        return hardcoded_data(program_options);
//    case Data_source::user_supplied_data:
//        return user_supplied_data(program_options);
    case Data_source::random_data:
        return prepare_random_data_samples(program_options);
    default:
        BOOST_LOG_TRIVIAL(error) << "No valid data preparation selection.";
    }
    throw; // todo
}

C_lcb calc_vector_from_points(const D_ucs& d_ucs) {
    /*
    Could this be done with boost geometry?  "Robust calculation of Slope Intercept"? http://www.boost.org/doc/libs/1_55_0/libs/polygon/doc/GTL_boostcon_draft03.pdf
    */
    // Load X: the system of linear equations to describe the hyperplane in the data dimensions.
    Eigen_size_t variable_columns = d_ucs[0].x_lcb.size()-1;
    MatrixXd x_uc(variable_columns, variable_columns);
    C_lcb f_coefficients = VectorXd::Zero(variable_columns+1); // one more for the constant (above the number of variables)

    // Use first N points to specify the hyperplane
    for (Eigen_size_t i = 0; i < variable_columns; ++i) {
        x_uc.row(i) = d_ucs[static_cast<size_t>(i)].x_lcb.segment(1,2);  // copy values // why doesn't eigen use size_t?? todo static_cast
    }
    // Xa = b or a = X.inverse * b
    f_coefficients << -1 , x_uc.inverse() * Eigen::VectorXd::Ones(variable_columns);  // Is this fragile?  What is "conjugate gradient solver" and relevant here? todo

    // how does one make asserts disappear when compiling for production?  todo
//    assert( abs(1.0e-15) > f_coefficients(0,0)*1+f_coefficients(1,0)*p1(0,0)+f_coefficients(2,0)*p1(1,0) );
//    assert( abs(1.0e-15) > f_coefficients(0,0)*1+f_coefficients(1,0)*p2(0,0)+f_coefficients(2,0)*p2(1,0) );
    return f_coefficients;
}

C_lcb calc_vector_from_2_points_normalized(Sample_point& p1, Sample_point& p2) {
    /*
    Could this be done with boost geometry?  "Robust calculation of Slope Intercept"? http://www.boost.org/doc/libs/1_55_0/libs/polygon/doc/GTL_boostcon_draft03.pdf
    https://stackoverflow.com/questions/13242738/how-can-i-find-the-general-form-equation-of-a-line-from-two-points
    https://www.khanacademy.org/math/linear-algebra/vectors-and-spaces/dot-cross-products/v/normal-vector-from-plane-equation
    Get the tangent by subtracting the two points (x2-x1, y2-y1).
    Normalize it and rotate by 90 degrees to get the normal vector (a,b).
    Take the dot product with one of the points to get the constant, c.
    */
//    boost::timer::auto_cpu_timer my_auto_cpu_timer;  // avoid name clash with type
    VectorXd tangent = p1 - p2;
    BOOST_LOG_TRIVIAL(debug) << "calc_vector_from_2_points_normalized()> tangent: "<<tangent.transpose();
/*
      BOOST_LOG_TRIVIAL(debug) << "select_random_unknown_boundary()> tangent_normalized: "<<tangent_normalized.transpose();
//    https://stackoverflow.com/questions/5594684/compute-a-vector-which-is-perpendicular-to-another-given-vector-all-are-in-3d
//    https://gamedev.stackexchange.com/questions/70075/how-can-i-find-the-perpendicular-to-a-2d-vector
//    https://stackoverflow.com/questions/5594684/compute-a-vector-which-is-perpendicular-to-another-given-vector-all-are-in-3d
//    VectorXd x(tangent.size());
//    x << -1, VectorXd::Ones(tangent.size() - 1);  // bug: need to swap coefficients too! // Perpendicular lines theorem:  slope_line1 * slope_line2 = -1
//    VectorXd normal_vector = tangent_normalized.array() * x.array();
*/
    VectorXd normal_vector(p1.size());
    Eigen::Rotation2D<double> rotation(boost::math::constants::pi<double>()/2.0);  // set up the transformation of 90 degrees or pi/2

    normal_vector = rotation * (tangent).normalized();  // why can't I have parenthesis around "tangent".  Eigen fails silently!! todo
    double_t constant = -1*normal_vector.dot(p1);  // bias or constant.  I reverse the sign to convert from "general/standard form" to my "modified equal to zero LA form"
    BOOST_LOG_TRIVIAL(debug) << "select_random_unknown_boundary()> constant, normal_vector: "<<constant<<", "<<normal_vector.transpose();

    C_lcb f_coefficients = VectorXd::Zero(p1.size()+1);
    f_coefficients << -1*(normal_vector.dot(p1)), normal_vector;  // returns constant and coefficients (from vector)
    BOOST_LOG_TRIVIAL(debug) << "calc_vector_from_2_points_normalized()> f_coefficients: "<<f_coefficients.transpose();

    // how does one make asserts disappear when compiling for production?  todo
    assert(abs(1.0e-15) > f_coefficients(0,0)*1+f_coefficients(1,0)*p1(0,0)+f_coefficients(2,0)*p1(1,0) );
    assert(abs(1.0e-15) > f_coefficients(0,0)*1+f_coefficients(1,0)*p2(0,0)+f_coefficients(2,0)*p2(1,0) );
    return f_coefficients;
}

/*
//C_lcb calc_vector_from_2_points_steps(Sample_point& p1, Sample_point& p2) {  // debugging version that shows steps used in calc_vector_from_points_normailzed() function
//    /
//    Could this be done with boost geometry?  "Robust calculation of Slope Intercept"? http://www.boost.org/doc/libs/1_55_0/libs/polygon/doc/GTL_boostcon_draft03.pdf
//    https://stackoverflow.com/questions/13242738/how-can-i-find-the-general-form-equation-of-a-line-from-two-points
//    https://www.khanacademy.org/math/linear-algebra/vectors-and-spaces/dot-cross-products/v/normal-vector-from-plane-equation
//    Get the tangent by subtracting the two points (x2-x1, y2-y1).
//    Normalize it and rotate by 90 degrees to get the normal vector (a,b).
//    Take the dot product with one of the points to get the constant, c.
//    /
//    VectorXd tangent = p1 - p2;
//    VectorXd tangent_normalized = tangent.normalized(); // same as: = tangent/tangent.norm();
//    BOOST_LOG_TRIVIAL(debug) << "select_random_unknown_boundary()> tangent: "<<tangent.transpose();
//    BOOST_LOG_TRIVIAL(debug) << "select_random_unknown_boundary()> tangent_normalized: "<<tangent_normalized.transpose();
/// //   https://stackoverflow.com/questions/5594684/compute-a-vector-which-is-perpendicular-to-another-given-vector-all-are-in-3d
////    https://gamedev.stackexchange.com/questions/70075/how-can-i-find-the-perpendicular-to-a-2d-vector
////    https://stackoverflow.com/questions/5594684/compute-a-vector-which-is-perpendicular-to-another-given-vector-all-are-in-3d
////    VectorXd x(tangent.size());
////    x << -1, VectorXd::Ones(tangent.size() - 1);  // bug: need to swap coefficients too! // Perpendicular lines theorem:  slope_line1 * slope_line2 = -1
////    VectorXd normal_vector = tangent_normalized.array() * x.array();
//    /

//    VectorXd normal_vector(p1.size());
//    Eigen::Rotation2D<double> rotation(boost::math::constants::pi<double>()/2.0);  // set up the transformation of 90 degrees or pi/2
//    normal_vector = rotation * tangent_normalized;

//    double_t constant = -1*normal_vector.dot(p1);  // bias or constant.  I reverse the sign to convert from "general/standard form" to my "modified equal to zero LA form"
//    BOOST_LOG_TRIVIAL(debug) << "select_random_unknown_boundary()> constant, normal_vector: "<<constant<<", "<<normal_vector.transpose();
//    C_lcb f_coefficients = VectorXd::Zero(p1.size()+1);
//    f_coefficients << constant, normal_vector;

//    double_t sum = f_coefficients(0,0)*1+f_coefficients(1,0)*p1(0,0)+f_coefficients(2,0)*p1(1,0);
//    assert(abs(1.0e-15) > sum );
//    sum = f_coefficients(0,0)*1+f_coefficients(1,0)*p2(0,0)+f_coefficients(2,0)*p2(1,0);
//    assert(abs(1.0e-15) > sum );
//    return f_coefficients;
//}
*/

C_lcb select_random_unknown_boundary(const Program_options& program_options) {
    Sample_point p1 = random_sample_point(program_options);  //  co-ordinates of the point or the features of the sample
    Sample_point p2 = random_sample_point(program_options);
    while ( p1 == p2) { // need two different random points
        p2 = random_sample_point(program_options);
    }
    BOOST_LOG_TRIVIAL(debug) << "select_random_unknown_boundary()> point_1: "<<", "<<p1.transpose();
    BOOST_LOG_TRIVIAL(debug) << "select_random_unknown_boundary()> point_2: "<<", "<<p2.transpose();
    return calc_vector_from_2_points_normalized(p1, p2);  //    return calc_vector_from_points(d_ucs);  // another coded function to do same thing but differently.  Don't delete it.
}

/* double threshold_sign(double const& sum) {
    return (sum >= 0) ? BLUE : RED;  // sign() threshold (AKA. activation) function.
}
*/

Sign_fn_t dot_sign(const C_lcb &f_coefficients, const X_lcb &x_lcb_sample) {
    double sum = f_coefficients.dot( x_lcb_sample );  // simply the dot product using Eigen::  // is this noexcept? todo
//    assert (sum !=0);
    BOOST_LOG_TRIVIAL(trace) << "dot product :" << sum;
    return boost::math::sign(sum);  // sign() threshold (AKA. activation) function.
}

Sign_fn_t dot_sign(const C_lcb &f_coefficients, const E_lcb &f_exponents, X_lcb x_lcb_sample) {
//          NOTE:  We do want a copy of x_lcb_sample, because we may mutate it in here and don't want that outside of this f()
    x_lcb_sample = x_lcb_sample.array().pow( f_exponents.replicate(1,f_exponents.cols()).array() ); // raise each sample element to the respective exponent
    double sum = f_coefficients.dot( x_lcb_sample );  // simply the dot product using Eigen::  // is this noexcept? todo
//    assert (sum !=0);
    BOOST_LOG_TRIVIAL(trace) << "dot product :" << sum;
    return boost::math::sign(sum);  // sign() threshold (AKA. activation) function.
}

Sign_fn_t xor_sample_label (C_lcb const& f_coefficients1, C_lcb const& f_coefficients2,
                         E_lcb const& f_exponents, X_lcb const& x_lcb) {
    bool b1 = {false},
         b2 = {false};
    b1 = dot_sign(f_coefficients1, f_exponents, x_lcb) == BLUE ? true : false;
    b2 = dot_sign(f_coefficients2, f_exponents, x_lcb) == BLUE ? true : false;
    return b1 ^ b2 ? BLUE : RED;  // xor
}

label_random_sample::label_random_sample(const C_lcb &f_coefficients1,  // capture state
                                         const E_lcb &f_exponents) :
                                         f_coefficients1(f_coefficients1),
                                         f_coefficients2(0),  // size must be zero for if to work in operator() function  todo
                                         f_exponents(f_exponents)
{}  // constructor only assigns parameters

label_random_sample::label_random_sample(C_lcb const& f_coefficients1,
                                         C_lcb const& f_coefficients2,
                                         E_lcb const& f_exponents) :
                                         f_coefficients1(f_coefficients1),
                                         f_coefficients2(f_coefficients2),
                                         f_exponents(f_exponents)
{}  // constructor only assigns parameters

void label_random_sample::operator()(X_lcb_sample &sample) {
    if (f_coefficients2.size() == 0) {  // meaning there is only one boundary line.
        sample.y_true_sample_label = dot_sign(f_coefficients1, f_exponents, sample.x_lcb);
    } else {    // meaning we have two boundary lines and possibly therefore an XOR of the data.
        sample.y_true_sample_label = xor_sample_label (f_coefficients1, f_coefficients2, f_exponents, sample.x_lcb);
    }
    BOOST_LOG_TRIVIAL(trace) << "label_random_sample(): .x_lcb vector, .y_sample_color: " << std::setprecision(5) << std::fixed << sample.x_lcb.transpose() << ", " << sample.y_true_sample_label << " ##";
}

void plot_points_and_boundary( Gnuplot& gp, const std::string& title, const D_ucs& d_ucs, const bool display_line,
                               const C_lcb& f_coefficients,
                               const E_lcb& f_exponents, const bool circular_line, const Program_options& program_options ) {
/*   Plot the current samples and classifier boundary line

     Accepts graphs in the form: m0*x0 + m1*x1 + m2*x2 = 0  where x0 = 1 for the yintercept (or x2 (or higher) intercept)

     https://github.com/matplotlib/matplotlib/issues/7684/
     https://stackoverflow.com/questions/30002058/gnuplot-3d-plot-scatter-points-and-a-surface-and-lines-between-the-them
     https://i.stack.imgur.com/pnthj.png
     http://gnuplot.sourceforge.net/demo/scatter.html
     * http://psy.swansea.ac.uk/staff/carter/gnuplot/gnuplot_3d.htm
     * http://gnuplot.sourceforge.net/demo/surface1.html
     */
    if ( !program_options.show_plot_graph ) return;
    assert( 3 == program_options.vector_size ); // below requires size 3 todo

    std::vector<std::tuple<double,double> > red_points;  // load classified data points into data structure appropriate for plotting.
    std::vector<std::tuple<double,double> > blue_points;
    for( auto i : d_ucs ) {
        if ( i.y_true_sample_label == BLUE ) {
            blue_points.push_back(std::make_tuple(i.x_lcb[1], i.x_lcb[2]));
        } else {
            red_points.push_back(std::make_tuple(i.x_lcb[1], i.x_lcb[2]));
        }
    }
    gp << "set title '"<< title <<"'\n set size ratio -1\n";
    gp << "set xlabel 'x_1'\n set ylabel 'x_2'\nset xrange [-1.1:3]\nset yrange [-1.1:1.1]\n";
    gp << "plot '-' with points title 'pass:', '-' with points title 'fail:' ";
    if ( display_line ) {
        if ( circular_line ) {
            // Shows boundary circle (zero origin only) of the specific transformation, shown over the untranfored points in 2 dimensions.
            // ie. x2 = +- sqrt( x0^2 - x1^2 )
            BOOST_LOG_TRIVIAL(debug) << "plot_graph(): " << std::setprecision(2) << std::fixed
                                     << ", sqrt("
                                     << -1.0*f_coefficients[1]/f_coefficients[2]<<"*x**"<< f_exponents[1]
                                                                               << "+"<< -1*f_coefficients[0]/f_coefficients[2]
                    << ") "
                       //           << std::setprecision(2) << std::fixed
                    << ", -sqrt("
                    << -1.0*f_coefficients[1]/f_coefficients[2]<<"*x**"<< f_exponents[1]
                                                              << "+"<< -1*f_coefficients[0]/f_coefficients[2]
                    << ") \n";
            gp << std::setprecision(2) << std::fixed
               << ", sqrt("
               << -1.0*f_coefficients[1]/f_coefficients[2]<<"*x**"<< f_exponents[1]
                                                         << "+"<< -1*f_coefficients[0]/f_coefficients[2]
                    << ") ";
            gp << std::setprecision(2) << std::fixed
               << ", -sqrt("
               << -1.0*f_coefficients[1]/f_coefficients[2]<<"*x**"<< f_exponents[1]
                                                         << "+"<< -1*f_coefficients[0]/f_coefficients[2]  // why is this negative? todo
                    << ") \n";
        } else { // Shows boundary straight line of the points in 2 dimensions. ie. x2 = (m1*x1 + m0*x0)/m2 (simplified: y=ax+C)
            BOOST_LOG_TRIVIAL(debug) << "plot_graph() showing raw coefficients: "
                                     << std::setprecision(2) << std::fixed
                                     << -1*f_coefficients[1]<<"/"<<f_coefficients[2]<<"*x**"<< f_exponents[1]
                                     << "+" << -1*f_coefficients[0]<<"/"<<f_coefficients[2]<<" \n";
            BOOST_LOG_TRIVIAL(debug) << "plot_graph() normal slope intercept form: "
                                     << std::setprecision(2) << std::fixed
                                     << -1*f_coefficients[1]/f_coefficients[2]<<"*x**"<< f_exponents[1]
                                                                             << "+" << -1*f_coefficients[0]/f_coefficients[2]<<" \n";
            gp << std::setprecision(2) << std::fixed
               << " , " << -1*f_coefficients[1]/f_coefficients[2]<<"*x**"<< f_exponents[1]
                                                                << "+" << -1*f_coefficients[0]/f_coefficients[2]<<" \n";
        }
    }
    gp.send1d(blue_points);
    gp.send1d(red_points);
    gp.flush();
    usleep(program_options.sleep_micro_seconds); /* is there a more modern way? todo */
/*    gp.getMouse(mx, my, mb, "Left click to aim arrows, right click to exit.");
    printf("You pressed mouse button %d at x=%f y=%f\n", mb, mx, my);
    if(mb < 0) printf("The gnuplot window was closed.\n");*/
}

void plot_points_and_boundary( Gnuplot& gp, const std::string& title, const D_ucs& d_ucs,
                               const C_lcb& f_coefficient1, const C_lcb &f_coefficient2,
                               const E_lcb& f_exponents, const Program_options& program_options ) {  // f_exponents is not implemented, not needed yet. todo?
/*   Plot the current samples and classifier boundary line

     Accepts graphs in the form: m0*x0 + m1*x1 + m2*x2 = 0  where x0 = 1 for the yintercept (or x2 (or higher) intercept)

     https://github.com/matplotlib/matplotlib/issues/7684/
     https://stackoverflow.com/questions/30002058/gnuplot-3d-plot-scatter-points-and-a-surface-and-lines-between-the-them
     https://i.stack.imgur.com/pnthj.png
     http://gnuplot.sourceforge.net/demo/scatter.html
     * http://psy.swansea.ac.uk/staff/carter/gnuplot/gnuplot_3d.htm
     * http://gnuplot.sourceforge.net/demo/surface1.html
     */
    if ( !program_options.show_plot_graph ) return;
    assert( 3 == program_options.vector_size ); // below requires size 3 todo

    std::vector<std::tuple<double,double> > blue_points;  // todo refactor some of following code into a function, used 2x
    std::vector<std::tuple<double,double> > red_points;
    for( auto i : d_ucs ) {
        if ( i.y_true_sample_label == BLUE ) {
            blue_points.push_back(std::make_tuple(i.x_lcb[1], i.x_lcb[2]));
        } else {
            red_points.push_back(std::make_tuple(i.x_lcb[1], i.x_lcb[2]));
        }
    }
    gp << "set title '"<< title <<"'\n set size ratio -1\n";
    gp << "set xlabel 'x_1'\n set ylabel 'x_2'\nset xrange [-1.1:3]\nset yrange [-1.1:1.1]\n";
    gp << "plot '-' with points title 'pass:', '-' with points title 'fail:' , ";
    // Shows boundary straight line of the points in 2 dimensions. ie. x2 = (m1*x1 + m0*x0)/m2 (simplified: y=ax+C)
    BOOST_LOG_TRIVIAL(debug) << "plot_graph() showing raw coefficients1: "
                             << std::setprecision(2) << std::fixed
                             << -1*f_coefficient1[1]<<"/"<<f_coefficient1[2]<<"*x+"<< -1*f_coefficient1[0]<<"/"<<f_coefficient1[2];
    BOOST_LOG_TRIVIAL(debug) << "plot_graph() normal slope intercept form1: "
                             << std::setprecision(2) << std::fixed
                             << -1*f_coefficient1[1]/f_coefficient1[2]<<"*x+"<< -1*f_coefficient1[0]/f_coefficient1[2];
    gp << std::setprecision(2) << std::fixed
       << -1*f_coefficient1[1]/f_coefficient1[2]<<"*x+"<< -1*f_coefficient1[0]/f_coefficient1[2];
    BOOST_LOG_TRIVIAL(debug) << "plot_graph() showing raw coefficients2: "
                             << std::setprecision(2) << std::fixed
                             << -1*f_coefficient2[1]<<"/"<<f_coefficient2[2]<<"*x+"<< -1*f_coefficient2[0]<<"/"<<f_coefficient2[2];
    BOOST_LOG_TRIVIAL(debug) << "plot_graph() normal slope intercept form2: "
                             << std::setprecision(2) << std::fixed
                             << -1*f_coefficient2[1]/f_coefficient2[2]<<"*x+"<< -1*f_coefficient2[0]/f_coefficient2[2];
    gp << std::setprecision(2) << std::fixed
       <<", "<< -1*f_coefficient2[1]/f_coefficient2[2]<<"*x+"<< -1*f_coefficient2[0]/f_coefficient2[2]<<" \n";

    gp.send1d(blue_points);
    gp.send1d(red_points);
    gp.flush();
    usleep(program_options.sleep_micro_seconds); /* is there a more modern way? todo */
/*    gp.getMouse(mx, my, mb, "Left click to aim arrows, right click to exit.");
    printf("You pressed mouse button %d at x=%f y=%f\n", mb, mx, my);
    if(mb < 0) printf("The gnuplot window was closed.\n");*/
}

void plot_points_and_boundary( Gnuplot& gp, const std::string& title,
                               const Data_samples_matrix& d_ucs,
                               const Data_label_vector& y_lcb,
                               const C_lcb& f_coefficient1,
                               const E_lcb& f_exponents,  // not used at this time.
                               const Program_options& program_options ) {  // f_exponents is not implemented, not needed yet. todo?
/*   Plot the current samples and classifier boundary line

     Accepts graphs in the form: m0*x0 + m1*x1 + m2*x2 = 0  where x0 = 1 for the yintercept (or x2 (or higher) intercept)

     https://github.com/matplotlib/matplotlib/issues/7684/
     https://stackoverflow.com/questions/30002058/gnuplot-3d-plot-scatter-points-and-a-surface-and-lines-between-the-them
     https://i.stack.imgur.com/pnthj.png
     http://gnuplot.sourceforge.net/demo/scatter.html
     * http://psy.swansea.ac.uk/staff/carter/gnuplot/gnuplot_3d.htm
     * http://gnuplot.sourceforge.net/demo/surface1.html
     */
    if ( !program_options.show_plot_graph ) return;
    assert( 2 == program_options.vector_size ); // below requires size 3 todo
    std::vector<std::tuple<double,double> > blue_points;
    Eigen_size_t j = 0;
    for( auto i : d_ucs ) {
        blue_points.push_back(std::make_tuple(i.x_lcb[1], y_lcb[j]));
        ++j;
    }
    gp << "set title '"<< title <<"'\n set size ratio -1\n";
    gp << "set xlabel 'x_1'\n set ylabel 'x_2'\nset xrange [-1.1:3]\nset yrange [-1.1:1.1]\n";
    gp << "plot '-' with points title 'feature value', ";
    // Shows boundary straight line of the points in 2 dimensions. ie. x2 = (m1*x1 + m0*x0)/m2 (simplified: y=ax+C)
    BOOST_LOG_TRIVIAL(debug) << "plot_graph() showing raw coefficients1: "
                             << std::setprecision(2) << std::fixed
                             << +1*f_coefficient1[1] << "/"<<1.0  <<"*x + "<< +1*f_coefficient1[0]<<"/"<<1.0 << endl;
    BOOST_LOG_TRIVIAL(debug) << "plot_graph() normal slope intercept form1: "
                             << std::setprecision(2) << std::fixed
                             << +1*f_coefficient1[1]    /    1.0  <<"*x + "<< +1*f_coefficient1[0]   /   1.0 << endl;
    gp << std::setprecision(2) << std::fixed
       << +1*f_coefficient1[1] / 1.0    <<  "*x + "   << +1*f_coefficient1[0] / 1.0<<" \n";
    gp.send1d(blue_points);
    gp.flush();
    usleep(program_options.sleep_micro_seconds); /* is there a more modern way? todo */
/*    gp.getMouse(mx, my, mb, "Left click to aim arrows, right click to exit.");
    printf("You pressed mouse button %d at x=%f y=%f\n", mb, mx, my);
    if(mb < 0) printf("The gnuplot window was closed.\n");*/
}
