/* .h

   Copyright (c) 2017 Grant Rostig. All rights reserved. Use by written permission only, evidenced with blue ink on paper or a verifyable (pgp) email interaction with Grant Rostig.
   Permission is granted to use these files during and in support of CppMSG meetup group events and projects.

   Inspired by: Caltech LFD

   Shows how to:
   -
   -
   -

// #include "../gsl/gsl"   // Standard C++ Foundation: C++14 Guidelines Support Library

// Variable naming convention to correspond to mathematical use of fonts.
// lc=Lower Case, uc=Upper Case, s=script (ie.not block characters), b=bold
*/
#ifndef TEST_CASES_H
#define TEST_CASES_H
#include "utils.h"


void test_iai_model_data_procedure(Program_options& program_options);

void test_graphing_procedure(Program_options& program_options);



#endif // TEST_CASES_H
