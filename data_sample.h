#ifndef DATA_SAMPLE_H
#define DATA_SAMPLE_H
#include "main_eigen.h"
#include "utils.h"

class Data_sample
{
public:
    X_lcb x_lcb = {};  // a vector of the features of one sample, values range from [-1:+1] for double data type
    Data_sample() = delete;
    explicit Data_sample(Program_options const& program_options );
};

//class Data_sample_label
//{
//public:
//    double y_true_sample_label = 0;
//    double h_temp_sample_label = 0;
//    Data_sample_label() = delete;
//    explicit Data_sample_label(Program_options const& program_options );
//};

#endif // DATA_SAMPLE_H
