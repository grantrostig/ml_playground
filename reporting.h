//copyright grant rostig
#ifndef REPORTING_H
#define REPORTING_H
#include "utils.h"


template <typename T>   // default formats a data item to be output in a report.
std::string rpt_field( T const& data,  // need to make a template for these.  todo
                       //             int const& width7,
                       //             std::ios_base::fmtflags alignment,
                       //             char fill_char,
                       //             std::ios_base::fmtflags number_base,  // dec, oct, hex
                       //             std::ios_base::fmtflags show_positive
                       int                     const& width =       15,
                       std::ios_base::fmtflags alignment =          std::ios_base::right,
                       // std::ios_base::fmtflags alignment_string = std::ios_base::left,      // todo : add template if to use this value instead
                       char                    fill_char =          ' ',
                       int const&              precision =          4,
                       std::ios_base::fmtflags floating_p_format =  std::ios_base::fixed,  // fixed, scientific, defaultfloat
                       std::ios_base::fmtflags boolean_in_alpha =   std::ios_base::boolalpha, // show true/false instead of +1/-1
                       std::ios_base::fmtflags number_base =        std::ios_base::dec,  // dec, oct, hex
                       std::ios_base::fmtflags show_positive =      std::ios_base::showpos )
{   // if ( T == t_prototype::string ) {alignment_string = std::ios_base::left};
    std::stringstream field;
    field.setf(                 floating_p_format); // todo: these setf's could be ORed together
    field.setf(                 alignment);
    field.setf(                 boolean_in_alpha);
    field.setf(                 number_base);
    field.setf(                 show_positive);      // todo : does't work for long on unsigned long??
    field << std::showbase
          << std::setw(         width)
          << std::setfill(      fill_char)
          << std::setprecision( precision)
          << data
          << std::noshowbase        // should add option for this whenever... todo
          << std::noshowpos         // should add option for this whenever... todo
          << std::noshowpoint;      // should add option for this whenever... todo
    return field.str();
}

std::string rpt_separator( char separator_char = '|');

template <typename T>  // uses default formatted field settings, only sets width and adds separator
std::string rpt_cell( T const& data,
                     int const& width = 15,
                     char separator_char = '|') {
    std::stringstream formatted_element;
    formatted_element << rpt_field(data, width) << separator_char;
    return formatted_element.str();
}

//template <typename T>  // must pass in formatted field, only adds separator
std::string rpt_custom_cell( std::string const& data,
                             char separator_char = '|');

void report_parameters(const Program_options& program_options);

void report_e_in_out( Program_options const& program_options, std::string const& description, double const e_in, double const e_out );

void error_report(const Program_options& program_options, const std::vector<size_t>& e_in, const std::vector<size_t>& e_out = {});

/* ===== prior work and still usefull for debugging.

//void test_reporting();

//std::string rpt_field( std::string const& data,
//                       //             int const& width,
//                       //             std::ios_base::fmtflags alignment,
//                       //             char fill_char
//                       int const& width = 15,
//                       std::ios_base::fmtflags alignment = std::ios_base::right,
//                       char fill_char = ' '
//        );

//std::string rpt_field( int const& data,  // need to make a template for these.  todo
//                       //             int const& width7,
//                       //             std::ios_base::fmtflags alignment,
//                       //             char fill_char,
//                       //             std::ios_base::fmtflags number_base,  // dec, oct, hex
//                       //             std::ios_base::fmtflags show_positive
//                       int const& width = 7,
//                       std::ios_base::fmtflags alignment = std::ios_base::right,
//                       char fill_char = ' ',
//                       std::ios_base::fmtflags number_base = std::ios_base::dec,  // dec, oct, hex
//                       std::ios_base::fmtflags show_positive = std::ios_base::showpos
//        );

//std::string rpt_field( size_t const& data,
//                       //             int const& width,
//                       //             std::ios_base::fmtflags alignment,
//                       //             char fill_char,
//                       //             std::ios_base::fmtflags number_base
//                       int const& width = 7,
//                       std::ios_base::fmtflags alignment = std::ios_base::right,
//                       char fill_char = ' ',
//                       std::ios_base::fmtflags number_base = std::ios_base::dec
//        );

//std::string rpt_field( double const& data,
//                       //             int const& width,
//                       //             std::ios_base::fmtflags alignment,
//                       //             char fill_char,
//                       //             int const& precision,
//                       //             std::ios_base::fmtflags floating_p_format  // fixed, scientific, defaultfloat
//                       int const& width = 10,
//                       std::ios_base::fmtflags alignment = std::ios_base::right,
//                       char fill_char = ' ',
//                       int const& precision = 4,
//                       std::ios_base::fmtflags floating_p_format = std::ios_base::fixed  // fixed, scientific, defaultfloat
//        );


//std::string rpt_field2( double const& data,  // need to make a template for these.  todo
//                        //             int const& width7,
//                        //             std::ios_base::fmtflags alignment,
//                        //             char fill_char,
//                        //             std::ios_base::fmtflags number_base,  // dec, oct, hex
//                        //             std::ios_base::fmtflags show_positive
//                        int const& width = 30,
//                        std::ios_base::fmtflags alignment = std::ios_base::right,
//                        char fill_char = ' ',
//                        int const& precision = 4,
//                        std::ios_base::fmtflags floating_p_format = std::ios_base::fixed,  // fixed, scientific, defaultfloat
//                        std::ios_base::fmtflags number_base = std::ios_base::dec,  // dec, oct, hex
//                        std::ios_base::fmtflags show_positive = std::ios_base::showpos
//        );
*/

#endif // REPORTING_H
