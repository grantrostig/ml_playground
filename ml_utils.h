/* .h

   Copyright (c) 2017 Grant Rostig. All rights reserved. Use by written permission only, evidenced with blue ink on paper or a verifyable (pgp) email interaction with Grant Rostig.
   Permission is granted to use these files during and in support of CppMSG meetup group events and projects.

   Inspired by: Caltech LFD

   Shows how to:
   -
   -
   -

// #include "../gsl/gsl"   // Standard C++ Foundation: C++14 Guidelines Support Library

// Variable naming convention to correspond to mathematical use of fonts.
// lc=Lower Case, uc=Upper Case, s=script (ie.not block characters), b=bold
*/
#ifndef ML_UTILS_H
#define ML_UTILS_H

#include "X_lcb_sample.h"
#include "main_eigen.h"
#include "d_ucs.h"
#include "data_samples_matrix.h"
#include "utils.h"
#include "main_gnuplot.h"

constexpr Sign_fn_t BLUE = +1.0;  // todo not unused, but not in the .h.
constexpr Sign_fn_t RED = -1.0;

enum class Side_of_coin  {
    HEADS = 1,
    TAILS = 0
};

D_ucs hardcoded_data(const Program_options& program_options);

Sample_point random_sample_point(const Program_options& program_options);

D_ucs prepare_random_data_samples(const Program_options& program_options);

D_ucs user_supplied_data(const Program_options& program_options);

D_ucs prepare_data_samples(const Program_options& program_options);

C_lcb calc_vector_from_2_points_normalized(Sample_point& p1, Sample_point& p2);
C_lcb calc_vector_from_2_points_steps(Sample_point& p1, Sample_point& p2);
C_lcb calc_vector_from_points(const D_ucs& d_ucs);

C_lcb select_random_unknown_boundary(Program_options const& program_options);

//double threshold_sign(double const& sum);

Sign_fn_t dot_sign(const C_lcb &f_coefficients, const X_lcb& x_lcb_sample);

Sign_fn_t dot_sign(const C_lcb &f_coefficients, const E_lcb &f_exponents, X_lcb x_lcb_sample);

Sign_fn_t xor_sample_label (C_lcb const& f_coefficients1,
                     C_lcb const& f_coefficients2,
                     E_lcb const& f_exponents,
                     X_lcb_sample const& x_lcb_sample);

class label_random_sample {  // function object // is it correct to split it into header and cpp?  todo
  const C_lcb&  f_coefficients1,
                f_coefficients2;  // size is zero if not used  todo
  const E_lcb &f_exponents;
public:
  explicit label_random_sample(const C_lcb& f_coefficients1, const E_lcb& f_exponents);
  explicit label_random_sample(C_lcb const& f_coefficients1, C_lcb const& f_coefficients2, E_lcb const& f_exponents);
  void operator()(X_lcb_sample& sample);
};

void plot_points_and_boundary(Gnuplot &gp,
                const std::string &title,
                const D_ucs &d_ucs,
                const bool display_line,
                const C_lcb &f_coefficient,
                const E_lcb &f_exponents,
                const bool circular_line,
                const Program_options &program_options);

void plot_points_and_boundary(Gnuplot &gp,
                const std::string &title,
                const D_ucs &d_ucs,
                const C_lcb &f_coefficient,
                const C_lcb &f_coefficient2,
                const E_lcb &f_exponents,
                const Program_options &program_options);

void plot_points_and_boundary( Gnuplot& gp,
                const std::string& title,
                const Data_samples_matrix& d_ucs,
                const Data_label_vector& y_lcb,
                const C_lcb& f_coefficient1,
                const E_lcb& f_exponents,
                const Program_options& program_options );

#endif // ML_UTILS_H
