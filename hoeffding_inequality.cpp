//#include <numeric>
#include <ratio>
#include "hoeffding_inequality.h"
#include "utils.h"
#include "ml_utils.h"
#include "random_toolkit.h"

typedef std::pair<bool,double> HinEQ_Return;

HinEQ_Return probability_of_larger_difference(double const nu_probability_of_sample, double const mu_probability_in_bin, double const epsilon, size_t const n_uc_sample_size)
            // note: some of these parameters are not used.  todo
{
    if ( epsilon > 0 ) {
        double exponent = -2.0 * std::pow( epsilon, 2 ) * n_uc_sample_size;
        double bound = 2 * std::pow( math_constant_e<std::ratio<1,1>>() , exponent );   //  OR if you prefer: exp(1.0l) is "e"
        BOOST_LOG_TRIVIAL(info) << "probability_of_larger_difference(): nu, mu, epsilon, N, exponent: " << nu_probability_of_sample << ", " << mu_probability_in_bin << ", " << epsilon << ", " << n_uc_sample_size << ", " << exponent;
        bool p_of = abs( mu_probability_in_bin - nu_probability_of_sample) > epsilon;
        return HinEQ_Return {p_of, bound};
    } else { return HinEQ_Return {false , -1}; }   // throw todo
}

size_t flip_a_coin(size_t const num_times)  // todo move this to a header
{
    size_t num_heads {0};
    BOOST_LOG_TRIVIAL(trace) << "flip_a_coin() num_times " <<  num_times << ": ";
    for (size_t each_coin_flips = 0; each_coin_flips < num_times; ++each_coin_flips)
    {
        // num_heads += sample_binomial_distribution(1, 0.5);  // todo is this totally correct?  Which is faster?
        num_heads += pick_a_number_t( 0, 1 );       // todo minor type problem.
        BOOST_LOG_TRIVIAL(trace) << num_heads << ", " ;
    }
    return num_heads;
}

Coin_filtered_result_probability flip_a_set_of_coins(size_t num_coins, size_t times_each)
{
    std::vector<size_t> each_coin_num_heads;
    each_coin_num_heads.reserve( num_coins );
    for (size_t coin = 0; coin < num_coins; ++coin)
    {
        auto num_heads = flip_a_coin( times_each );
        each_coin_num_heads.emplace_back( num_heads );

    }
    Coin_filtered_result coin_filtered_result {};
    coin_filtered_result.coin_minimum_num_heads =   *std::min_element( each_coin_num_heads.begin(), each_coin_num_heads.end() );
    coin_filtered_result.first_coin_num_heads =     each_coin_num_heads[ 0 ];
    coin_filtered_result.random_coin_num_heads =    each_coin_num_heads[ pick_a_number_t( 0ul, each_coin_num_heads.size() - 1 )];
    BOOST_LOG_TRIVIAL(debug) << "flip_a_set_of_coins(): first, min, rand : " << coin_filtered_result.first_coin_num_heads << ", " << coin_filtered_result.coin_minimum_num_heads << ", " << coin_filtered_result.random_coin_num_heads << ", ";
    Coin_filtered_result_probability coin_filtered_result_probability;
    coin_filtered_result_probability.first_coin_num_heads = coin_filtered_result.first_coin_num_heads / static_cast<double>(times_each);
    coin_filtered_result_probability.coin_minimum_num_heads = coin_filtered_result.coin_minimum_num_heads / static_cast<double>(times_each);
    coin_filtered_result_probability.random_coin_num_heads = coin_filtered_result.random_coin_num_heads / static_cast<double>(times_each);
    BOOST_LOG_TRIVIAL(debug) << "flip_a_set_of_coins(): first, min, rand%: " << coin_filtered_result_probability.first_coin_num_heads << ", " << coin_filtered_result_probability.coin_minimum_num_heads << ", " << coin_filtered_result_probability.random_coin_num_heads << ", ";

    return coin_filtered_result_probability;
}

void heoffding_inequality_procedure(const Program_options& program_options)
{
    std::vector<Coin_filtered_result_probability> historical_coin_filtered_p_results; // keep history of multible trials, only used if program_options.measure_e_out == true // how can we show this in code? todo
    historical_coin_filtered_p_results.reserve( program_options.num_desired_trials );  // about 100,000 big number so why not do it.
    for (size_t trial = 0; trial < program_options.num_desired_trials; ++trial)
    {
        Coin_filtered_result_probability trial_coin_filtered_p_results;
        trial_coin_filtered_p_results = flip_a_set_of_coins( 1000, 10 );
        historical_coin_filtered_p_results.emplace_back( trial_coin_filtered_p_results );
    }
    Coin_filtered_result_probability result_totals;
    for (Coin_filtered_result_probability i : historical_coin_filtered_p_results ) {
        result_totals.random_coin_num_heads += i.random_coin_num_heads;
        result_totals.coin_minimum_num_heads += i.coin_minimum_num_heads;
        result_totals.first_coin_num_heads += i.first_coin_num_heads;
    }
    Coin_filtered_result_probability result_probablity;
    result_probablity.random_coin_num_heads = result_totals.random_coin_num_heads / static_cast<double>(program_options.num_desired_trials);
    result_probablity.coin_minimum_num_heads = result_totals.coin_minimum_num_heads / static_cast<double>(program_options.num_desired_trials);
    result_probablity.first_coin_num_heads = result_totals.first_coin_num_heads / static_cast<double>(program_options.num_desired_trials);
    cout << "heoffding_inequality_procedure() first, min, rand: " << result_probablity.first_coin_num_heads << ", " << result_probablity.coin_minimum_num_heads << ", " << result_probablity.random_coin_num_heads << ", " << endl;

    bool p_of {false};
    double as_much_probability_bad_thing {0};
    double nu_probability_of_sample { result_probablity.first_coin_num_heads };
    double mu_probability_in_bin { 0.5 };
    double epsilon {.05};
    size_t n_uc_sample_size {10};
    HinEQ_Return ret {};

    ret = probability_of_larger_difference( nu_probability_of_sample, mu_probability_in_bin, epsilon, n_uc_sample_size );
    p_of = ret.first;
    as_much_probability_bad_thing = ret.second;
    cout << "heoffding_inequality_procedure() probability_of_larger_difference first: epsilon, p_of, as_much_probability_bad_thing: " << epsilon <<", "<< p_of << ", " << as_much_probability_bad_thing << endl;

    nu_probability_of_sample = result_probablity.first_coin_num_heads;
    mu_probability_in_bin = 0.5;
    epsilon = 0.5;
    n_uc_sample_size = 10;
    ret = probability_of_larger_difference( nu_probability_of_sample, mu_probability_in_bin, epsilon, n_uc_sample_size );
    p_of = ret.first;
    as_much_probability_bad_thing = ret.second;
    cout << "heoffding_inequality_procedure() probability_of_larger_difference first: epsilon, p_of, as_much_probability_bad_thing: " << epsilon <<", "<< p_of << ", " << as_much_probability_bad_thing << endl;

    nu_probability_of_sample = result_probablity.coin_minimum_num_heads;
    mu_probability_in_bin = 0.5;
    epsilon = 0.05;
    n_uc_sample_size = 10;
    ret = probability_of_larger_difference( nu_probability_of_sample, mu_probability_in_bin, epsilon, n_uc_sample_size );
    p_of = ret.first;
    as_much_probability_bad_thing = ret.second;
    cout << "heoffding_inequality_procedure() probability_of_larger_difference **min: epsilon, p_of, as_much_probability_bad_thing: " << epsilon <<", "<< p_of << ", " << as_much_probability_bad_thing << endl;

    nu_probability_of_sample = result_probablity.coin_minimum_num_heads;
    mu_probability_in_bin = 0.5;
    epsilon = 0.5;
    n_uc_sample_size = 10;
    ret = probability_of_larger_difference( nu_probability_of_sample, mu_probability_in_bin, epsilon, n_uc_sample_size );
    p_of = ret.first;
    as_much_probability_bad_thing = ret.second;
    cout << "heoffding_inequality_procedure() probability_of_larger_difference **min: epsilon, p_of, as_much_probability_bad_thing: " << epsilon <<", " << p_of << ", " << as_much_probability_bad_thing << endl;

}
