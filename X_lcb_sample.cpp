/* .cpp

   Copyright (c) 2017 Grant Rostig. All rights reserved. Use by written permission only, evidenced with blue ink on paper or a verifyable (pgp) email interaction with Grant Rostig.
   Permission is granted to use these files during and in support of CppMSG meetup group events and projects.

   Inspired by: -

   Shows how to:
   -
   -
   -
*/
#include "X_lcb_sample.h"
#include "utils.h"
#include "ml_utils.h"

X_lcb_sample::X_lcb_sample(Program_options const& program_options ) {
    x_lcb.resize(program_options.vector_size);
    x_lcb << 1.0, random_sample_point(program_options) ;  // features of one sample with range [-1:+1] in double
//    for (Eigen_size_t i=0; i < program_options.vector_size - 1; ++i )
//        x_lcb << grostig::pick_a_number_t(program_options.sample_space_lower_bound, program_options.sample_space_upper_bound); // features of one sample, in this case without the x0 term of 1 for the constant intercepts.
}







