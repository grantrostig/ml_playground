#ifndef DATA_SAMPLES_MATRIX_H
#define DATA_SAMPLES_MATRIX_H

//#include "main_cppheaders.h"
#include "main_eigen.h"
#include "data_sample.h"

class Data_samples_matrix {
    std::vector<Data_sample> x_lcb_samples /*= {0}*/;   // why not?  todo
public:
    explicit Data_samples_matrix(Program_options const& program_options);
//    ~Data_samples_matrix2(); // to have or not to have ?? todo

     // The 6 default constructed functions that are created by the compiler
       Data_samples_matrix() =                           default;                        // constructor
       ~Data_samples_matrix() noexcept =                 default;              // destructor
       Data_samples_matrix(const Data_samples_matrix&) =              default;            // copy constructor
       Data_samples_matrix& operator=(const Data_samples_matrix&) =   default; // assignment ?? todo
       Data_samples_matrix(Data_samples_matrix&&) =                   default;                 // move constructor ?? todo
       //Data_samples_matrix2& operator=(const Data_samples_matrix2&&) = default;  // move assignment ?? todo    // won't compile with const, why did sutter include const?? bug on his part?
       Data_samples_matrix& operator=(Data_samples_matrix&&) =        default;  // move assignment ?? todo

    size_t                                      size();
    size_t                                      size() const;  // do we generally need this or both??  todo

    Data_sample                                get(size_t& i) const;
    void                                        set(size_t& i, Data_sample& v);
    Data_sample&                               operator[](size_t i);
    const Data_sample&                         operator[](size_t i) const;  // is this sensible??? todo
//    void                                        add();     // like push_back()

    std::vector<Data_sample>::iterator         erase( std::vector<Data_sample>::iterator begin);
    std::vector<Data_sample>::iterator         erase( std::vector<Data_sample>::iterator begin, std::vector<Data_sample>::iterator end);
//          *** creating an iterator therefore using "partial delegation"
//          instead of using "full delegation" to demonstrate implementation ***
//    class                                       Data_samples_matrix_iterator;  // should I have used a "friend class" instead?? not using "reisbeck's iterator from iterator pattern" yet todo
    std::vector<Data_sample>::iterator         begin();  // appears to be a long, why not size_t?  todo
    std::vector<Data_sample>::const_iterator   begin() const;  // no definion yet.
    std::vector<Data_sample>::iterator         end();
    std::vector<Data_sample>::const_iterator   end() const;  // no definion yet.
//    std::vector<Data_sample>::iterator cbegin();
//    std::vector<Data_sample>::iterator cbegin() const;
//    std::vector<Data_sample>::iterator cbegin();
    void add_noise(double const& per_cent_noise);  // add noise where labels are +1/-1 by flipping their sign
};
/*
//class Data_samples_matrix2::Data_samples_matrix_iterator {
//    std::vector<Data_sample>::iterator         current_Data_samples_matrix_iterator;
//public:
//    using Underlying_type = std::vector<Data_sample>;  // not used in templated version // is this trash?
//#define Underlying_type std::vector<Data_sample>
// //    using self = Data_samples_matrix_iterator<Underlying_type>;
//    using self = Underlying_type;
//    using value_type = std::iterator_traits<std::vector::iterator>::value_type;
//    using reference = std::iterator_traits<Underlying_type>::reference;
//    using pointer = std::iterator_traits<Underlying_type>::pointer;
//    using difference_type = std::iterator_traits<Underlying_type>::difference_type;
//    using iterator_category = std::iterator_traits<Underlying_type>::iterator_category;
    // std::random_access_iterator  // where do I show this? todo

//    Data_samples_matrix_iterator():
//        current_Data_samples_matrix_iterator{my_pointer} {}  // initialize or  = delete; this?? todo
//    Data_samples_matrix_iterator(Data_sample* my_pointer):
//        current_Data_samples_matrix_iterator{my_pointer} {}
// //    ~iterator();  // to have or not to have ?? todo

// //  iterator&                           operator=();  // never defined for an iterator?? todo
//    std::vector<Data_sample>::iterator operator*() const;
//    Data_sample&                       operator->() const;
//    Data_samples_matrix_iterator&                     operator++();
//    Data_samples_matrix_iterator                      operator++(int);  // why not size_t?? compile error todo
//    Data_samples_matrix_iterator&                     operator--();
//    Data_samples_matrix_iterator                      operator--(int);

//    ptrdiff_t                           operator-(const Data_samples_matrix_iterator&) const;
//    Data_samples_matrix_iterator                      operator+(ptrdiff_t) const;
//    Data_samples_matrix_iterator                      operator-(ptrdiff_t) const;

//    bool                                operator==(const Data_samples_matrix_iterator& rhs) const;
//    bool                                operator!=(const Data_samples_matrix_iterator& rhs) const;
//    bool                                operator<(const Data_samples_matrix_iterator& rhs) const;
//    bool                                operator<=(const Data_samples_matrix_iterator& rhs) const;
//    bool                                operator>(const Data_samples_matrix_iterator& rhs) const;
//    bool                                operator>=(const Data_samples_matrix_iterator& rhs) const;
//}
*/

#endif // DATA_SAMPLES_MATRIX_H
