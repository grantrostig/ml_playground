#ifndef MAIN_GNUPLOT_H
#define MAIN_GNUPLOT_H

// #include <boost/tuple/tuple.hpp>  // todo not needed since c++ got tuple?
// GNUPLOT_ENABLE_PTY must be defined before the first time that "gnuplot-iostream.h" is included if using ? feature.
#define GNUPLOT_ENABLE_PTY
#include "../grostig_tools/gnuplot-iostream.h"

#endif // MAIN_GNUPLOT_H
