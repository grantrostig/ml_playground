#include "features_n_label.h"
#include "reporting.h"
#include "ai_model.h"

std::ostream& operator<<(std::ostream & os, Ifloat_fnl_features_vector const& d) {  // compile error if placed in .cpp file - claims 3 parameters??? todo
    //    std::string program_algorithm = (*d.po_vm)["algorithm"].as<std::string>() ;
            //          "12345678901234567890"
    os      << rpt_separator('|')
            << rpt_cell("name",15)  // taking two fields plus one separator char.
            << endl;
    os      << rpt_separator('|')
            << rpt_cell(d.getName(),15)  // need to rethink sizing of cells and how to parameterize them  todo
            << endl;
    return os;
}

//std::string Iai_model_data::getFq_path_name() const
//{
//    return fq_path_name;
//}

//void Iai_model_data::setFq_path_name(const std::string &value)
//{
//    fq_path_name = value;
//}

//int Iai_model_data::getData_generation_strategy() const
//{
//    return data_generation_strategy;
//}

//void Iai_model_data::setData_generation_strategy(int value)
//{
//    data_generation_strategy = value;
//}

//std::string Iai_model_data::getName() const
//{
//    return name;
//}

//void Iai_model_data::setName(const std::string &value)
//{
//    name = value;
//}

//void Iint_fnl_label_scalar::getLabel() const
//{
//    return label;
//}

void Iint_fnl_label_scalar::setLabel(int value)
{
    label = value;
}

//float Ifloat_fnl_label_scalar::getLabel() const
//{
//    return label;
//}

void Ifloat_fnl_label_scalar::setLabel(float value)
{
    label = value;
}

//double Idouble_fnl_label_scalar::getLabel() const
//{
//    return label;
//}

void Idouble_fnl_label_scalar::setLabel(double value)
{
    label = value;
}

std::vector<double> Idouble_fnl_features_vector::getFeatures_vector() const
{
    return features_vector;
}

void Idouble_fnl_features_vector::setFeatures_vector(const std::vector<double> &value)
{
    features_vector = value;
}

std::vector<float> Ifloat_fnl_features_vector::getFeatures_vector() const
{
    return features_vector;
}

void Ifloat_fnl_features_vector::setFeatures_vector(const std::vector<float> &value)
{
    features_vector = value;
}

std::string Ifnl_label_scalar::getName() const
{
    return name;
}

void Ifnl_label_scalar::setName(const std::string &value)
{
    name = value;
}

std::string Ifnl_features_vector::getName() const
{
    return name;
}

void Ifnl_features_vector::setName(const std::string &value)
{
    name = value;
}

//Ifnl_features_vector Features_n_label::getFeatures() const
//{
//    return features;
//}

//void Features_n_label::setFeatures(const Ifnl_features_vector &value)
//{
//    features = value;
//}

//Ifnl_label_scalar Features_n_label::getScalar() const
//{
//    return scalar;
//}

//void Features_n_label::setScalar(const Ifnl_label_scalar &value)
//{
//    scalar = value;
//}

std::string Features_n_label::getName() const
{
    return name;
}

void Features_n_label::setName(const std::string &value)
{
    name = value;
}

std::vector<Features_n_label> Vector_of_samples::getVector_of_samples() const
{
    return vector_of_samples;
}

// =========================================================================

void Vector_of_samples::setVector_of_samples(const std::vector<Features_n_label> &value)
{
    vector_of_samples = value;
}

std::string Vector_of_samples::getName() const
{
    return name;
}

void Vector_of_samples::setName(const std::string &value)
{
    name = value;
}
