#ifndef MAIN_EIGEN_H
#define MAIN_EIGEN_H

//#include "main_cppheaders.h"
#include <vector>
#include <eigen3/Eigen/Dense>
using Eigen::VectorXd;
using Eigen::MatrixXd;

using Sample_point = VectorXd;    // Typically one fewer element than X_lcb (missing the leading 1).

// Below we have of the different components of an element of a polynomial eg: (4(x^2))/3
// these are not enforced by the type system (would typedef do it? todo), but may be useful to organize thinking.

using C_lcb = VectorXd;    // Coefficient in linear equation // not used in bias_variance
using W_lcb = VectorXd;    // Calculated Weight vector which plays the role of the coefficient
using X_lcb = VectorXd;    // Variable x's in linear equation OR Sample Vector // had problems in declaring a variable with VectorXd(3) below  todo
using E_lcb = VectorXd;    // Exponents on the x's in linear equation
//using D_lcb = VectorXd;  // Divisor for algebra of linear equation.  // not used - intended to do algebra on linear equations

using Data_label_vector = VectorXd;
using MLP_matrix            = MatrixXd;
using Vec_of_vectors    = std::vector<VectorXd>;
using Vec_of_matrices   = std::vector<MatrixXd>;
//using W_lcb_zero = Eigen::VectorXd::Zero(2); todo why not? is it a function?

using Eigen_size_t = long;  // pull this type from the eigen library todo
using Sign_fn_t    = int;  // the type resturned by boost::sign()  pull this type from the boost library todo

#endif // MAIN_EIGEN_H
