/* .h

   Copyright (c) 2017 Grant Rostig. All rights reserved. Use by written permission only, evidenced with blue ink on paper or a verifyable (pgp) email interaction with Grant Rostig.
   Permission is granted to use these files during and in support of CppMSG meetup group events and projects.

   Inspired by: Caltech LFD

   Shows how to:
   -
   -
   -

// #include "../gsl/gsl"   // Standard C++ Foundation: C++14 Guidelines Support Library

// Variable naming convention to correspond to mathematical use of fonts.
// lc=Lower Case, uc=Upper Case, s=script (ie.not block characters), b=bold
*/
#ifndef X_LCB_SAMPLE_H
#define X_LCB_SAMPLE_H
#include "main_eigen.h"
#include "utils.h"

class X_lcb_sample {  // contains the features of one sample.  called data vector in linear algebra
public:
    X_lcb x_lcb = {};  // a vector of the features of one sample, values range from [-1:+1] for double data type
    Sign_fn_t y_true_sample_label = {0};
    Sign_fn_t h_temp_sample_label = {0};
    double y_true_sample_label_double = {0};  // todo union? inheritance? this is used in different algorithm (bias variance) than the two above.
    X_lcb_sample() = delete;
    explicit X_lcb_sample(Program_options const& program_options );
//    virtual ~X_lcb_sample();  // why did alan suggest this?? todo
};

#endif // X_LCB_SAMPLE_H




