#ifndef HOEFFDING_INEQUALITY_H
#define HOEFFDING_INEQUALITY_H
#include "utils.h"

struct Coin_filtered_result {
    size_t coin_minimum_num_heads {0};
    size_t first_coin_num_heads {0};
    size_t random_coin_num_heads {0};
};

struct Coin_filtered_result_probability {
    double coin_minimum_num_heads {0};
    double first_coin_num_heads {0};
    double random_coin_num_heads {0};
};

void heoffding_inequality_procedure(const Program_options& program_options);

#endif // HOEFFDING_INEQUALITY_H
