#ifndef IVECTOR_OF_LABELS_H
#define IVECTOR_OF_LABELS_H
//#include "main_cppheaders.h"
#include "utils.h"

template <typename Num_type>
class Vector_of_labels_num_type_eigen;

template <typename Num_type>
class Ivector_of_labels_num_type_tfl;

template <typename Num_type>  // todo IMPORTANT double or float
class Ivector_of_labels  // memory organized for row wise operations
{
    std::string name;
    // Ivector_of_labels    vector_of_labels;
    Num_type vector_of_labels;
public:
    Ivector_of_labels/*<T>*/() {}  // todo : T needed or not?
    ~Ivector_of_labels() {}

    Ivector_of_labels(Ivector_of_labels const&)=delete;                 // guideline_TCPL ss3.3.4 else typically a disaster.
    Ivector_of_labels(Ivector_of_labels &&)=delete;                     // guideline_TCPL ss3.3.4 else typically a disaster.
    Ivector_of_labels& operator=(Ivector_of_labels const&)=delete;      // guideline_TCPL ss3.3.4 else typically a disaster.
    Ivector_of_labels& operator=(Ivector_of_labels &&)=delete;          // guideline_TCPL ss3.3.4 else typically a disaster.

    std::string getName() const;
    void setName(const std::string &value);

    virtual Num_type getVector_of_labels() const =0;
    virtual void setVector_of_labels(const Num_type &value) =0;

    static Ivector_of_labels* create(Numerical_library library) {
        switch (library) {
        case Numerical_library::eigen :
            return new Vector_of_labels_num_type_eigen<Num_type>;
            break;
        case Numerical_library::tensorflow :
            return new Ivector_of_labels_num_type_tfl<Num_type>;
            break;
        default:
            throw std::logic_error("bad library.");
        }
    }
    void sort() {

    }

};

//template <typename Num_type>  // todo IMPORTANT double or float
//class Ivector_of_labels_num_type : public Ivector_of_labels<Num_type>
//{
//    std::string name;  //element_numeric_type; // todo is this usefull or needed to denote float or double?
//    virtual void pure_virtural_sentinel()=0;
//public:
//    Ivector_of_labels_num_type() {}
//    ~Ivector_of_labels_num_type() {}
//};


template <typename Num_type>
class Vector_of_labels_num_type_eigen : public Ivector_of_labels<Num_type>
{
public:
    Vector_of_labels_num_type_eigen() {}
    ~Vector_of_labels_num_type_eigen() {}
    virtual Num_type getVector_of_labels() const {

    }
    virtual void setVector_of_labels(const Num_type &value) {

    }
};

template <typename Num_type>
class Ivector_of_labels_num_type_tfl : public Ivector_of_labels<Num_type>
{
//    ifdef (Num_type == double)  // todo IMPORTANT
//        Eigen::VectorXd  feature_matrix;
//    ifdef (Num_type == float)   // todo IMPORTANT
//        Eigen::VectorXd  feature_matrix;
public:
    Ivector_of_labels_num_type_tfl() {}
    ~Ivector_of_labels_num_type_tfl() {}
    virtual Num_type getVector_of_labels() const {

    }
    virtual void setVector_of_labels(const Num_type &value) {

    }
};




#endif // IVECTOR_OF_LABELS_H
