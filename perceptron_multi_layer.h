#ifndef PERCEPTRON_MULTI_LAYER_H
#define PERCEPTRON_MULTI_LAYER_H

#include "utils.h"

void perceptron_multi_layer_procedure(const Program_options& program_options);

#endif // PERCEPTRON_MULTI_LAYER_H
