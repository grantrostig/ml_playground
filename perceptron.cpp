/* .cpp

   Copyright (c) 2017 Grant Rostig. All rights reserved. Use by written permission only, evidenced with blue ink on paper or a verifyable (pgp) email interaction with Grant Rostig.
   Permission is granted to use these files during and in support of CppMSG meetup group events and projects.

   Inspired by: -

   Shows how to:
   -
   -
   -
*/
#include "perceptron.h"
#include "main_boost.h"
#include "random_toolkit.h"
#include "reporting.h"

std::tuple<W_lcb, unsigned long> perceptron( D_ucs& d_ucs, W_lcb const& w_lcb_weights_starting, size_t const num_trials, Gnuplot& plot_canvas, const Program_options& program_options) {

    bool any_misclassified = {};
    unsigned long perceptron_w_lcb_updates = 0;
    W_lcb w_lcb_weights {w_lcb_weights_starting};
    do {
        any_misclassified = false;
        long num_misclassied_points = {0};
        E_lcb ones = VectorXd::Ones(program_options.vector_size);
        for ( auto & x_lcb_sample_of_t : d_ucs ) { /* Look for one sample that is misclassified by current weight vector */
            /* Classify sample
             * classify based on current w */
            double h_lc_of_x_t = dot_sign(w_lcb_weights, x_lcb_sample_of_t.x_lcb);
            x_lcb_sample_of_t.h_temp_sample_label = h_lc_of_x_t;  // Storing the current classification for reporting purposes only.
            BOOST_LOG_TRIVIAL(trace) << ">> x1, truecolor, classcolor: "<<x_lcb_sample_of_t.x_lcb[1]<<", "<< x_lcb_sample_of_t.y_true_sample_label<<", "<< x_lcb_sample_of_t.h_temp_sample_label;
            if (h_lc_of_x_t != x_lcb_sample_of_t.y_true_sample_label) {
                ++num_misclassied_points;
                any_misclassified = true;
                if (!program_options.update_random_point_perceptron) {
                    /* *** Update weight vector ***
                     * calculate temp  = y(t)*x(t) without mutating x_lcb
                     * This is the magical step where the color's sign is used to create an incremental
                     * update to be applied to w
                     */
                    /*
                                                        BOOST_LOG_TRIVIAL(trace) << ">> perceptron_w_lcb_updates: " << perceptron_w_lcb_updates << "// ";
//                                                        cout << " scalar * x: ";
// //                                                     cout<< ">> add w adjustment/old w vec: " << w_lcb_weights << ", "<< temp<<" ";
// //                                                     cout << ">new w vec: " << w_lcb_weights << x_lcb_of_t.x_lcb << " // truecolor, classified: " << x_lcb_of_t.y_true_sample_label << ", " << x_lcb_of_t.h_temp_sample_label << std::endl;
                    */
                    if (program_options.max_update_attempts_times_num_desired_trials * program_options.num_desired_trials
                        < ++perceptron_w_lcb_updates) {
                        cout << std::endl;
                        cout.flush();
                        throw std::runtime_error("too many update attempts");
                    }
                    w_lcb_weights += (x_lcb_sample_of_t.y_true_sample_label * x_lcb_sample_of_t.x_lcb); // multiply the vector by the scalar + or -1, then add result to w.
                    plot_points_and_boundary( plot_canvas, "Perceptron (First Misclfy) Update Attempt:" + std::to_string( num_trials) +"."+ std::to_string( perceptron_w_lcb_updates ),
                                              d_ucs,
                                              true,
                                              w_lcb_weights,
                                              ones,
                                              false, program_options );
                }
            };
        }
        if ( program_options.update_random_point_perceptron && ( num_misclassied_points > 0 ) )
        {
            long random_integer_offset = pick_a_number_t(0ul,d_ucs.size()-1); // starting point for search of misclassified point
//                                      cout << ">>random_perceptron_update// num_misclassied_points, random_integer_offset: "<<num_misclassied_points<<", "<<random_integer_offset;
            /* https://stackoverflow.com/questions/17865488/find-the-nth-element-satisfying-a-condition */
            std::vector<X_lcb_sample>::iterator nth = d_ucs.begin() + random_integer_offset;
            auto equal = [](const X_lcb_sample & x) {
                BOOST_LOG_TRIVIAL(trace) << ">> equal: "<< (x.h_temp_sample_label == x.y_true_sample_label);
                return (x.h_temp_sample_label == x.y_true_sample_label);
            };
            auto misclassified_sample_it = std::find_if_not( nth, d_ucs.end(), equal );
            if ( d_ucs.end() == misclassified_sample_it ) {  /* in case we run off the end looking for a misclassified sample, search from beginning */
                BOOST_LOG_TRIVIAL(trace) << ">> before my offset :";
                misclassified_sample_it = std::find_if_not( d_ucs.begin(), d_ucs.end(), equal );
                assert ( d_ucs.end() != misclassified_sample_it );
            }
            if (program_options.max_update_attempts_times_num_desired_trials * program_options.num_desired_trials
                < ++perceptron_w_lcb_updates) {
                cout << std::endl;
                cout.flush();
                throw std::runtime_error("too many update attempts");
            }
            w_lcb_weights += (misclassified_sample_it->y_true_sample_label * misclassified_sample_it->x_lcb);
            plot_points_and_boundary( plot_canvas, "Perceptron (Random) Update Attempt:" + std::to_string( num_trials) +"."+ std::to_string( perceptron_w_lcb_updates ), d_ucs,
                                      true,
                                      w_lcb_weights,
                                      ones,
                                      false, program_options );
        }
    } while (any_misclassified);
    return std::tuple<W_lcb, unsigned long>(w_lcb_weights, perceptron_w_lcb_updates);
}

void perceptron_procedure(const Program_options& program_options) {
    assert (program_options.vector_size > 1); // don't think we want to do regression with only the constant feature?  todo
    unsigned long perceptron_w_lcb_updates_total = {0};
    Gnuplot plot_canvas;
    for ( size_t num_trials=1; num_trials <= program_options.num_desired_trials; ++num_trials ) {
        D_ucs d_ucs = prepare_data_samples(program_options);
        /*
       For our starting unknown f(), the Decision Boundary equation of the linear boundary hyperplane.
       We will do the initial marking of our samples with this hyperplane.
       y = f(x1,x2) = m0*x0 + m1*x1 + m2*x2, where x0=1 which makes the m0*x0 term the line intercept constant.
       Example 1 f() is:
       boundary line:       y = 2x + .01
       Green region is:     y >= x + .01
                            0 >= -y + x + .01
                            0 <= +y -x -.01
                            + m0*x0 + m1*x1 + m2*x2 >= 0
       ( (m0, m1, m2) dot (1, x1, x2) ) >= 0

       Example 2 f() is:
       boundary line:       y =  -+sqrt(- x**2 + 0.6)
       Green region is:     y >= -+sqrt(- x**2 + 0.6)
                            y**2 >=     - x**2 + 0.6
                            0 >= - y**2 - x**2 + 0.6
                            0 <= + y**2 + x**2 - 0.6
                            + -1*0.6 + 1*x**2 + 1*y**2 >=0     ** = "to the power of"
                            + m0*x0**e0 + m1*x1**e1 + m2*x2**e2 >= 0
       ( (m0, m1, m2) dot (1, x1, x2) ) >= 0

       Set characteristics of our unknown f() ie. coefficients and exponents
       */
        E_lcb ones = VectorXd::Ones(program_options.vector_size);  // only used: if algorithm == Program_algorithm::transformed_linear_ols
        /*
        evdouble m0 = 0;
        double m1 = 0;
        double m2 = 0;
        //            m0 = my_distribution2(my_random_device2);        // can be any line intercept constant that fits domain
        //            m1 = my_distribution2(my_random_device2);        // can be any slope.
        //            m2 = my_distribution2(my_random_device2);        // can be any slope
        m0 = -1;        // can be any line intercept constant
        m1 = -7.0;        // can be any slope.
        m2 = 4.0;        // can be any slope
        f_coefficients << m0, m1, m2;  // eigen also : << m0,m1,m2
        */
        W_lcb f_coefficients(program_options.vector_size);
        f_coefficients = select_random_unknown_boundary(program_options);
        // *** Mark the samples with their true colors derived from our unknown target f(). ***
        std::for_each(d_ucs.begin(), d_ucs.end(), label_random_sample{f_coefficients, ones}); // function object (functor) why can't I use ={}?  Would () do same?  todo
        W_lcb starting_w_lcb_weights = VectorXd::Zero(program_options.vector_size);  // usually set to zero vector [0]
        plot_points_and_boundary( plot_canvas, "Perceptron Labeled Points", d_ucs, true, f_coefficients, ones, false, program_options );
        // *** Actually run the algorithm ***
        auto [w_lcb_weights, perceptron_w_lcb_updates] = perceptron(d_ucs, starting_w_lcb_weights, num_trials, plot_canvas, program_options);  // Run the perceptron for one trial to calculate g(), ie. the weights. // why can't I do auto&?? todo
        // *** Report Phase 1 Trial Results ***
        perceptron_w_lcb_updates_total += perceptron_w_lcb_updates;
        BOOST_LOG_TRIVIAL(debug) << ">> Target f(x): " << f_coefficients.transpose() << ", Phase 1 weight vector: "<< w_lcb_weights.transpose();
        plot_points_and_boundary( plot_canvas, "Perceptron Final:" + std::to_string( num_trials ) +"."+ std::to_string( perceptron_w_lcb_updates ),
                                  d_ucs,
                                  true,
                                  w_lcb_weights,
                                  ones,
                                  false, program_options );
    }
    double updates_per_trial = perceptron_w_lcb_updates_total / program_options.num_desired_trials;
    cout << ">>> Number of features/dimensions: " << program_options.vector_size-1 << std::endl;
    cout << ">>> Average updates per perceptron classification ("<< ( program_options.update_random_point_perceptron ? "random" : "first") <<"): " << updates_per_trial << std::endl;
    cout << ">>> Ratio of average updates over number of samples: " << updates_per_trial*1.0 / program_options.n_uc_num_samples*1.0 << std::endl;
}
