/* .h

   Copyright (c) 2017 Grant Rostig. All rights reserved. Use by written permission only, evidenced with blue ink on paper or a verifyable (pgp) email interaction with Grant Rostig.
   Permission is granted to use these files during and in support of CppMSG meetup group events and projects.

   Inspired by: Caltech LFD

   Shows how to:
   -
   -
   -

// #include "../gsl/gsl"   // Standard C++ Foundation: C++14 Guidelines Support Library

// Variable naming convention to correspond to mathematical use of fonts.
// lc=Lower Case, uc=Upper Case, s=script (ie.not block characters), b=bold
*/
#ifndef BIAS_VARIANCE_H
#define BIAS_VARIANCE_H
#include "main_gnuplot.h"
#include "main_eigen.h"
#include "d_ucs.h"

//using D_ucs = std::vector<X_lcb_sample>;

void plot_graph_bias_variance( Gnuplot& gp, std::string title, D_ucs const& d_ucs, W_lcb const& w_lcb, E_lcb const& f_exponents, Program_options const& program_options );

double calc_g_mean(const std::vector<W_lcb>& historical_g_lcs_w);

double calc_bias(const std::vector<D_ucs>& historical_d_ucs, const double& g_mean);

double calc_variance(const std::vector<D_ucs>& historical_d_ucs, const std::vector<W_lcb>& historical_g_lcs_w, const double& g_mean);

void lin_reg_bias_variance_procedure(const Program_options& my_program_options);

#endif // BIAS_VARIANCE_H
