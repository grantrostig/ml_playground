ml_playground README.md file
=========================

Here is an example of a shell script to run the program twice with different
algorithm and store the standard output in a file.

====================
BEGIN ml_test.sh
====================

# run ml_playground a few times and append output
set -x
./ml_playground --algorithm lin_reg_4classification \
--n_uc_num_samples 1000 \
--num_desired_trials 1000 \
--per_cent_noise 0 \
--num_desired_propability_tests 10 \
--measure_e_in true --measure_e_out true --num_desired_e_out_tests 1  \
--show_plot_graph false \
--diagnostic_logging error \
--update_random_point_perceptron false \
--lrt_transform_algorithm using_2nd_order_polynomial \
| tee --append ml_playground_output.txt

./ml_playground --algorithm lin_reg_4class_transformed \
--n_uc_num_samples 1000 \
--num_desired_trials 1000 \
--per_cent_noise 0 \
--num_desired_propability_tests 10 \
--measure_e_in true --measure_e_out true --num_desired_e_out_tests 1  \
--show_plot_graph false \
--diagnostic_logging error \
--update_random_point_perceptron false \
--lrt_transform_algorithm using_2nd_order_polynomial \
| tee --append ml_test_output.txt

====================
END ml_test.sh
====================
