/* .cpp

   Copyright (c) 2017 Grant Rostig. All rights reserved. Use by written permission only, evidenced with blue ink on paper or a verifyable (pgp) email interaction with Grant Rostig.
   Permission is granted to use these files during and in support of CppMSG meetup group events and projects.

   Inspired by: -

   Shows how to:
   -
   -
   -
*/

#include "neural_network.h"
//#include "main_cppheaders.h"
#include "main_boost.h"
#include "X_lcb_sample.h"
#include "utils.h"
#include "ml_utils.h"
#include "lin_reg.h"
#include "perceptron.h"

double feed_forward2(X_lcb const& x_lcb_input, Vec_of_vectors& layer_outputs,
                    Vec_of_vectors& layer_signals, Vec_of_matrices const& layer_weights) {
    /* classify based on current w */
            // run MLP perceptron to determine current output.
            // layer 0 consists of x_lcb_of_t.x_lcb which are the data input points including x0=1 placeholder.
    size_t l_lc = 0; // layer 1
    layer_signals[l_lc] = layer_weights[l_lc].transpose() * x_lcb_input;      // matrix mulltiply which yields the equiv of a vector vector dot product.
    layer_outputs[l_lc] << 1.0, layer_signals[l_lc].cwiseSign();  // currently called by function pointer, check on performance of using a function object or lambda??? todo
    l_lc++; // layer 2
    layer_signals[l_lc] = layer_weights[l_lc].transpose() * layer_outputs[l_lc-1];
    layer_outputs[l_lc] << 1.0, layer_signals[l_lc].cwiseSign();  // grostig was &threshold_sign
    l_lc++; // layer 3
    layer_signals[l_lc] = layer_weights[l_lc].transpose() * layer_outputs[l_lc-1];
    layer_outputs[l_lc] << layer_signals[l_lc].unaryExpr(std::ref(boost::math::sign<double>)).cast<double>();  // alternate way versus cwiseSign() ;)
//    double return_value = layer_outputs[l_lc][0];
    return layer_outputs[l_lc][0];
}

auto perceptron_multi_layer(D_ucs& d_ucs, MLP_matrix const& w_lcb_of_known_f, // has a bug in at least the perfect weight case, and other algos may not be opimizable anyway.
                            E_lcb const& f_exponents, Gnuplot& plot_canvas,
                            Program_options const& program_options) {
    size_t perceptron_w_lcb_updates = 0;
    /* We will only build: 3 layers and two nodes per level
     *
     * l: layer
     * l=l0 input layer  is: x_lcb       points only
     * l=l1 hidden layer is: w_lc{1,2}_l_1 with two 'AND nodes' plus a constant 1
     * l=l2 hidden layer is: w_lc{1,2}_l_2 with two 'OR nodes'  plus a constant 1
     * l=l3 output layer is: w_lc{1,2}_l_3 with one 'OR node'   plus a constant 1
     *
     * s: signal
     * j: node index?
     *
     * Initialize constant components of xor MLP.
     */
    constexpr size_t l_uc = 3;
    Vec_of_vectors   layer_outputs(l_uc),
                     layer_signals(l_uc);
    Vec_of_matrices  layer_weights(l_uc);
    // layer 0 consists of d_ucs[].x_lcb which are the data input points including x0=1 placeholder.
    size_t l_lc = 0;  // layer = 1, but indexed at 0 in data
    layer_weights[l_lc].resize(3,2);  // rows/cols
    layer_signals[l_lc].resize(2);
    layer_outputs[l_lc].resize(3);
    if ( program_options.hmlp_update_algorithm_strategy == HMLP_update_algorithm::use_perfect_weights ) {
        layer_weights[0] = w_lcb_of_known_f;  // know function
//        layer_weights[0].col(1) = ;
    } else {
        layer_weights[l_lc] <<    0, 0,     // load initial weights for two weight vectors for two lines. // magic todo
                                  0, 0,
                                  0, 0;
    }
    l_lc++; // layer 2
    layer_weights[l_lc].resize(3,2);        // magic todo
    layer_signals[l_lc].resize(2);          // magic todo
    layer_outputs[l_lc].resize(3);          // magic todo
    layer_weights[l_lc] <<    -1.5, -1.5,   // implements AND  // magic todo
                              +1.0, -1.0,
                              +1.0,  1.0;
    l_lc++; // layer 3
    layer_weights[l_lc].resize(3,1);        // magic todo
    layer_signals[l_lc].resize(1);          // magic todo
    layer_outputs[l_lc].resize(1);          // magic todo
    layer_weights[l_lc] <<    -1.5,         // implements OR  // magic todo
                              +1.0,
                              +1.0;
    bool converged = false;
    while ( !converged ) {
        bool any_misclassified = false;
        long num_misclassied_points = {0};
//        std::vector<X_lcb_sample>::iterator first_misclassified_point = {};
        for ( auto & x_lcb_sample_of_t : d_ucs ) { /* Mark all samples with current hypothesis - ie. - Look for one+ sample that is misclassified by the two current weight vectors */
            /* Classify sample */
            Sign_fn_t h_lc_of_x_t = feed_forward2(x_lcb_sample_of_t.x_lcb, layer_outputs, layer_signals, layer_weights);  // todo +1 or -1
            x_lcb_sample_of_t.h_temp_sample_label = h_lc_of_x_t;  // Storing the current classification.
            BOOST_LOG_TRIVIAL(trace) << ">>Perceptron Multi-Layer(): x1, truecolor, classcolor: "<<x_lcb_sample_of_t.x_lcb[1]<<", "<< x_lcb_sample_of_t.y_true_sample_label<<", "<< x_lcb_sample_of_t.h_temp_sample_label;
            if (h_lc_of_x_t != x_lcb_sample_of_t.y_true_sample_label) {
                ++num_misclassied_points;
                if ( !any_misclassified ) {
                    any_misclassified = true;
                    // first_misclassified_point = x_lcb_sample_of_t;  // iterator pointing to first one  // can't get this to compile, want to use this later outside the loop, good idea? not??? todo
                }
            };
        }
        BOOST_LOG_TRIVIAL(debug) << ">>Perceptron Multi-Layer() num_misclassied_points: " << num_misclassied_points;
        if ( num_misclassied_points == 0 ) {
            converged = true;
            continue;
        }
        // determine which weight vector to update
        switch (program_options.hmlp_update_algorithm_strategy) {
        case HMLP_update_algorithm::first_point_both_w_lcb :
            // find first point // or remember it from the prior loop but can't compile???  todo
            for (auto x_lcb_sample_of_t:d_ucs ) {
                if (x_lcb_sample_of_t.h_temp_sample_label != x_lcb_sample_of_t.y_true_sample_label) {
                    // retrieve h's
                    // update w_lcb1
                    layer_weights[0].col(0) += (x_lcb_sample_of_t.y_true_sample_label * x_lcb_sample_of_t.x_lcb); // multiply the vector by the scalar + or -1, then add result to w.
                    BOOST_LOG_TRIVIAL(trace) << ">>Perceptron Multi-Layer() w1: layer_weights[0].col(0) x_lcb_sample_of_t.y_true_sample_label , x_lcb_sample_of_t.x_lcb" << layer_weights[0].col(0).transpose()<<x_lcb_sample_of_t.y_true_sample_label<<", "<<x_lcb_sample_of_t.x_lcb.transpose();
                    plot_points_and_boundary( plot_canvas, "Perceptron (First Misclfy) Update Attempt w1",
                                              d_ucs,  true, layer_weights[0].col(0), f_exponents, false, program_options );
                    // update w_lcb2
                    layer_weights[0].col(1) += (x_lcb_sample_of_t.y_true_sample_label * x_lcb_sample_of_t.x_lcb); // multiply the vector by the scalar + or -1, then add result to w.
                    BOOST_LOG_TRIVIAL(trace) << ">>Perceptron Multi-Layer() w2: layer_weights[0].col(1) x_lcb_sample_of_t.y_true_sample_label , x_lcb_sample_of_t.x_lcb" << layer_weights[0].col(1).transpose()<<x_lcb_sample_of_t.y_true_sample_label<<", "<<x_lcb_sample_of_t.x_lcb.transpose();
                    plot_points_and_boundary( plot_canvas, "Perceptron (First Misclfy) Update Attempt w2",
                                              d_ucs, true, layer_weights[0].col(1), f_exponents, false, program_options );
                    if (program_options.max_update_attempts_times_num_desired_trials * program_options.num_desired_trials
                        < ++perceptron_w_lcb_updates) {
                        cout << endl;  // endl or flush or both ?? todo
                        cout.flush();
                        throw std::runtime_error("too many update attempts");
                    }
                    BOOST_LOG_TRIVIAL(trace) << ">>Perceptron Muli-Layer() perceptron_w_lcb_updates: " << perceptron_w_lcb_updates << "// ";
                    break;
                }
            };
            break;
        case HMLP_update_algorithm::first_point_first_w_lcb :
            // find first point // or remember it from the prior loop but can't compile???  todo
            for (auto x_lcb_sample_of_t:d_ucs ) {
                if (x_lcb_sample_of_t.h_temp_sample_label != x_lcb_sample_of_t.y_true_sample_label) {
                    // retrieve h's
                    // update w_lcb1
                    layer_weights[0].col(0) += (x_lcb_sample_of_t.y_true_sample_label * x_lcb_sample_of_t.x_lcb); // multiply the vector by the scalar + or -1, then add result to w.
                    BOOST_LOG_TRIVIAL(trace) << ">>Perceptron Multi-Layer() w1: layer_weights[0].col(0) x_lcb_sample_of_t.y_true_sample_label , x_lcb_sample_of_t.x_lcb" << layer_weights[0].col(0).transpose()<<x_lcb_sample_of_t.y_true_sample_label<<", "<<x_lcb_sample_of_t.x_lcb.transpose();
                    plot_points_and_boundary( plot_canvas, "Perceptron (First Misclfy) Update Attempt w1",
                                              d_ucs, true, layer_weights[0].col(0), f_exponents, false, program_options );
                    // DON'T update w_lcb2
//                    layer_weights[0].col(1) += (x_lcb_sample_of_t.y_true_sample_label * x_lcb_sample_of_t.x_lcb); // multiply the vector by the scalar + or -1, then add result to w.
//                    BOOST_LOG_TRIVIAL(trace) << ">>Perceptron Multi-Layer() w2: layer_weights[0].col(1) x_lcb_sample_of_t.y_true_sample_label , x_lcb_sample_of_t.x_lcb" << layer_weights[0].col(1).transpose()<<x_lcb_sample_of_t.y_true_sample_label<<", "<<x_lcb_sample_of_t.x_lcb.transpose();
                    plot_points_and_boundary( plot_canvas, "Perceptron (First Misclfy) Update Attempt w2",
                                              d_ucs, true, layer_weights[0].col(1), f_exponents, false, program_options );
                    if (program_options.max_update_attempts_times_num_desired_trials * program_options.num_desired_trials
                        < ++perceptron_w_lcb_updates) {
                        cout << std::endl;  // endl or flush or both ?? todo
                        cout.flush();
                        throw std::runtime_error("too many update attempts");
                    }
                    BOOST_LOG_TRIVIAL(trace) << ">>Perceptron Muli-Layer() perceptron_w_lcb_updates: " << perceptron_w_lcb_updates << "// ";
                    break;
                }
            };
            break;
        default:
            throw std::logic_error("Perceptron Multi-Layer()@switch/default: should never get here.");
        }
    }
    std::tuple<W_lcb, W_lcb, size_t> return_value = {layer_weights[0].col(0), layer_weights[0].col(1), perceptron_w_lcb_updates};
    return return_value;
}

void neural_network_procedure(const Program_options& program_options) {
    assert (program_options.vector_size > 1); // don't think we want to do regression with only the constant feature?  todo
    long error_probabilty_samples_total = {0};
    unsigned long perceptron_w_lcb_updates_total = {0};
    //    std::vector<W_lcb> historical_g_lcs_w = {}; // keep history of multible trials
    //    std::vector<W_lcb> historical_f_lcs = {};
    Gnuplot plot_canvas_w1_a_w2_initial,
            plot_canvas_w1_initial,
            plot_canvas_w2_initial;
    for ( size_t trial_number=1; trial_number <= program_options.num_desired_trials; ++trial_number ) {
        D_ucs d_ucs = prepare_data_samples(program_options);
        // Create an unknown f() to classify and label points
        E_lcb f_exponents = VectorXd::Ones(program_options.vector_size); // typically ones.
        W_lcb   f_coefficients1(program_options.vector_size),
                f_coefficients2(program_options.vector_size);  // no initialization done here.
        f_coefficients1 = select_random_unknown_boundary(program_options);
        f_coefficients2 = select_random_unknown_boundary(program_options);

        // Mark the samples with their true colors as we decided with our unknown f().
        std::for_each(d_ucs.begin(), d_ucs.end(), label_random_sample{f_coefficients1, f_coefficients2, f_exponents});

        plot_points_and_boundary( plot_canvas_w1_initial, "Perceptron Multi-layer Procedure with unknown f() {line 1} illustrated",
                                  d_ucs, true, f_coefficients1, f_exponents, false, program_options );
        plot_points_and_boundary( plot_canvas_w2_initial, "Perceptron Multi-layer Procedure with unknown f() {line 2} illustrated",
                                  d_ucs, true, f_coefficients2, f_exponents, false, program_options );
        plot_points_and_boundary( plot_canvas_w1_a_w2_initial, "Perceptron Multi-layer Procedure with unknown f() {two lines} illustrated",
                                  d_ucs, f_coefficients1, f_coefficients2, f_exponents, program_options );
        //        add_noise( d_ucs, per_cent_noise );
        MLP_matrix w_lcb_of_known_f;
        w_lcb_of_known_f.resize(3,2);       // magic todo
        w_lcb_of_known_f.col(0) = f_coefficients1;
        w_lcb_of_known_f.col(1) = f_coefficients2;
        auto [w_lcb_weights1, w_lcb_weights2, perceptron_w_lcb_updates] =       // can I retrieve these by reference using auto& ?? todo
                    perceptron_multi_layer(d_ucs, w_lcb_of_known_f, f_exponents,
                                           plot_canvas_w1_a_w2_initial, program_options);  // Run the perceptron for one trial to calculate g(), ie. the weights.  // what about get<0> etc?  todo
        perceptron_w_lcb_updates_total += perceptron_w_lcb_updates;
        BOOST_LOG_TRIVIAL(debug) << ">> Target f1(x): " << f_coefficients1.transpose() << ", Phase 1 weight vector: "<< w_lcb_weights1.transpose();
        BOOST_LOG_TRIVIAL(debug) << ">> Target f2(x): " << f_coefficients2.transpose() << ", Phase 1 weight vector: "<< w_lcb_weights2.transpose();
        plot_points_and_boundary( plot_canvas_w1_a_w2_initial, "Perceptron Multi-layer Procedure Final",
                                  d_ucs, w_lcb_weights1, w_lcb_weights2, f_exponents, program_options );
    };
    double error_probability_all_samples = error_probabilty_samples_total / static_cast<double> (program_options.num_desired_propability_tests * program_options. num_desired_trials); // better way ?? todo
    cout << ">>> JUNK? Probability of error all trials: " << error_probability_all_samples << endl;

    double updates_per_trial = perceptron_w_lcb_updates_total / program_options.num_desired_trials;
    cout << ">>> Number of features/dimensions: " << program_options.vector_size-1 << std::endl;
    cout << ">>> Number of samples: " << program_options.n_uc_num_samples << std::endl;
    cout << ">>> Number of trials: " << program_options.num_desired_trials << std::endl;
    cout << ">>> Average updates per perceptron classification ("<< ( program_options.update_random_point_perceptron ? "random" : "first") <<"): " << updates_per_trial << std::endl;
    cout << ">>> Ratio of average updates over number of samples: " << updates_per_trial*1.0 / program_options.n_uc_num_samples*1.0 << std::endl;
    return;
}


