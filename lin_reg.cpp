/* .cpp

   Copyright (c) 2017 Grant Rostig. All rights reserved. Use by written permission only, evidenced with blue ink on paper or a verifyable (pgp) email interaction with Grant Rostig.
   Permission is granted to use these files during and in support of CppMSG meetup group events and projects.

   Inspired by: -

   Shows how to:
   -
   -
   -
*/
#include "lin_reg.h"
#include "main_boost.h"
#include "data_samples_matrix.h"
#include "random_toolkit.h"
#include "perceptron.h"
#include "ml_utils.h"
#include "reporting.h"

// ================================================

size_t inter_vector_label_discrepancies(D_ucs const& d_ucs,
                                        W_lcb const& w_lcb_weights,
                                        C_lcb const& f_coefficients,
                                        E_lcb const& f_exponents ) {
    size_t errors = {0};
    for (auto i:d_ucs) {
        auto base_label        = dot_sign(f_coefficients, f_exponents, i.x_lcb);  // todo : this may have been done in prior steps of various algorithms, need to optimize whenever...
        auto transformed_label = dot_sign(w_lcb_weights, f_exponents, i.x_lcb);
        if ( base_label != transformed_label ) {
            ++errors;
            BOOST_LOG_TRIVIAL(trace) << "inter_vector_label_discrepancies() found num_errors/base_label/transformed_label/x: "<<errors<<", "<< base_label<<", "<<transformed_label;
        }
    }
    return errors;
}

size_t inter_vector_label_discrepancies(D_ucs const& d_ucs,
                                        D_ucs const& d_ucs_transformed,
                                        C_lcb const& f_coefficients,
                                        E_lcb const& f_exponents,
                                        W_lcb const& w_lcb_weights
                                         ) {
    size_t  j      = {0},
            errors = {0};
//    C_lcb   f_ones   = Eigen::VectorXd::Ones(f_coefficients.size());
    W_lcb   w_ones   = Eigen::VectorXd::Ones(w_lcb_weights.size());
    for (auto i:d_ucs) {
        auto base_label         = dot_sign(f_coefficients, f_exponents, i.x_lcb);  // todo : this may have been done in prior steps of various algorithms, need to optimize whenever...
        auto transformed_label  = dot_sign(w_lcb_weights,  w_ones,      d_ucs_transformed[j].x_lcb);
        if ( base_label != transformed_label ) {
            ++errors;
            BOOST_LOG_TRIVIAL(trace) << "inter_vector_label_discrepancies() found num_errors/base_label/transformed_label/x: "<<errors<<", "<< base_label<<", "<<transformed_label;
        }
        ++j;
    }
    return errors;
}

size_t vector_vs_label_discrepancies(D_ucs & d_ucs, W_lcb const& w_lcb_weights ) {
    size_t errors = {0};
    E_lcb f_exponents = Eigen::VectorXd::Ones(d_ucs[0].x_lcb.size());  // todo might need to refactor dot_sign and this would be unnecessary.
    for (auto i:d_ucs) {
        i.h_temp_sample_label = dot_sign(w_lcb_weights, f_exponents, i.x_lcb);
        if ( i.y_true_sample_label != i.h_temp_sample_label ) {
            ++errors;
            BOOST_LOG_TRIVIAL(trace) << "vector_label_discrepancies() found num/f.dot/x: "<<errors<<", "<< dot_sign(w_lcb_weights, f_exponents, i.x_lcb)<<", "<<i.x_lcb.transpose();
        }
    }
    return errors;
}

double test_probability_of_errors(const C_lcb& f_coefficients,  // todo fis used but probably incorrectly.
                    const W_lcb& w_lcb_weights,
                    const E_lcb& f_exponents,
                    const Program_options& program_options )
{  // utility function for reporting purposes.
    long error_probabilty_samples = {0};
    for (size_t i = 1; i < program_options.num_desired_propability_tests; ++i) {
        // create a random sample to test against g()
        X_lcb random_sample(program_options.vector_size);
        random_sample << 1.0, random_sample_point(program_options);
        // label the random sample with both g() and f()
        Sign_fn_t true_label       = dot_sign(f_coefficients, f_exponents, random_sample);
        Sign_fn_t classified_label = dot_sign(w_lcb_weights, f_exponents, random_sample);
        if (classified_label != true_label) {
            ++error_probabilty_samples;
        }
    }
    return error_probabilty_samples;
}

D_ucs transform_exponentiate_x0_x1_x2( D_ucs const& d_ucs, E_lcb const& f_exponents, Program_options const& program_options) {
    D_ucs d_ucs_transformed(program_options);
    size_t j = {0};
    for ( auto i: d_ucs ) {  // not as fragile, would work if d_ucs wasn't indexable
        d_ucs_transformed[j].y_true_sample_label = i.y_true_sample_label;  // retain y's
        d_ucs_transformed[j].x_lcb = i.x_lcb.array().pow( f_exponents.replicate(1,f_exponents.cols()).array());
        ++j;
    }
    /*
    for ( auto it = d_ucs.begin(); it != d_ucs.end() ; ++it ) {  something missing from D_ucs? iterator arithmetic?  todo
        auto debug1 = d_ucs.begin();
        auto debug2 = it;
        long debug1l = d_ucs.begin();
        long debug2l = it;
        auto debug3 = d_ucs.begin() - it;
        size_t debug4 = d_ucs.begin() - it;
        size_t j = static_cast<size_t>(d_ucs.begin() - it);  // is this optimzed out and the same speed as commented version above?  todo  // appears to be a long, why not size_t?  todo
        d_ucs_transformed[j].y_true_sample_label = (*it).y_true_sample_label;  // two ways to dereference - need the parens.  todo
        d_ucs_transformed[j].x_lcb = it->x_lcb.array().pow( f_exponents.replicate(1,f_exponents.cols()).array());
    } */
    return d_ucs_transformed;
}

D_ucs transform_2nd_ord_polynm_in_x1_x2( D_ucs const& d_ucs, Program_options const& program_options) { // todo : MAGIC NUMBER
    Program_options transformed_p_options = {program_options};
    transformed_p_options.vector_size = 6;  // todo : MAGIC NUMBER assumes vector of 3 plus 3 for new transformed points.
    D_ucs d_ucs_transformed(transformed_p_options);  // create
    size_t j = {0};
    for ( auto i: d_ucs ) {
        d_ucs_transformed[j].y_true_sample_label = i.y_true_sample_label;   // retain y
        d_ucs_transformed[j].x_lcb <<   i.x_lcb,
                                        i.x_lcb[1]*i.x_lcb[2],
                                        i.x_lcb[1]*i.x_lcb[1],
                                        i.x_lcb[2]*i.x_lcb[2]; // retain the untransformed features and add transformed features (3) x*y, x**2, y**2
        ++j;
    }
    return d_ucs_transformed;
}

W_lcb ordinary_least_squares(const D_ucs& d_ucs, const Program_options& program_options ) {  // Linear regression by 'analytic solution' using 'ordinary least squares' algorithm AKA 'normal equation'
    BOOST_LOG_TRIVIAL(debug) << "OLS(): ";
//    boost::timer::auto_cpu_timer my_auto_cpu_timer;
    //     Construct X and y - todo : not efficient, yes this does a copy into the appropriate Eigen data structures to allow optimization algorithm.
    auto size = program_options.vector_size;
    if ( program_options.algorithm == Program_algorithm::lin_reg_4class_transformed
         && program_options.lrt_transform_algorithm == LRT_transform_algorithm::using_2nd_order_polynomial)
               size += 3;  // todo MAGIC NUMBER
    MatrixXd x_uc(d_ucs.size(), size);
    VectorXd y_lcb(d_ucs.size());

    {  // ugly yes, see 10 lines up. todo ??
//        boost::timer::auto_cpu_timer my_auto_cpu_timer;
        long j = 0;
        for ( auto i : d_ucs ) {
            x_uc.row(j) = i.x_lcb;  // why don't I need to do a transpose() here? helper functions :( eigen block todo
            y_lcb(j) = i.y_true_sample_label;
            ++j;
        }
    }
//    my_auto_cpu_timer.stop();
    BOOST_LOG_TRIVIAL(trace) << "OLS() X before algo: \n" << x_uc;
    BOOST_LOG_TRIVIAL(trace) << "OLS() y before algo: \n" << y_lcb.transpose();
//    my_auto_cpu_timer.resume();
    //    Perform Linear Regression to calcuate the weight vector
    /* Piecewise debugging of below
    MatrixXd x1 = x_uc.transpose() * x_uc;
    BOOST_LOG_TRIVIAL(debug) << "X1: \n" << x1;
    MatrixXd x2 = x1.inverse();
    BOOST_LOG_TRIVIAL(debug) << "X2: \n" << x2;
    MatrixXd x3 = x2 * x_uc.transpose();
    BOOST_LOG_TRIVIAL(debug) << "X3: \n" << x3;
    x_uc = x3;
    */
    x_uc = (x_uc.transpose() * x_uc).inverse() * x_uc.transpose();  // X hat calculation  // which has as a part the pseudo inverse?
    W_lcb temp = x_uc * y_lcb;
//    my_auto_cpu_timer.stop();
    BOOST_LOG_TRIVIAL(debug) << "OLS() return w_lcb transpose: " << temp.transpose();
//    my_auto_cpu_timer.resume();
    return temp;
};

W_lcb ordinary_least_squares2(const D_ucs& d_ucs, const Program_options& program_options ) {  // Linear regression by 'analytic solution' using 'ordinary least squares' algorithm AKA 'normal equation'
    BOOST_LOG_TRIVIAL(debug) << "OLS(): ";
//    boost::timer::auto_cpu_timer my_auto_cpu_timer;
    //     Construct X and y - todo : not efficient, yes this does a copy into the appropriate Eigen data structures to allow optimization algorithm.
    auto size = program_options.vector_size;
    if ( program_options.algorithm == Program_algorithm::lin_reg_4class_transformed
         && program_options.lrt_transform_algorithm == LRT_transform_algorithm::using_2nd_order_polynomial)
               size += 3;  // todo MAGIC NUMBER
    MatrixXd x_uc(d_ucs.size(), size);
    VectorXd y_lcb(d_ucs.size());

    {  // ugly yes, see 10 lines up. todo ??
//        boost::timer::auto_cpu_timer my_auto_cpu_timer;
        long j = 0;
        for ( auto i : d_ucs ) {
            x_uc.row(j) = i.x_lcb;  // why don't I need to do a transpose() here? helper functions :( eigen block todo
            y_lcb(j) = i.y_true_sample_label_double;  // todo only difference between above ie. !2
            ++j;
        }
    }
//    my_auto_cpu_timer.stop();
    BOOST_LOG_TRIVIAL(trace) << "OLS() X before algo: \n" << x_uc;
    BOOST_LOG_TRIVIAL(trace) << "OLS() y before algo: \n" << y_lcb.transpose();
//    my_auto_cpu_timer.resume();
    //    Perform Linear Regression to calcuate the weight vector
    /* Piecewise debugging of below
    MatrixXd x1 = x_uc.transpose() * x_uc;
    BOOST_LOG_TRIVIAL(debug) << "X1: \n" << x1;
    MatrixXd x2 = x1.inverse();
    BOOST_LOG_TRIVIAL(debug) << "X2: \n" << x2;
    MatrixXd x3 = x2 * x_uc.transpose();
    BOOST_LOG_TRIVIAL(debug) << "X3: \n" << x3;
    x_uc = x3;
    */
    x_uc = (x_uc.transpose() * x_uc).inverse() * x_uc.transpose();  // X hat calculation  // which has as a part the pseudo inverse?
    W_lcb temp = x_uc * y_lcb;
//    my_auto_cpu_timer.stop();
    BOOST_LOG_TRIVIAL(debug) << "OLS() return w_lcb transpose: " << temp.transpose();
//    my_auto_cpu_timer.resume();
    return temp;
};

// ===================================================

W_lcb ordinary_least_squares( Data_samples_matrix const& d_ucs, Data_label_vector const& y_lcb, Program_options const& program_options ) {  // Linear regression algorithm AKA Normal Equation per A Ng. ? todo
    BOOST_LOG_TRIVIAL(debug) << "OLS(): ";
//    boost::timer::auto_cpu_timer my_auto_cpu_timer;
    //     Construct X - todo : not efficient, yes this does a copy into the appropriate Eigen data structures to allow optimization algorithm.
    MatrixXd x_uc(d_ucs.size(), program_options.vector_size);

    {  // ugly yes, see 10 lines up. todo ??
//        boost::timer::auto_cpu_timer my_auto_cpu_timer;
        long j = 0;
        for ( auto i : d_ucs ) {
            x_uc.row(j) = i.x_lcb;  // why don't I need to do a transpose() here? helper functions :( eigen block todo
            ++j;
        }
    }
//    my_auto_cpu_timer.stop();
    BOOST_LOG_TRIVIAL(trace) << "OLS() X before algo: \n" << x_uc;
    BOOST_LOG_TRIVIAL(trace) << "OLS() y before algo: \n" << y_lcb.transpose();
//    my_auto_cpu_timer.resume();
    //    Perform Linear Regression to calcuate the weight vector
    /* Piecewise debugging of below
    MatrixXd x1 = x_uc.transpose() * x_uc;
    BOOST_LOG_TRIVIAL(debug) << "X1: \n" << x1;
    MatrixXd x2 = x1.inverse();
    BOOST_LOG_TRIVIAL(debug) << "X2: \n" << x2;
    MatrixXd x3 = x2 * x_uc.transpose();
    BOOST_LOG_TRIVIAL(debug) << "X3: \n" << x3;
    x_uc = x3;
    */
    x_uc = (x_uc.transpose() * x_uc).inverse() * x_uc.transpose();  // X hat calculation  // which has as a part the pseudo inverse?
    W_lcb weights = x_uc * y_lcb;
//    auto residual = (x_uc * weights - y_lcb).norm();  // todo not the correct formula for residual!? https://eigen.tuxfamily.org/dox/group__InplaceDecomposition.html
//    my_auto_cpu_timer.stop();
    BOOST_LOG_TRIVIAL(debug) << "OLS() return w_lcb transpose: " << weights.transpose();
//    my_auto_cpu_timer.resume();
    return weights;
};

//C_lcb residuals( Data_samples_matrix const& d_ucs, Data_label_vector const& y_lcb, W_lcb const& w_lcb_weights ) {

//}

std::tuple<W_lcb, unsigned long> gradient_descent( D_ucs& d_ucs, W_lcb const& w_lcb_weights_starting, E_lcb const& f_exponents, Gnuplot& plot_canvas, const Program_options& program_options) {
//    W_lcb perceptron( D_ucs2& d_ucs, W_lcb& w_lcb_weights, E_lcb f_exponents, unsigned long& perceptron_w_lcb_updates, Gnuplot& plot_canvas, const My_program_options& program_options) {

    bool any_misclassified = true;
    unsigned long perceptron_w_lcb_updates = 0;
    W_lcb w_lcb_weights {w_lcb_weights_starting};
    while (any_misclassified) {
        any_misclassified = false;
//        long num_misclassied_points = {0};
        for ( auto & x_lcb_sample_of_t : d_ucs ) { /* Look for one sample that is misclassified by current weight vector */
            /* Classify sample
             * classify based on current w */
            double h_lc_of_x_t = dot_sign(w_lcb_weights, f_exponents, x_lcb_sample_of_t.x_lcb);
            x_lcb_sample_of_t.h_temp_sample_label = h_lc_of_x_t;  // Storing the current classification for reporting purposes only.
            BOOST_LOG_TRIVIAL(trace) << ">> x1, truecolor, classcolor: "<<x_lcb_sample_of_t.x_lcb[1]<<", "<< x_lcb_sample_of_t.y_true_sample_label<<", "<< x_lcb_sample_of_t.h_temp_sample_label;
            if (h_lc_of_x_t != x_lcb_sample_of_t.y_true_sample_label) {
//                ++num_misclassied_points;
                any_misclassified = true;

                /* *** Update weight vector ***
                     * calculate temp  = y(t)*x(t) without mutating x_lcb
                     * This is the magical step where the color's sign is used to create an incremental
                     * update to be applied to w
                     */
                /*
                                                        BOOST_LOG_TRIVIAL(trace) << ">> perceptron_w_lcb_updates: " << perceptron_w_lcb_updates << "// ";
//                                                        cout << " scalar * x: ";
// //                                                     cout<< ">> add w adjustment/old w vec: " << w_lcb_weights << ", "<< temp<<" ";
// //                                                     cout << ">new w vec: " << w_lcb_weights << x_lcb_of_t.x_lcb << " // truecolor, classified: " << x_lcb_of_t.y_true_sample_label << ", " << x_lcb_of_t.h_temp_sample_label << std::endl;
                    */
                if (program_options.max_update_attempts_times_num_desired_trials * program_options.num_desired_trials
                        < ++perceptron_w_lcb_updates) {
                    cout << std::endl;
                    cout.flush();
                    throw std::runtime_error("too many update attempts");
                }
                w_lcb_weights += (x_lcb_sample_of_t.y_true_sample_label * x_lcb_sample_of_t.x_lcb); // multiply the vector by the scalar + or -1, then add result to w.
                plot_points_and_boundary( plot_canvas, "Perceptron (First Misclfy) Update Attempt", d_ucs, true, w_lcb_weights, f_exponents, false, program_options );

            };
        }
    };
    return std::tuple<W_lcb, unsigned long>(w_lcb_weights, perceptron_w_lcb_updates);
}

void lin_reg_gradient_descent_procedure(const Program_options& program_options){
    assert (program_options.vector_size > 1); // don't think we want to do regression with only the constant feature?  todo
    unsigned long perceptron_w_lcb_updates_total = {0};
    Gnuplot plot_canvas;
    for ( size_t num_trials=1; num_trials <= program_options.num_desired_trials; ++num_trials ) {
        D_ucs d_ucs = prepare_data_samples(program_options);
        /*
       For our starting unknown f(), the Decision Boundary equation of the linear boundary hyperplane.
       We will do the initial marking of our samples with this hyperplane.
       y = f(x1,x2) = m0*x0 + m1*x1 + m2*x2, where x0=1 which makes the m0*x0 term the line intercept constant.
       Example 1 f() is:
       boundary line:       y = 2x + .01
       Green region is:     y >= x + .01
                            0 >= -y + x + .01
                            0 <= +y -x -.01
                            + m0*x0 + m1*x1 + m2*x2 >= 0
       ( (m0, m1, m2) dot (1, x1, x2) ) >= 0

       Example 2 f() is:
       boundary line:       y =  -+sqrt(- x**2 + 0.6)
       Green region is:     y >= -+sqrt(- x**2 + 0.6)
                            y**2 >=     - x**2 + 0.6
                            0 >= - y**2 - x**2 + 0.6
                            0 <= + y**2 + x**2 - 0.6
                            + -1*0.6 + 1*x**2 + 1*y**2 >=0     ** = "to the power of"
                            + m0*x0**e0 + m1*x1**e1 + m2*x2**e2 >= 0
       ( (m0, m1, m2) dot (1, x1, x2) ) >= 0

       Set characteristics of our unknown f() ie. coefficients and exponents
       */
        E_lcb f_exponents = VectorXd::Ones(program_options.vector_size);  // only used: if algorithm == Program_algorithm::transformed_linear_ols
        /*
        evdouble m0 = 0;
        double m1 = 0;
        double m2 = 0;
        //            m0 = my_distribution2(my_random_device2);        // can be any line intercept constant that fits domain
        //            m1 = my_distribution2(my_random_device2);        // can be any slope.
        //            m2 = my_distribution2(my_random_device2);        // can be any slope
        m0 = -1;        // can be any line intercept constant
        m1 = -7.0;        // can be any slope.
        m2 = 4.0;        // can be any slope
        f_coefficients << m0, m1, m2;  // eigen also : << m0,m1,m2
        */
        W_lcb f_coefficients(program_options.vector_size);
        f_coefficients = select_random_unknown_boundary(program_options);
        // *** Mark the samples with their true colors derived from our unknown target f(). ***
        std::for_each(d_ucs.begin(), d_ucs.end(), label_random_sample{f_coefficients, f_exponents}); // function object (functor) why can't I use ={}?  Would () do same?  todo
        W_lcb starting_w_lcb_weights = VectorXd::Zero(program_options.vector_size);  // usually set to zero vector [0]
        plot_points_and_boundary( plot_canvas, "Perceptron Labeled Points", d_ucs, true, f_coefficients, f_exponents, false, program_options );
        auto [w_lcb_weights, perceptron_w_lcb_updates] = gradient_descent(d_ucs, starting_w_lcb_weights, f_exponents, /*perceptron_w_lcb_updates,*/ plot_canvas, program_options);  // Run the perceptron for one trial to calculate g(), ie. the weights. // why can't I do auto&?? todo
        // Report Phase 1 Trial Results
        perceptron_w_lcb_updates_total += perceptron_w_lcb_updates;
        BOOST_LOG_TRIVIAL(debug) << ">> Target f(x): " << f_coefficients.transpose() << ", Phase 1 weight vector: "<< w_lcb_weights.transpose();
        plot_points_and_boundary( plot_canvas, "Perceptron Final", d_ucs, true, w_lcb_weights, f_exponents, false, program_options );
    }
    double updates_per_trial = perceptron_w_lcb_updates_total / program_options.num_desired_trials;
    cout << ">>> Number of features/dimensions: " << program_options.vector_size-1 << std::endl;
    cout << ">>> Average updates per perceptron classification ("<< ( program_options.update_random_point_perceptron ? "random" : "first") <<"): " << updates_per_trial << std::endl;
    cout << ">>> Ratio of average updates over number of samples: " << updates_per_trial*1.0 / program_options.n_uc_num_samples*1.0 << std::endl;
}

void lin_reg_least_sq_procedure(Program_options const& program_options) {
    /*
         * http://setosa.io/ev/ordinary-least-squares-regression/
         * https://theclevermachine.wordpress.com/2012/09/01/derivation-of-ols-normal-equations/
         * http://mathworld.wolfram.com/NormalEquation.html
         * https://eli.thegreenplace.net/2014/derivation-of-the-normal-equation-for-linear-regression
         * https://web.stanford.edu/~mrosenfe/soc_meth_proj3/matrix_OLS_NYU_notes.pdf
         *
         *     In the case of linear models without transformation.
               For our starting unknown f(), the Decision Boundary equation of the linear boundary hyperplane.
               We will do the initial marking of our samples with this hyperplane.
               y = f(x1,x2) = m0*x0 + m1*x1 + m2*x2, where x0=1 which makes the m0*x0 term the line intercept constant.
               Example 1 f() is:
               boundary line:       m2*y =  -m1*x           + -m0*0.01  ie. the common y = ax + C form
               Green region is:     m2*y >= -m0x            + -m0*0.01
                                    0 >=    -m2*y   + -m1*x + -m0*0.01
                                    0 <=    +m2*y   - -m1*x - -m0*0.01
                                            + m0*x0 + m1*x1 + m2*x2 >= 0  where x0=1
               ( (m0, m1, m2) dot (1, x1, x2) ) >= 0

               Example 2 f() is:
               boundary line:       y =  -+sqrt(- x**2 + 0.6)
               Green region is:     y >= -+sqrt(- x**2 + 0.6)
                                    y**2 >=     - x**2 + 0.6
                                    0 >= - y**2 - x**2 + 0.6
                                    0 <= + y**2 + x**2 - 0.6
                                    + -1*0.6 + 1*x**2 + 1*y**2 >=0     ** = "to the power of"
                                    + m0*x0**e0 + m1*x1**e1 + m2*x2**e2 >= 0
               ( (m0, m1, m2) dot (1, x1, x2) ) >= 0

               Set characteristics of our unknown f() ie. coefficients and exponents
         */
    assert (program_options.vector_size > 1);   // don't think we want to do regression with only the constant feature?  todo
    size_t misclassifed_samples_e_in = {0};
    std::vector<W_lcb> historical_f_lcs_w       = {}; // keep history of multible trials, only used if program_options.measure_e_out == true // how can we show this in code? todo
    std::vector<E_lcb> historical_f_exponents   = {}; // todo refactor ordering of weight and exponents to emphasize them together with sign(); comprising f() or g().
    std::vector<W_lcb> historical_g_lcs_w       = {};

    Gnuplot plot_canvas_unknown_f,                        // several windows with graph outputs at various stages of the trial
            plot_canvas_with_noise,
            plot_canvas_final;

    for ( size_t trial_number=1; trial_number <= program_options.num_desired_trials; ++trial_number ) {
        // ====== Prepare Test Data ======
        Data_samples_matrix d_ucs(program_options);
        Data_label_vector   y_lcb = Eigen::VectorXd::Random(program_options.n_uc_num_samples);  // todo allow a range of values versus just -1 +1.
        // === Create an unknown weights of f() to classify and label points, consists of coefficients (and exponents which are experimental)  Note unknown f() consists of the sign function and the weights.
        W_lcb f_coefficients(program_options.vector_size);  // no initialization done here. todo
        E_lcb f_exponents = VectorXd::Ones(program_options.vector_size); // set to ones, therefore have no effect in this case.  todo should this be factored out?

        // === create a more complicated shape by systematically eliminating some samples
        // TOTAL REFACTOR f_coefficients = select_random_unknown_boundary(program_options);

        // === Mark the samples with their true colors as we decided with our unknown f().
        // TOTAL REFACTOR std::for_each(d_ucs.begin(), d_ucs.end(), label_random_sample{f_coefficients, f_exponents});
        // TOTAL REFACTOR d_ucs.erase(std::remove_if(d_ucs.begin(), d_ucs.end(), [](auto const x){ return x.y_true_sample_label == RED; })); // remove red points

        // TOTAL REFACTOR plot_points_and_boundary( plot_canvas_unknown_f, "lin_reg_least_sq_procedure Labeled Points with unknown f() illustrated",
                          // TOTAL REFACTOR         d_ucs, true,
                          // TOTAL REFACTOR         f_coefficients, f_exponents,
                          // TOTAL REFACTOR         false, // bool circular_line, only true if doing circular transformation (which is the only transformation supported at this point)
                          // TOTAL REFACTOR         program_options );
        // ====== Add Noise to prepared data, optionally ======
        if ( program_options.per_cent_noise > 0) {
            // TOTAL REFACTOR d_ucs.add_noise_11classifer( program_options.per_cent_noise );
//            plot_points_and_boundary( plot_canvas_with_noise, "lin_reg_least_sq_procedure Noisy Labeled Points with unknown f() illustrated",
//                                      d_ucs, true,
//                                      f_coefficients, f_exponents,
//                                      false,
//                                      program_options );
        }
        // ====== Calculate ordinary_least_squares ======
        W_lcb w_lcb_weights = VectorXd::Zero(program_options.vector_size);  // usually set to zero vector [0}
        size_t misclassified_samples = {0};
        // == Program_algorithm::lin_reg_least_sq_procedure
        w_lcb_weights = ordinary_least_squares(d_ucs, y_lcb, program_options); // *** Run LR for one trial to calculate g(), ie. the weights. ***
        // TOTAL REFACTOR  misclassified_samples = residuals( d_ucs, y_lcb, w_lcb_weights );
        plot_points_and_boundary( plot_canvas_final, "lin reg least sq procedure Final",
                                  d_ucs, y_lcb,
                                  w_lcb_weights, f_exponents,
                                  program_options );
        misclassifed_samples_e_in += misclassified_samples;
        // ====== Store OlS Results ======
        if ( program_options.measure_e_out ) {
            historical_f_lcs_w.push_back(f_coefficients);
            historical_f_exponents.push_back(f_exponents);
            historical_g_lcs_w.push_back(w_lcb_weights);
        }
        // === Log Single Trial Results ===
        BOOST_LOG_TRIVIAL(info)  << "lin_reg_least_sq_procedure() Trial #: " <<rpt_field(trial_number)<< ", probability of error this trial: " << rpt_field( misclassified_samples / static_cast<double>(program_options.n_uc_num_samples));
        BOOST_LOG_TRIVIAL(debug) << "lin_reg_least_sq_procedureleast() Boundary line equation, ie. the unknown weights of f(): " << f_coefficients.transpose() << " // Calculated weight vector: "<< w_lcb_weights.transpose();
    };
//    // ====== Measure E_out Performance with New Data, optionally  ======
//    size_t misclassified_samples_e_out = {0};
//    if ( program_options.measure_e_out ) {
//        for ( auto i = program_options.num_desired_trials; i-->0; ) {
//            for ( auto j = program_options.num_desired_e_out_tests; j-->0; ) {
//                D_ucs d_ucs_e_out(program_options);
//                // REWORK misclassified_samples_e_out += inter_vector_label_discrepancies(                            d_ucs_e_out, historical_g_lcs_w[i], historical_f_lcs_w[i], historical_f_exponents[i]);
//            }
//        }
//    }
//    // ====== Report Single Trial Results ======
//    report_e_in_out( program_options, "lin_reg_least_sq_procedure"
//                     , static_cast<double>(misclassifed_samples_e_in)
//                     / (program_options.n_uc_num_samples * program_options.num_desired_trials)
//                     , static_cast<double> (misclassified_samples_e_out)
//                     / (program_options.n_uc_num_samples * program_options.num_desired_trials * program_options.num_desired_e_out_tests)
//                     );
//    return;
};

void lin_reg_4class_transformed_procedure(const Program_options& program_options) {
    /*
     * http://setosa.io/ev/ordinary-least-squares-regression/
     * https://theclevermachine.wordpress.com/2012/09/01/derivation-of-ols-normal-equations/
     * http://mathworld.wolfram.com/NormalEquation.html
     * https://eli.thegreenplace.net/2014/derivation-of-the-normal-equation-for-linear-regression
     * https://web.stanford.edu/~mrosenfe/soc_meth_proj3/matrix_OLS_NYU_notes.pdf
     *
     *     In the case of linear models without transformation.
           For our starting unknown f(), the Decision Boundary equation of the linear boundary hyperplane.
           We will do the initial marking of our samples with this hyperplane.
           y = f(x1,x2) = m0*x0 + m1*x1 + m2*x2, where x0=1 which makes the m0*x0 term the line intercept constant.
           Example 1 f() is:
           boundary line:       m2*y =  -m1*x           + -m0*0.01  ie. the common y = ax + C form
           Green region is:     m2*y >= -m0x            + -m0*0.01
                                0 >=    -m2*y   + -m1*x + -m0*0.01
                                0 <=    +m2*y   - -m1*x - -m0*0.01
                                        + m0*x0 + m1*x1 + m2*x2 >= 0  where x0=1
           ( (m0, m1, m2) dot (1, x1, x2) ) >= 0

           Example 2 f() is:
           boundary line:       y =  -+sqrt(- x**2 + 0.6)
           Green region is:     y >= -+sqrt(- x**2 + 0.6)
                                y**2 >=     - x**2 + 0.6
                                0 >= - y**2 - x**2 + 0.6
                                0 <= + y**2 + x**2 - 0.6
                                + -1*0.6 + 1*x**2 + 1*y**2 >=0     ** = "to the power of"
                                + m0*x0**e0 + m1*x1**e1 + m2*x2**e2 >= 0
           ( (m0, m1, m2) dot (1, x1, x2) ) >= 0

           Set characteristics of our unknown f() ie. coefficients and exponents
     */
    assert (program_options.vector_size > 1);   // don't think we want to do regression with only the constant feature?  todo
    size_t misclassifed_samples_e_in = {0};
    std::vector<W_lcb> historical_f_lcs_w       = {}; // keep history of multible trials, only used if program_options.measure_e_out == true // how can we show this in code? todo
    std::vector<W_lcb> historical_g_lcs_w       = {};
    std::vector<E_lcb> historical_f_exponents   = {};
    Gnuplot plot_canvas_unknown_f,                    // several windows with graph outputs at various stages of the trial
            plot_canvas_with_noise,
            plot_canvas_transformed,
            plot_canvas_final,
            plot_canvas_final_transformed;
    for ( size_t trial_number=1; trial_number <= program_options.num_desired_trials; ++trial_number ) {
        // ====== Prepare Test Data ======
        D_ucs d_ucs = prepare_data_samples(program_options);
        // === Create an unknown weights of f() to classify and label points, consists of coefficients (and exponents which are experimental)  Note unknown f() consists of the sign function and the weights.
        W_lcb f_coefficients(program_options.vector_size);  // no initialization done here. todo
        E_lcb f_exponents = VectorXd::Ones(program_options.vector_size); // typically ones therefore no effect in non-transformed algorithm, but are set for to calculate a transformation in that algorithm.
        if ( program_options.lrt_transform_algorithm == LRT_transform_algorithm::using_2nd_order_polynomial ) {
            //  === unknown weights of f() === Must be carefully specified to result in a sensible circular region and that special case only.
            f_coefficients << -0.4, 1.0, 1.0;
                        // f_coefficients << -0.4, 1.0, 1.0;  // try other values for each, x0 probably needs to be less than one.
            f_exponents    <<  1,   2,   2;   // not 1,1,1 anymore, currently this setting is the only reason f_exponents exists, but could possibly be expanded for other cases or algorithms
        }
        else  // == LRT_transform_algorithm::using_exponentiate_x0_x1_x2 // same for this test case.
        {
            f_coefficients << -0.4, 1.0, 1.0;
            //            f_coefficients << -0.4, 1.0, 1.0;  // try other values for each, x0 probably needs to be less than one.
            f_exponents    <<  1,   2,   2;   // not 1,1,1 anymore, currently this setting is the only reason f_exponents exists, but could possibly be expanded for other cases or algorithms
        }
        /* Debug coding of data points and lines
         * if ( program_options.algorithm == Program_algorithm::transformed_linear_ols ) {
            assert( 3 == program_options.vector_size ); // below requires size 3 todo
            m0 = -0.4;         // Must be carefully specified to result in a sensible region.
            m1 = 1.0;        //
            m2 = 1.0;        //
            f_coefficients << m0, m1, m2;  // eigen also : << m0,m1,m2
            f_exponents << 1, 2, 2;        // only used: if algorithm == Program_algorithm::transformed_linear_ols
        } else {
            //            m0 = my_distribution2(my_random_device2);        // can be any line intercept constant that fits domain
            //            m1 = my_distribution2(my_random_device2);        // can be any slope.
            //            m2 = my_distribution2(my_random_device2);        // can be any slope
            assert( 3 == program_options.vector_size ); // below requires size 3 todo
            m0 = -.2;        // can be any line intercept constant
            m1 = 1.0;        // can be any slope.
            m2 = 1.0;        // can be any slope
            f_coefficients << m0, m1, m2;  // eigen also : << m0,m1,m2
        }
        */
        // === Mark the samples with their true colors as we decided with our unknown f().
        std::for_each(d_ucs.begin(), d_ucs.end(), label_random_sample{f_coefficients, f_exponents});
        plot_points_and_boundary( plot_canvas_unknown_f, "Linear Regression for Classification Labeled Points with unknown f() illustrated",
                                  d_ucs, true,
                                  f_coefficients, f_exponents,
                                  program_options.algorithm == Program_algorithm::lin_reg_4class_transformed ? true : false, // bool circular_line, only true if doing circular transformation (which is the only transformation supported at this point)
                                  program_options );
        // ====== Add Noise to prepared data, optionally ======
        if ( program_options.per_cent_noise > 0) {
            d_ucs.add_noise_11classifer( program_options.per_cent_noise );
            plot_points_and_boundary( plot_canvas_with_noise, "Linear Regression for Classification Noisy Labeled Points with unknown f() illustrated",
                                      d_ucs, true,
                                      f_coefficients, f_exponents,
                                      program_options.algorithm == Program_algorithm::lin_reg_4class_transformed ? true : false,
                                      program_options );
        }
        // ====== Calculate OlS ======
        D_ucs d_ucs_transformed(program_options);   // creates with random points which is wasteful,
                                                    // just need to declare and initialize it here with Zeros,
                                                    // D_ucs needs a copy construtor and or assignment operator.
                                                    // Also could/should?? be inside the if statement (following) todo
        W_lcb w_lcb_weights = VectorXd::Zero(program_options.vector_size);  // usually set to zero vector [0}
        size_t misclassified_samples_transformed    = {0};
        // create special transformed data set
        if ( program_options.lrt_transform_algorithm == LRT_transform_algorithm::using_2nd_order_polynomial ) {
            d_ucs_transformed = transform_2nd_ord_polynm_in_x1_x2( d_ucs, program_options );
            //              === can't be graphed on in 2 dimensions ===
            w_lcb_weights = ordinary_least_squares(d_ucs_transformed, program_options); // *** Run the OLS for one trial to calculate g(), ie. the weights. *** // mutates both variables.
            //              IS THIS EVEN POSSIBLE with 2nd order polynomial -  misclassified_samples = vector_label_discrepancies( d_ucs, w_lcb_weights, f_coefficients, f_exponents );
            misclassified_samples_transformed = vector_vs_label_discrepancies( d_ucs_transformed, w_lcb_weights );
            plot_points_and_boundary( plot_canvas_final, "Linear Regression for Classification Transformed Final with unknown f() illustrated",
                                      d_ucs_transformed, true,
                                      f_coefficients, f_exponents,
                                      true, program_options );
        }
        else  // == LRT_transform_algorithm::using_exponentiate_x0_x1_x2
        {
            d_ucs_transformed = transform_exponentiate_x0_x1_x2( d_ucs, f_exponents, program_options );
            plot_points_and_boundary( plot_canvas_transformed, "Linear Regression for Classification Transformed Labeled Points with unknown f() transform illustrated",
                                      d_ucs_transformed, true,
                                      f_coefficients, VectorXd::Ones(program_options.vector_size),
                                      false, program_options );  // why does this appear to work on the first case?  Just getting rid of the exponents doesn't seem a reliable method to show a line we have not computed yet.  I'm thinking that I should suppress printing of any line!
            w_lcb_weights = ordinary_least_squares(d_ucs_transformed, program_options); // *** Run the OLS for one trial to calculate g(), ie. the weights. *** // mutates both variables.
            //              IS THIS EVEN POSSIBLE with 2nd order polynomial -  misclassified_samples = vector_label_discrepancies( d_ucs, w_lcb_weights );
            misclassified_samples_transformed = vector_vs_label_discrepancies( d_ucs_transformed, w_lcb_weights );
            plot_points_and_boundary( plot_canvas_final, "Linear Regression for Classification Transformed Final",
                                      d_ucs, true,
                                      w_lcb_weights, f_exponents,
                                      true,  program_options );
            plot_points_and_boundary( plot_canvas_final_transformed, "Linear Regression for Classification Transformed Final-t",
                                      d_ucs_transformed, true,
                                      w_lcb_weights, VectorXd::Ones(program_options.vector_size),
                                      false, program_options );
        }
        misclassifed_samples_e_in += misclassified_samples_transformed;
        // ====== Store OlS Results ======
        if ( program_options.measure_e_out ) {
            historical_f_lcs_w.push_back(f_coefficients);
            historical_f_exponents.push_back(f_exponents);
            historical_g_lcs_w.push_back(w_lcb_weights);
        }
        // === Log Single Trial Results ===
        BOOST_LOG_TRIVIAL(info)  << "ordinary_least_squares_procedure() Trial #: " <<rpt_field(trial_number,15)<< ", probability of error this trial: " << rpt_field( misclassified_samples_transformed / static_cast<double>(program_options.n_uc_num_samples),15);
        BOOST_LOG_TRIVIAL(debug) << "ordinary_least_squares_procedure() Boundary line equation, ie. the unknown weights of f(): " << f_coefficients.transpose() << " // Calculated weight vector: "<< w_lcb_weights.transpose();
    };
    // ====== Measure E_out Performance with New Data, optionally  ======
    size_t misclassified_samples_e_out = {0};
    if ( program_options.measure_e_out ) {
        for ( auto i = program_options.num_desired_trials; i-->0; ) {
            for ( auto j = program_options.num_desired_e_out_tests; j-->0; ) {
                D_ucs d_ucs_e_out(program_options);
                if ( program_options.lrt_transform_algorithm == LRT_transform_algorithm::using_2nd_order_polynomial ) {
                    D_ucs d_ucs_e_out_transformed = transform_2nd_ord_polynm_in_x1_x2( d_ucs_e_out, program_options );  // todo : default copy constructor worked?  apparently
                    misclassified_samples_e_out += inter_vector_label_discrepancies(
                                d_ucs_e_out, d_ucs_e_out_transformed, historical_f_lcs_w[i], historical_f_exponents[i], historical_g_lcs_w[i]);
                }
                else  // == LRT_transform_algorithm::using_exponentiate_x0_x1_x2
                {
                    misclassified_samples_e_out += inter_vector_label_discrepancies(
                                d_ucs_e_out, historical_g_lcs_w[i], historical_f_lcs_w[i], historical_f_exponents[i]);
                }
            }
        }
    }
    // ====== Report All Trials Results ======
    report_e_in_out( program_options, "ordinary_least_squares_procedure All Trials Results"
                     , static_cast<double>(misclassifed_samples_e_in)
                        / (program_options.n_uc_num_samples * program_options.num_desired_trials)
                     , static_cast<double> (misclassified_samples_e_out)
                        / (program_options.n_uc_num_samples * program_options.num_desired_trials * program_options.num_desired_e_out_tests)
                     );
    return;
}

void lin_reg_4classification_procedure(const Program_options& program_options) {
    /*
     * http://setosa.io/ev/ordinary-least-squares-regression/
     * https://theclevermachine.wordpress.com/2012/09/01/derivation-of-ols-normal-equations/
     * http://mathworld.wolfram.com/NormalEquation.html
     * https://eli.thegreenplace.net/2014/derivation-of-the-normal-equation-for-linear-regression
     * https://web.stanford.edu/~mrosenfe/soc_meth_proj3/matrix_OLS_NYU_notes.pdf
     *
     *     In the case of linear models without transformation.
           For our starting unknown f(), the Decision Boundary equation of the linear boundary hyperplane.
           We will do the initial marking of our samples with this hyperplane.
           y = f(x1,x2) = m0*x0 + m1*x1 + m2*x2, where x0=1 which makes the m0*x0 term the line intercept constant.
           Example 1 f() is:
           boundary line:       m2*y =  -m1*x           + -m0*0.01  ie. the common y = ax + C form
           Green region is:     m2*y >= -m0x            + -m0*0.01
                                0 >=    -m2*y   + -m1*x + -m0*0.01
                                0 <=    +m2*y   - -m1*x - -m0*0.01
                                        + m0*x0 + m1*x1 + m2*x2 >= 0  where x0=1
           ( (m0, m1, m2) dot (1, x1, x2) ) >= 0

           Example 2 f() is:
           boundary line:       y =  -+sqrt(- x**2 + 0.6)
           Green region is:     y >= -+sqrt(- x**2 + 0.6)
                                y**2 >=     - x**2 + 0.6
                                0 >= - y**2 - x**2 + 0.6
                                0 <= + y**2 + x**2 - 0.6
                                + -1*0.6 + 1*x**2 + 1*y**2 >=0     ** = "to the power of"
                                + m0*x0**e0 + m1*x1**e1 + m2*x2**e2 >= 0
           ( (m0, m1, m2) dot (1, x1, x2) ) >= 0

           Set characteristics of our unknown f() ie. coefficients and exponents
     */
    assert (program_options.vector_size > 1);   // don't think we want to do regression with only the constant feature?  todo
    size_t misclassifed_samples_e_in = {0};
    std::vector<W_lcb> historical_f_lcs_w       = {}; // keep history of multible trials, only used if program_options.measure_e_out == true // how can we show this in code? todo
    std::vector<W_lcb> historical_g_lcs_w       = {};
    std::vector<E_lcb> historical_f_exponents   = {};
    Gnuplot plot_canvas_unknown_f,                        // several windows with graph outputs at various stages of the trial
            plot_canvas_with_noise,
            plot_canvas_final;

    for ( size_t trial_number=1; trial_number <= program_options.num_desired_trials; ++trial_number ) {
        // ====== Prepare Test Data ======
        D_ucs d_ucs = prepare_data_samples(program_options);
        // === Create an unknown weights of f() to classify and label points, consists of coefficients (and exponents which are experimental)  Note unknown f() consists of the sign function and the weights.
        W_lcb f_coefficients(program_options.vector_size);  // no initialization done here. todo
        E_lcb f_exponents = VectorXd::Ones(program_options.vector_size); // set to ones, therefore have no effect in this case.  todo should this be factored out?
        f_coefficients = select_random_unknown_boundary(program_options);

        /* Debug coding of data points and lines
         * if ( program_options.algorithm == Program_algorithm::transformed_linear_ols ) {
            assert( 3 == program_options.vector_size ); // below requires size 3 todo
            m0 = -0.4;         // Must be carefully specified to result in a sensible region.
            m1 = 1.0;        //
            m2 = 1.0;        //
            f_coefficients << m0, m1, m2;  // eigen also : << m0,m1,m2
            f_exponents << 1, 2, 2;        // only used: if algorithm == Program_algorithm::transformed_linear_ols
        } else {
            //            m0 = my_distribution2(my_random_device2);        // can be any line intercept constant that fits domain
            //            m1 = my_distribution2(my_random_device2);        // can be any slope.
            //            m2 = my_distribution2(my_random_device2);        // can be any slope
            assert( 3 == program_options.vector_size ); // below requires size 3 todo
            m0 = -.2;        // can be any line intercept constant
            m1 = 1.0;        // can be any slope.
            m2 = 1.0;        // can be any slope
            f_coefficients << m0, m1, m2;  // eigen also : << m0,m1,m2
        }
        */
        // === Mark the samples with their true colors as we decided with our unknown f().
        std::for_each(d_ucs.begin(), d_ucs.end(), label_random_sample{f_coefficients, f_exponents});
        plot_points_and_boundary( plot_canvas_unknown_f, "Linear Regression for Classification Labeled Points with unknown f() illustrated",
                                  d_ucs, true,
                                  f_coefficients, f_exponents,
                                  false, // bool circular_line, only true if doing circular transformation (which is the only transformation supported at this point)
                                  program_options );
        // ====== Add Noise to prepared data, optionally ======
        if ( program_options.per_cent_noise > 0) {
            d_ucs.add_noise_11classifer( program_options.per_cent_noise );
            plot_points_and_boundary( plot_canvas_with_noise, "Linear Regression for Classification Noisy Labeled Points with unknown f() illustrated",
                                      d_ucs, true,
                                      f_coefficients, f_exponents,
                                      false,
                                      program_options );
        }
        // ====== Calculate ordinary_least_squares ======
        W_lcb w_lcb_weights = VectorXd::Zero(program_options.vector_size);  // usually set to zero vector [0}
        size_t misclassified_samples = {0};
        // == Program_algorithm::lin_reg_4classification
        w_lcb_weights = ordinary_least_squares(d_ucs, program_options); // *** Run LR for one trial to calculate g(), ie. the weights. *** // mutates both variables.
        misclassified_samples = vector_vs_label_discrepancies( d_ucs, w_lcb_weights );
        plot_points_and_boundary( plot_canvas_final, "Linear Regression for Classification Final",
                                  d_ucs, true,
                                  w_lcb_weights, f_exponents,
                                  false, program_options );
        misclassifed_samples_e_in += misclassified_samples;
        // ====== Store OlS Results ======
        if ( program_options.measure_e_out ) {
            historical_f_lcs_w.push_back(f_coefficients);
            historical_f_exponents.push_back(f_exponents);
            historical_g_lcs_w.push_back(w_lcb_weights);
        }
        // === Log Single Trial Results ===
        BOOST_LOG_TRIVIAL(info)  << "ordinary_least_squares_procedure() Trial #: " <<rpt_field(trial_number)<< ", probability of error this trial: " << rpt_field( misclassified_samples / static_cast<double>(program_options.n_uc_num_samples));
        BOOST_LOG_TRIVIAL(debug) << "ordinary_least_squares_procedure() Boundary line equation, ie. the unknown weights of f(): " << f_coefficients.transpose() << " // Calculated weight vector: "<< w_lcb_weights.transpose();
    };
    // ====== Measure E_out Performance with New Data, optionally  ======
    size_t misclassified_samples_e_out = {0};
    if ( program_options.measure_e_out ) {
        for ( auto i = program_options.num_desired_trials; i-->0; ) {
            for ( auto j = program_options.num_desired_e_out_tests; j-->0; ) {
                D_ucs d_ucs_e_out(program_options);
                misclassified_samples_e_out += inter_vector_label_discrepancies(
                            d_ucs_e_out, historical_g_lcs_w[i], historical_f_lcs_w[i], historical_f_exponents[i]);
            }
        }
    }
    // ====== Report Single Trial Results ======
    report_e_in_out( program_options, "ordinary_least_squares_procedure"
                     , static_cast<double>(misclassifed_samples_e_in)
                        / (program_options.n_uc_num_samples * program_options.num_desired_trials)
                     , static_cast<double> (misclassified_samples_e_out)
                        / (program_options.n_uc_num_samples * program_options.num_desired_trials * program_options.num_desired_e_out_tests)
                     );
    return;
}

void lin_reg_4c_to_perceptron_procedure(const Program_options& program_options) {
    assert (program_options.vector_size > 1); // don't think we want to do regression with only the constant feature?  todo
    long error_probabilty_samples_total = {0};
    unsigned long perceptron_w_lcb_updates_total = {0};
    //    std::vector<W_lcb> historical_g_lcs_w = {}; // keep history of multible trials
    //    std::vector<W_lcb> historical_f_lcs = {};
    Gnuplot plot_canvas;
    for ( size_t trial_number=1; trial_number <= program_options.num_desired_trials; ++trial_number ) {
        D_ucs d_ucs = prepare_data_samples(program_options);
        /* In the case of linear models without transformation.
           For our starting unknown f(), the Decision Boundary equation of the linear boundary hyperplane.
           We will do the initial marking of our samples with this hyperplane.
           y = f(x1,x2) = m0*x0 + m1*x1 + m2*x2, where x0=1 which makes the m0*x0 term the line intercept constant.
           Example 1 f() is:
           boundary line:       m2*y =  -m1*x           + -m0*0.01  ie. the common y = ax + C form
           Green region is:     m2*y >= -m0x            + -m0*0.01
                                0 >=    -m2*y   + -m1*x + -m0*0.01
                                0 <=    +m2*y   - -m1*x - -m0*0.01
                                        + m0*x0 + m1*x1 + m2*x2 >= 0  where x0=1
           ( (m0, m1, m2) dot (1, x1, x2) ) >= 0

           Example 2 f() is:
           boundary line:       y =  -+sqrt(- x**2 + 0.6)
           Green region is:     y >= -+sqrt(- x**2 + 0.6)
                                y**2 >=     - x**2 + 0.6
                                0 >= - y**2 - x**2 + 0.6
                                0 <= + y**2 + x**2 - 0.6
                                + -1*0.6 + 1*x**2 + 1*y**2 >=0     ** = "to the power of"
                                + m0*x0**e0 + m1*x1**e1 + m2*x2**e2 >= 0
           ( (m0, m1, m2) dot (1, x1, x2) ) >= 0

           Set characteristics of our unknown f() ie. coefficients and exponents
        */
        // Create an unknown f() to classify and label points

        E_lcb f_exponents = VectorXd::Ones(program_options.vector_size); // typically ones.
        W_lcb f_coefficients(program_options.vector_size);  // no initialization done here.
        f_coefficients = select_random_unknown_boundary(program_options);

        /* if ( program_options.algorithm == Program_algorithm::transformed_linear_ols ) {
            assert( 3 == program_options.vector_size ); // below requires size 3 todo
            m0 = -0.4;         // Must be carefully specified to result in a sensible region.
            m1 = 1.0;        //
            m2 = 1.0;        //
            f_coefficients << m0, m1, m2;  // eigen also : << m0,m1,m2
            f_exponents << 1, 2, 2;        // only used: if algorithm == Program_algorithm::transformed_linear_ols
        } else {
            //            m0 = my_distribution2(my_random_device2);        // can be any line intercept constant that fits domain
            //            m1 = my_distribution2(my_random_device2);        // can be any slope.
            //            m2 = my_distribution2(my_random_device2);        // can be any slope
            assert( 3 == program_options.vector_size ); // below requires size 3 todo
            m0 = -.2;        // can be any line intercept constant
            m1 = 1.0;        // can be any slope.
            m2 = 1.0;        // can be any slope
            f_coefficients << m0, m1, m2;  // eigen also : << m0,m1,m2
        }
        */
        // Mark the samples with their true colors as we decided with our unknown f().
        std::for_each(d_ucs.begin(), d_ucs.end(), label_random_sample{f_coefficients, f_exponents});

        W_lcb w_lcb_weights = VectorXd::Zero(program_options.vector_size);  // usually set to zero vector [0]
        plot_points_and_boundary( plot_canvas, "Linear Regression (OLS) Labeled Points with unknown f() illustrated",
                                  d_ucs, true, f_coefficients, f_exponents, false, program_options );
        //        add_noise( d_ucs, per_cent_noise );

        w_lcb_weights = ordinary_least_squares(d_ucs, program_options); /* *** Run the OLS for one trial to calculate g(), ie. the weights. *** */ // mutates both variables.
                    /*if (algorithm == e_out_linear_ols_algorithm) {
                        historical_f_lcs.push_back(f_coefficients);
                        historical_g_lcs.push_back(w_lcb_weights);
                    }
                }
                else if ( algorithm == Program_algorithm::transformed_linear_ols ) {
                    w_lcb_weights = ordinary_least_squares(d_ucs, program_options);  // maybe this is part of the others similar todo
                } */

        /* Report Phase 1 Trial Results */
        BOOST_LOG_TRIVIAL(debug) << ">> Boundary line equation//unknown f(x): " << f_coefficients.transpose() << " //Phase 1 weight vector: "<< w_lcb_weights.transpose() << std::endl;
        // PROBABLE ERROR ON NEXT LINE todo
        double error_probabilty_samples = test_probability_of_errors( f_coefficients, w_lcb_weights, f_exponents, program_options );
        error_probabilty_samples_total += error_probabilty_samples;
        double probability_of_error = error_probabilty_samples / static_cast<double>(program_options.num_desired_propability_tests);  // Todo
        BOOST_LOG_TRIVIAL(debug) << ">> Trial #, probability of error: " <<trial_number<< ", " <<probability_of_error<<std::endl;

        plot_points_and_boundary( plot_canvas, "Linear Regression before Perceptron",
                                  d_ucs, true, w_lcb_weights, f_exponents, false, program_options );
//        W_lcb w_lcb_temp = w_lcb_weights;
//        w_lcb_weights = perceptron( d_ucs, w_lcb_weights, f_exponents, perceptron_w_lcb_updates, plot_canvas, program_options ); // *** Run the perceptron for one trial to calculate g(), ie. the weights. ***
//        plot_graph( plot_canvas, "Perceptron after Linear Regression", d_ucs, w_lcb_weights, f_exponents, program_options );

        // Report Phase 2 Trial Results

        W_lcb starting_w_lcb_weights = w_lcb_weights;  // set perceptron starting weights with calculated linear regression weights
        size_t perceptron_w_lcb_updates = 0;
        std::tie( w_lcb_weights, perceptron_w_lcb_updates ) = perceptron(d_ucs, starting_w_lcb_weights, 1, plot_canvas, program_options);  // Run the perceptron for one trial to calculate g(), ie. the weights.  // Report Phase 1 Trial Results
        perceptron_w_lcb_updates_total += perceptron_w_lcb_updates;
        BOOST_LOG_TRIVIAL(debug) << ">> Target f(x): " << f_coefficients.transpose() << ", Phase 1 weight vector: "<< w_lcb_weights.transpose();

        plot_points_and_boundary( plot_canvas, "Linear Regression to Perceptron Final",
                                  d_ucs, true, w_lcb_weights, f_exponents, false, program_options );
    };
    double error_probability_all_samples = error_probabilty_samples_total / static_cast<double> (program_options.num_desired_propability_tests * program_options. num_desired_trials); // better way ?? todo
    cout << ">>> Linear Regression OLS Result: Probability of error all trials: " << error_probability_all_samples << endl;

    double updates_per_trial = perceptron_w_lcb_updates_total / program_options.num_desired_trials;
    cout << ">>> Number of features/dimensions: " << program_options.vector_size-1 << std::endl;
    cout << ">>> Number of samples: " << program_options.n_uc_num_samples << std::endl;
    cout << ">>> Number of trials: " << program_options.num_desired_trials << std::endl;
    cout << ">>> Average updates per perceptron classification ("<< ( program_options.update_random_point_perceptron ? "random" : "first") <<"): " << updates_per_trial << std::endl;
    cout << ">>> Ratio of average updates over number of samples: " << updates_per_trial*1.0 / program_options.n_uc_num_samples*1.0 << std::endl;
    return;
}

void lin_reg_stoch_grad_descent_procedure(const Program_options& program_options){

}


