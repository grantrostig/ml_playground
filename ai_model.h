#ifndef AI_MODEL_H
#define AI_MODEL_H
//#include "main_cppheaders.h"
#include "main_eigen.h"
#include "features_n_label.h"
#include "ivector_of_labels.h"
#include "ifeature_matrix.h"

// ===========================================================================
// todo NOT USED YET

class   Vector_of_labels_data;
class   Matrix_data;
class   Idata_derived_type1;  // made-up to types used to compile :)
class   Idata_derived_type2;

class Iai_model_data  // modeled after Stroustrup, CPPPL Mountains , p 622 Ch 21.2.3
{
    std::string         name;           // RTTI? Idata_derived_type
    std::string         fq_path_name;   // todo violates open/closed principle?
    int                 data_generation_strategy;  // random, hard coded, user_provided
    virtual void pure_virtural_sentinel()=0;    // guideline_grostig
public:
//    Iai_model_data() {}
//    virtual ~Iai_model_data() {}                         // guideline_TCPL ss3.3.4 - should have one, but not necessarly empty

//    Iai_model_data(Iai_model_data const&)=delete;                 // guideline_TCPL ss3.3.4 else typically a disaster.
//    Iai_model_data(Iai_model_data &&)=delete;                     // guideline_TCPL ss3.3.4 else typically a disaster.
//    Iai_model_data& operator=(Iai_model_data const&)=delete;      // guideline_TCPL ss3.3.4 else typically a disaster.
//    Iai_model_data& operator=(Iai_model_data &&)=delete;          // guideline_TCPL ss3.3.4 else typically a disaster.

//    std::string getName() const;
//    void setName(const std::string &value);
//    std::string getFq_path_name() const;
//    void setFq_path_name(const std::string &value);
//    int getData_generation_strategy() const;
//    virtual void setData_generation_strategy(int value);

//    virtual void prepare_data (/* program_options */ void* const&) =0;

//    using Imatrix_data_tuple = std::tuple<Matrix_data, Ivector_of_labels<double>>;
//    virtual Imatrix_data_tuple get_features_n_labels (Idata_derived_type1 const&) const=0;

//    virtual Vector_of_labels_data get_features_n_labels (Idata_derived_type2 const&) const=0;

//    virtual void set_features_n_labels (Vector_of_labels_data const&)=0;
//    virtual void set_features_n_labels (Matrix_data const&, Ivector_of_labels<double> const*)=0;

//    virtual void get_features ()    const               =0;
//    virtual void set_features (Vector_of_labels_data const&) =0;
//    virtual void set_features (Matrix_data const&)     =0;
//    virtual void get_labels ()                          const   =0;
//    virtual void set_labels (Vector_of_labels_data const&)   =0;
//    virtual void set_labels (Ivector_of_labels<double> const*)  =0;  // only need to set labels if also setting features via Imatrix_data

//    virtual void next_sample (void* const&)     const   =0;
//    virtual void add_sample (void* const&)              =0;
//    virtual void clear (void* const&)           const   =0;
//    virtual void reset (void* const&)                   =0;
//    virtual void empty (void* const&)                   =0;
};

class Matrix_data: public Iai_model_data
{
    std::string         name;
    std::string         data_layout_style;  // flag to show it's type? RTTI?
//    Ifeature_matrix     feature_matrix;
//    Ivector_of_labels   vector_of_labels;
    void pure_virtural_sentinel() {};
public:
    Matrix_data() {}
    ~Matrix_data() {}
//    void prepare_data (void* const&) override;  // todo should I have virtual ?  Any effect other than documentation?  Doesn't override tell same story?
};

class Vector_of_vector_data: public Iai_model_data
{
    std::string         name;
    std::string         data_layout_style;  // flag to show it's type? RTTI?
    Vector_of_samples  vector_of_samples;  // one reference to one container of samples
public:
    Vector_of_vector_data() {}
    ~Vector_of_vector_data() {}
//    void prepare_data (void* const&) override;
};

// ===========================================================================

using AI_model_type = std::string;

class Iai_model  // approach to learning, including the algorithms and datastructures.
{
    std::string     name;       // some types are: linear, neural networks, SVM, nearest neighbors, RBF, gaussian processes, SVD, graphical models
    Iai_model_data *         data;       // todo use smart pointer.  also document precicely how to use to avoid avoid slicing
    virtual void pure_virtural_sentinel()=0;
public:
    Iai_model() {}
    ~Iai_model() {}

    Iai_model(Iai_model const&)=delete;                 // guideline_TCPL ss3.3.4 else typically a disaster.
    Iai_model(Iai_model &&)=delete;                     // guideline_TCPL ss3.3.4 else typically a disaster.
    Iai_model& operator=(Iai_model const&)=delete;      // guideline_TCPL ss3.3.4 else typically a disaster.
    Iai_model& operator=(Iai_model &&)=delete;          // guideline_TCPL ss3.3.4 else typically a disaster.

    virtual void prepare_data (Iai_model_data const&);
    virtual void set_model (AI_model_type const&);
    virtual void set_model ();
};

// ===========================================================================

//class Ifunction
//{
//    std::string                         name;
//    bool                                is_linear;
//    std::vector<double>                 exponents;
//    std::vector<std::experimental::any> functions;
//public:
//    Ifunction() {}
//    ~Ifunction() {}
//    virtual void prepare_function ();
//};

//// todo:  Do I need a class for Hypothesis set?  Can 'unknown target f' and 'hypotheis h and then g' be from a different 'hypothesis set'?

//class Ihypothesis: public Ifunction
//{
//    std::string                         name;
//    bool                                is_linear;
//    std::vector<double>                 exponents;
//    std::vector<std::experimental::any> functions;
//public:
//    Ihypothesis() {}
//    ~Ihypothesis() {}
//};

//class Itarget_function: public Ifunction
//{
//    std::string                         name;
//    bool                                is_linear;
//    std::vector<double>                 exponents;
//    std::vector<std::experimental::any> functions;
//public:
//    Itarget_function() {}
//    ~Itarget_function() {}
//};

//// ===========================================================================

//class Iai_method  //  No plan to implement at this time.
//{
//    std::string name;
//    std::vector<std::experimental::any> input_processing;
//    std::vector<std::experimental::any> validation;
//    std::vector<std::experimental::any> aggregation;
//    std::vector<std::experimental::any> regularization;
//public:
//    Iai_method() {}
//    ~Iai_method() {}
//};

//// ===========================================================================

//class Iai_technique  // Contains models and methods. Is this too high level?
//{
//    std::string name;
//    // models
//    // methods
//public:
//    Iai_technique() {}
//    ~Iai_technique() {}
//};



// ===========================================================================
// Paradigms:
// supervised
// unsupervised
// reinforcement
// active
// online
// ===========================================================================
// Theories:
// VC
// bias variance
// complexity
// bayesian


#endif // AI_MODEL_H
