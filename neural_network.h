#ifndef NEURAL_NETWORK_H
#define NEURAL_NETWORK_H
#include "utils.h"

void neural_network_procedure(const Program_options& program_options);

#endif // NEURAL_NETWORK_H
