/* .cpp

   Copyright (c) 2017 Grant Rostig. All rights reserved. Use by written permission only, evidenced with blue ink on paper or a verifyable (pgp) email interaction with Grant Rostig.
   Permission is granted to use these files during and in support of CppMSG meetup group events and projects.

   Inspired by: -

   Shows how to:
   -
   -
   -
*/
#include "main_boost.h"
#include "bias_variance.h"
#include "ml_utils.h"
#include "lin_reg.h"

void plot_graph_bias_variance( Gnuplot& gp, const std::string title, const D_ucs& d_ucs, const W_lcb& w_lcb, const E_lcb& f_exponents, const Program_options& program_options ) {
/*     Plot the current samples and classifier boundary line
     https://github.com/matplotlib/matplotlib/issues/7684/
     https://stackoverflow.com/questions/30002058/gnuplot-3d-plot-scatter-points-and-a-surface-and-lines-between-the-them
     https://i.stack.imgur.com/pnthj.png
     http://gnuplot.sourceforge.net/demo/scatter.html
     http://psy.swansea.ac.uk/staff/carter/gnuplot/gnuplot_3d.htm
     http://gnuplot.sourceforge.net/demo/surface1.html
*/
    std::vector<std::tuple<double,double> > green_points;
    for (auto i : d_ucs){
        green_points.push_back(std::make_tuple(i.x_lcb[0], i.y_true_sample_label));
    }
    gp << "set title '"<< title <<"'\n set size ratio -1\n";
    gp << "set xlabel 'x_1'\n";
    gp << "set ylabel 'x_2'\n";
    gp << "set xrange [-1:3]\n";
    gp << "set yrange [-1:1]\n";
    gp << "plot '-' with points title 'pass:' ";
    // *** Plot the lines ***
    gp << std::setprecision(2) << std::fixed
       << ", sin("
       << "pi*x**"<< f_exponents[0] // update this to use coefficient not pi. todo
       << ") ";
    gp << " ";
    // then: plot the g() line
    gp << std::setprecision(2) << std::fixed
       << ", "
       << w_lcb[0]<<"*x**"<< f_exponents[0];
    gp << " \n";
/*
    else if ( algorithm == Program_algorithm::transformed_linear_ols ) {
        gp << std::setprecision(2) << std::fixed
           << ", sqrt("
           << roundp2(-1.0*w_lcb[1])<<"*x**"<< f_exponents[1]
           << "+"<<roundp2(-1*w_lcb[0])
           << ") ";
        cout << std::setprecision(2) << std::fixed
           << ", sqrt("
           << roundp2(-1.0*w_lcb[1])<<"*x**"<< f_exponents[1]
           << "+"<<roundp2(-1*w_lcb[0])
           << ") ";
        gp << std::setprecision(2) << std::fixed
           << ", -sqrt("
           << roundp2(-1.0*w_lcb[1])<<"*x**"<< f_exponents[1]
           << "+"<<roundp2(-1*w_lcb[0])
           << ") \n";
        cout << std::setprecision(2) << std::fixed
           << ", -sqrt("
           << roundp2(-1.0*w_lcb[1])<<"*x**"<< f_exponents[1]
           << "+"<<roundp2(-1*w_lcb[0])
           << ") \n";
    } else {
        //       <<-m1/m2<<"*x+"<<-1*m0/m2<<" , "
        gp << std::setprecision(2) << std::fixed
           << roundp2(-w_lcb[1]/w_lcb[2])<<"*x+"<<roundp2(-1*w_lcb[0]/w_lcb[2])<<" \n";
    }
*/
    gp.send1d(green_points);  // now plot the points
    gp.flush();  // may not be necessary
    usleep(program_options.sleep_micro_seconds); /* is there a more modern way? todo */
}

double calc_g_mean(const std::vector<W_lcb>& historical_g_lcs_w) {
    double g_sum = 0;
//    for ( size_t i=0; i<program_options.num_desired_trials; ++i ) {
//        W_lcb temp = historical_g_lcs_w[i];
//        g_sum += temp(0);
//    }
    for ( auto i:historical_g_lcs_w ) g_sum += i(0);  // todo only the first element?  bug?  todo is there some eigen sum to get this if it is correct?
    return g_sum / historical_g_lcs_w.size();
}

double calc_bias(const std::vector<D_ucs>& historical_d_ucs, const double& g_mean) {
    size_t d_size = historical_d_ucs[0].size();
    size_t num_trials = historical_d_ucs.size();
    double bias = 0;
    for ( auto my_d_ucs:historical_d_ucs ) {  // calc bias_average for all trials
        for (auto i:my_d_ucs) {  // calc bias for one trial
            X_lcb my_x_lcb = i.x_lcb;
            double g_mean_of_x = g_mean * my_x_lcb[0];
            bias += pow( (g_mean_of_x - i.y_true_sample_label), 2);
        }
    }
    double bias_average = bias / ( d_size * num_trials );  // Page 62 LFD
    return bias_average;
}

double calc_variance(const std::vector<D_ucs>& historical_d_ucs, const std::vector<W_lcb>& historical_g_lcs_w, const double& g_mean) {
    size_t d_size = historical_d_ucs[0].size();
    size_t num_trials = historical_g_lcs_w.size();
    double variance = 0;
    size_t i = 0;
    for ( auto my_d_ucs:historical_d_ucs ) {
        W_lcb my_g_lcb_w  = historical_g_lcs_w[i++];  // todo better way to do this without i++?
        double g_x0_w     = my_g_lcb_w[0];
        for ( auto my_sample:my_d_ucs ) {  // calc variance for one trial
            double g_d_of_x =    g_x0_w * my_sample.x_lcb[0];
            double g_mean_of_x = g_mean * my_sample.x_lcb[0];
            variance +=          pow( ( g_d_of_x - g_mean_of_x), 2 );  // Page 64 LFD
        }
    }
    double variance_average = variance / ( d_size * num_trials );
    return variance_average;
}

void lin_reg_bias_variance_procedure(const Program_options& program_options) {
    BOOST_LOG_TRIVIAL(info) << "bias_variance_analysis_ols";
    std::vector<W_lcb> historical_g_lcs_w = {}; // keep history of multible trials
    std::vector<C_lcb> historical_f_lcs   = {};
    std::vector<D_ucs> historical_d_ucs   = {};
    Gnuplot            plot_canvas;
    for ( auto i = program_options.num_desired_trials; i-->0; ) {
        D_ucs d_ucs =          prepare_data_samples(program_options); // Build data set with initial size.
        W_lcb f_coefficients = VectorXd::Zero(program_options.vector_size);  // eigen also : << m0,m1,m2
        E_lcb ones =           VectorXd::Ones(program_options.vector_size);
        W_lcb w_lcb_weights =  VectorXd::Zero(program_options.vector_size);  // usually set to zero vector [0]
        //               *** Mark the samples ***
        auto label_sample_lambda = [](X_lcb_sample &sample) {
            sample.y_true_sample_label_double = std::sin(boost::math::constants::pi<double_t>() * sample.x_lcb[0]);
            BOOST_LOG_TRIVIAL(trace) << " y_sample_color: " << sample.y_true_sample_label;
        };
        std::for_each(d_ucs.begin(), d_ucs.end(), label_sample_lambda);
        plot_graph_bias_variance( plot_canvas, "points on f(x)", d_ucs, f_coefficients, ones, program_options );  // f_exponents not implemented todo note
        //              *** Find fn() ***
        w_lcb_weights = ordinary_least_squares2(d_ucs, program_options);
        plot_graph_bias_variance( plot_canvas, "points on f(x)", d_ucs, w_lcb_weights,  ones, program_options );  // f_exponents not implemented todo note
        historical_g_lcs_w.push_back(w_lcb_weights);
        historical_d_ucs.push_back(d_ucs);
    }
    double g_mean =             calc_g_mean(   historical_g_lcs_w );
    double bias_average =       calc_bias(     historical_d_ucs,   g_mean );
    double variance_average =   calc_variance( historical_d_ucs,   historical_g_lcs_w,  g_mean );
    cout << "\n g_mean, bias_average, variance_average, expected value of out-of-sample error: " << g_mean << ", " << bias_average << ", " << variance_average <<", "<< bias_average+variance_average;
    return;
}


