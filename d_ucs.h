/* .h

   Copyright (c) 2017 Grant Rostig. All rights reserved. Use by written permission only, evidenced with blue ink on paper or a verifyable (pgp) email interaction with Grant Rostig.
   Permission is granted to use these files during and in support of CppMSG meetup group events and projects.

   Inspired by: Caltech LFD

   Shows how to:
   -
   -
   -

// #include "../gsl/gsl"   // Standard C++ Foundation: C++14 Guidelines Support Library

// Variable naming convention to correspond to mathematical use of fonts.
// lc=Lower Case, uc=Upper Case, s=script (ie.not block characters), b=bold
*/
#ifndef D_UCS_H
#define D_UCS_H

//#pragma once // not using here.
//#include "main_cppheaders.h"
#include "main_eigen.h"
#include "X_lcb_sample.h"

class D_ucs {
    std::vector<X_lcb_sample> x_lcb_samples /*= {0}*/;   // why not?  todo
public:
    explicit D_ucs(Program_options const& program_options);
//    ~D_ucs2(); // to have or not to have ?? todo

     // Explicit Defaults: the 6 default constructed functions that are created by the compiler. CPL 17.6.1
       D_ucs() =                            default;             // constructor
       ~D_ucs() noexcept =                  default;             // destructor todo: consequences of noexcept?  sutter uses/ stroustrup doesn't in on example.
       D_ucs(const D_ucs&) =                default;             // copy constructor
       D_ucs(D_ucs&&) =                     default;             // move constructor ?? todo
       D_ucs& operator=(const D_ucs&) =     default;             // copy assignment ?? todo
       //D_ucs2& operator=(const D_ucs2&&) = default;            // move assignment ?? todo    // won't compile with const, why did sutter include const?? bug on his part?
       D_ucs& operator=(D_ucs&&) =        default;               // move assignment ?? todo

    size_t                                      size();
    size_t                                      size() const;  // do we generally need this or both??  todo

    X_lcb_sample                                get(size_t& i) const;
    void                                        set(size_t& i, X_lcb_sample& v);
    X_lcb_sample&                               operator[](size_t i);
    const X_lcb_sample&                         operator[](size_t i) const;  // is this sensible??? todo
//    void                                        add();     // like push_back()

    std::vector<X_lcb_sample>::iterator         erase( std::vector<X_lcb_sample>::iterator begin);
    std::vector<X_lcb_sample>::iterator         erase( std::vector<X_lcb_sample>::iterator begin, std::vector<X_lcb_sample>::iterator end);
//          *** creating an iterator therefore using "partial delegation"
//          instead of using "full delegation" to demonstrate implementation ***
//    class                                       D_ucs_iterator;  // should I have used a "friend class" instead?? not using "reisbeck's iterator from iterator pattern" yet todo
    std::vector<X_lcb_sample>::iterator         begin();  // appears to be a long, why not size_t?  todo
    std::vector<X_lcb_sample>::const_iterator   begin() const;  // no definion yet.
    std::vector<X_lcb_sample>::iterator         end();
    std::vector<X_lcb_sample>::const_iterator   end() const;  // no definion yet.
//    std::vector<X_lcb_sample>::iterator cbegin();
//    std::vector<X_lcb_sample>::iterator cbegin() const;
//    std::vector<X_lcb_sample>::iterator cbegin();
    void add_noise_11classifer(double const& per_cent_noise);  // add noise where labels are +1/-1 by flipping their sign
};
/*
//class D_ucs2::D_ucs_iterator {
//    std::vector<X_lcb_sample>::iterator         current_d_ucs_iterator;
//public:
//    using Underlying_type = std::vector<X_lcb_sample>;  // not used in templated version // is this trash?
//#define Underlying_type std::vector<X_lcb_sample>
// //    using self = D_ucs_iterator<Underlying_type>;
//    using self = Underlying_type;
//    using value_type = std::iterator_traits<std::vector::iterator>::value_type;
//    using reference = std::iterator_traits<Underlying_type>::reference;
//    using pointer = std::iterator_traits<Underlying_type>::pointer;
//    using difference_type = std::iterator_traits<Underlying_type>::difference_type;
//    using iterator_category = std::iterator_traits<Underlying_type>::iterator_category;
    // std::random_access_iterator  // where do I show this? todo

//    D_ucs_iterator():
//        current_d_ucs_iterator{my_pointer} {}  // initialize or  = delete; this?? todo
//    D_ucs_iterator(X_lcb_sample* my_pointer):
//        current_d_ucs_iterator{my_pointer} {}
// //    ~iterator();  // to have or not to have ?? todo

// //  iterator&                           operator=();  // never defined for an iterator?? todo
//    std::vector<X_lcb_sample>::iterator operator*() const;
//    X_lcb_sample&                       operator->() const;
//    D_ucs_iterator&                     operator++();
//    D_ucs_iterator                      operator++(int);  // why not size_t?? compile error todo
//    D_ucs_iterator&                     operator--();
//    D_ucs_iterator                      operator--(int);

//    ptrdiff_t                           operator-(const D_ucs_iterator&) const;
//    D_ucs_iterator                      operator+(ptrdiff_t) const;
//    D_ucs_iterator                      operator-(ptrdiff_t) const;

//    bool                                operator==(const D_ucs_iterator& rhs) const;
//    bool                                operator!=(const D_ucs_iterator& rhs) const;
//    bool                                operator<(const D_ucs_iterator& rhs) const;
//    bool                                operator<=(const D_ucs_iterator& rhs) const;
//    bool                                operator>(const D_ucs_iterator& rhs) const;
//    bool                                operator>=(const D_ucs_iterator& rhs) const;
//}
*/

#endif // D_UCS_H
