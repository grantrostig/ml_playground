#include "reporting.h"

std::string rpt_separator( char separator_char ) {  // convert a char into a string. todo better way to do this
//    std::string c = {separator_char};
    return {separator_char};  // c++17 feature I think - initializer converted to string
}

//template <typename T>  // must pass in formatted field, only adds separator
std::string rpt_custom_cell( std::string const& data,
                             char separator_char ) {
    std::stringstream formatted_element;  // TODO get rid of stringstream  - not needed anymore.
    formatted_element << data << separator_char;
    return formatted_element.str();
}

void report_parameters( Program_options const& program_options) {
    cout << "======================== Program Parameter - Report ============================= " << endl;
    cout << program_options;
    cout << "==================================================================================" << endl;
}

//void error_report( Program_options const& program_options,
//                   std::vector<size_t> const& e_in, std::vector<size_t> const& e_out ) {
//    cout << " ====== Likelyhood of Algorithm making an Error - Report ***" << endl;
//    cout << endl;
//    cout << "=========================================" << endl;
//}

void report_e_in_out( Program_options const& program_options, std::string const& description, double const e_in, double const e_out ) {
    cout << "======================== E_in & E_out Report from " << description << "======" << endl;
    cout << rpt_separator('|')
         << rpt_cell("e_in",20)
         << rpt_cell("e_out",20)
         << rpt_cell("out/in ratio",20)
         << rpt_cell("in/out ratio",20)
         << rpt_cell("e_in tests",20)
         << rpt_cell("e_out tests",20)
         << endl;
    cout << rpt_separator('|')
         << rpt_cell(e_in,20)
         << rpt_cell(e_out,20)
         << rpt_cell(e_out/e_in,20)
         << rpt_cell(e_in/e_out,20)
         << rpt_cell(program_options.n_uc_num_samples * program_options.num_desired_trials,20)
         << rpt_cell(program_options.n_uc_num_samples * program_options.num_desired_trials * program_options.num_desired_e_out_tests,20)
                            // << rpt_custom_cell(rpt_field(static_cast<int>(d.algorithm),20))  // alternative use
         << endl;
    cout << "==================================================================================" << endl;
}

/* ===== prior work and still usefull for debugging.

//void test_reporting() {
//    const int i = {878787};
//        cout << ">" << pf_cell(i) << "<" << endl;
//    long l = 99;
//        cout << ">" << pf_cell(l) << "<" << endl;
//    const size_t st = 101;  // works either way, const or not todo
//    cout << ">" << rpt_cell(st) << "<" << endl;
//    cout << ">" << rpt_field(st) << "<" << endl;

//    const double d = 1.12345678900000001;
//    cout << ">" << rpt_cell(d) << "<" << endl;
//    cout << ">" << rpt_field2(d) << "<" << endl;
//    cout << ">" << rpt_field2(d, 15) << "<" << endl;


//    const std::string ss = "<this is my_string>";
//    cout << ">" << pf_field(ss) << "<" << endl;
//    cout << ">" << pf_cell(ss) << "<" << endl;
//    const std::string ss2 = "<my_string>";
//    cout << ">" << pf_field(ss2) << "<" << endl;
//    cout << ">" << pf_cell(ss2) << "<" << endl;

//    Program_options p;
//    cout << p << endl;
//    cout << ">" << rpt_field(p.n_uc_num_samples) << "<" << endl;
//}

//std::string rpt_field( int const& data,
////             int const& width = 7,
////             std::ios_base::fmtflags alignment = std::ios_base::right,
////             char fill_char = ' ',
////             std::ios_base::fmtflags number_base = std::ios_base::dec,  // dec, oct, hex
////             std::ios_base::fmtflags show_positive = std::ios_base::showpos
//                                   int const& width,
//                                   std::ios_base::fmtflags alignment,
//                                   char fill_char,
//                                   std::ios_base::fmtflags number_base,  // dec, oct, hex
//                                   std::ios_base::fmtflags show_positive

//        )
//{
//    std::stringstream formatted_element;
//    formatted_element.setf(alignment);
//    formatted_element.setf(number_base);
//    formatted_element.setf(show_positive);
//    formatted_element << std::showbase
//                      << std::setw(width)
//                      << std::setfill(fill_char)
//                      << data
//                      << std::noshowbase // should add option for this whenever... todo
//                      << std::noshowpos // should add option for this whenever... todo
//                      << std::noshowpoint;  // should add option for this whenever... todo
//    return formatted_element.str();
//}

//std::string rpt_field( size_t const& data,
////                      int const& width = 7,
////                      std::ios_base::fmtflags alignment = std::ios_base::right,
////                      char fill_char = ' ',
////                      std::ios_base::fmtflags number_base = std::ios_base::dec
//                      int const& width,
//                      std::ios_base::fmtflags alignment,
//                      char fill_char,
//                      std::ios_base::fmtflags number_base
//        )
//{
//    std::stringstream formatted_element;
//    formatted_element.setf(alignment);
//    formatted_element.setf(number_base);
//    formatted_element << std::showbase
//                      << std::setw(width)
//                      << std::setfill(fill_char)
//                      << data
//                      << std::noshowbase;
//    return formatted_element.str();
//}

//std::string rpt_field( double const& data,
//                      int const& width,
//                      std::ios_base::fmtflags alignment,
//                      char fill_char,
//                      int const& precision,
//                      std::ios_base::fmtflags floating_p_format  // fixed, scientific, defaultfloat
////             int const& width = 10,
////             std::ios_base::fmtflags alignment = std::ios_base::right,
////             char fill_char = ' ',
////             int const& precision = 4,
////             std::ios_base::fmtflags floating_p_format = std::ios_base::fixed  // fixed, scientific, defaultfloat
//        )
//{
//    std::stringstream formatted_element;
//    formatted_element.setf(alignment);
//    formatted_element.setf(floating_p_format);
//    formatted_element << std::setw(width)
//                     << std::setfill(fill_char)
//                     << std::setprecision(precision)
//                     << data;
//    return formatted_element.str();
//}

//std::string rpt_field( std::string const& data,
////             int const& width = 15,
////             std::ios_base::fmtflags alignment = std::ios_base::right,
////             char fill_char = '_'

//        int const& width,
//        std::ios_base::fmtflags alignment,
//        char fill_char
//        )
//{
//    std::stringstream formatted_element;
//    formatted_element.setf(alignment);
//    formatted_element << std::setw(width)
//                     << std::setfill(fill_char)
//                     << data;
//    return formatted_element.str();
//}

//std::string rpt_field2( double const& data,  // need to make a template for these.  todo
//                        //             int const& width7,
//                        //             std::ios_base::fmtflags alignment,
//                        //             char fill_char,
//                        //             std::ios_base::fmtflags number_base,  // dec, oct, hex
//                        //             std::ios_base::fmtflags show_positive
//                        int const& width,
//                        std::ios_base::fmtflags alignment,
//                        char fill_char,
//                        int const& precision,
//                        std::ios_base::fmtflags floating_p_format,  // fixed, scientific, defaultfloat
//                        std::ios_base::fmtflags number_base,  // dec, oct, hex
//                        std::ios_base::fmtflags show_positive
//        ) {
//    std::stringstream formatted_element;
//    formatted_element.setf(floating_p_format);
//    formatted_element.setf(alignment);
//    formatted_element.setf(number_base);
//    formatted_element.setf(show_positive);
//    formatted_element << std::showbase
//                      << std::setw(width)
//                      << std::setfill(fill_char)
//                      << std::setprecision(precision)
//                      << data
//                      << std::noshowbase // should add option for this whenever... todo
//                      << std::noshowpos // should add option for this whenever... todo
//                      << std::noshowpoint;  // should add option for this whenever... todo
//    return formatted_element.str();
//}
*/
