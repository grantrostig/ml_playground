# How to run the program: ./ml_playground --algorithm lin_reg_4class_transformed --update_random_point_perceptron false --lrt_transform_algorithm using_2nd_order_polynomial --n_uc_num_samples 30 --num_desired_trials 40 --per_cent_noise 0 --num_desired_propability_tests 10 --show_plot_graph false --diagnostic_logging error --measure_e_in true --measure_e_out true --num_desired_e_out_tests 1
TEMPLATE = app
CONFIG -= app_bundle
CONFIG -= qt
CONFIG += console c++17  # places gnu++1z on compile line for some strange reason!
QMAKE_CXXFLAGS += \
#            -std=c++17 \
#            -std=c++1z \
#            -std=gnu++17 \
#            -std=gnu++1z \
#            -std=c++2a \
#            -std=gnu++2a \
#            -pedantic-errors \
#            -pedantic \
#            -Weffc++ \
            -Wall \
            -Wno-unused-parameter \
#            -fconcepts \
#            -DBOOST_SYSTEM_NO_DEPRECATED \  # not sure what it does.
            -DBOOST_LOG_DYN_LINK  # on compile line not link line.
#SUBDIRS +=
LIBS += -pthread  # to avoid some link error on Fedora26
# boost on _fedora26_ parial list only
LIBS += \
#        -lboost_atomic \
#        -lboost_chrono \
#        -lboost_context \
#        -lboost_coroutine \
#        -lboost_date_time \
#        -lboost_fiber \
#        -lboost_filesystem \
#        -lboost_graph_parallel \
#        -lboost_graph \
        -lboost_iostreams \
#        -lboost_locale \
#        -lboost_log_setup \
        -lboost_log \
#        -lboost_math_c99f \
#        -lboost_math_c99l \
#        -lboost_math_c99 \
#        -lboost_math_tr1f \
#        -lboost_math_tr1l \
#        -lboost_math_tr1 \
#        -lboost_mpi_python-py27 \
#        -lboost_mpi_python-py35 \
#        -lboost_mpi_python \
#        -lboost_mpi \
#        -lboost_prg_exec_monitor \
        -lboost_program_options \
#        -lboost_python-py27 \
#        -lboost_python-py35 \
#        -lboost_python \
#        -lboost_random \
#        -lboost_regex \
#        -lboost_serialization \
#        -lboost_signals \
        -lboost_system \
#        -lboost_thread \
#        -lboost_timer \
#        -lboost_type_erasure \
#        -lboost_unit_test_framework \
#        -lboost_wave \
#        -lboost_wserialization

# boost on _ubuntu_17.04_
#LIBS += \
#        -lboost_atomic \
#        -lboost_chrono \
#        -lboost_context \
#        -lboost_coroutine \
#        -lboost_date_time \
#        -lboost_fiber \
#        -lboost_filesystem \
#        -lboost_graph_parallel \
#        -lboost_graph \
#        -lboost_iostreams \
#        -lboost_locale \
#        -lboost_log_setup \
#        -lboost_log \
#        -lboost_math_c99f \
#        -lboost_math_c99l \
#        -lboost_math_c99 \
#        -lboost_math_tr1f \
#        -lboost_math_tr1l \
#        -lboost_math_tr1 \
#        -lboost_mpi_python-py27 \
#        -lboost_mpi_python-py35 \
#        -lboost_mpi_python \
#        -lboost_mpi \
#        -lboost_prg_exec_monitor \
#        -lboost_program_options \
#        -lboost_python-py27 \
#        -lboost_python-py35 \
#        -lboost_python \
#        -lboost_random \
#        -lboost_regex \
#        -lboost_serialization \
#        -lboost_signals \
#        -lboost_system \
#        -lboost_thread \
#        -lboost_timer \
#        -lboost_type_erasure \
#        -lboost_unit_test_framework \
#        -lboost_wave \
#        -lboost_wserialization

mac:{
INCLUDEPATH += /usr/local/opt/boost/include \
#               /usr/local/opt/eigen/include
               # /usr/include/eigen3/
                /usr/include/

LIBS -= -lboost_log

LIBS += -L/usr/local/opt/boost/lib/ \
        -lboost_log-mt
}

SOURCES += main.cpp \
    X_lcb_sample.cpp \
    bias_variance.cpp \
    utils.cpp \
    ml_utils.cpp \
    perceptron.cpp \
    test_cases.cpp \
    d_ucs.cpp \
    random_toolkit.cpp \
    perceptron_multi_layer.cpp \
    neural_network.cpp \
    lin_reg.cpp \
    reporting.cpp \
    program_options.cpp \
    logging.cpp \
    main_gnuplot.cpp \
    data_samples_matrix.cpp \
    data_sample.cpp \
    ai_model.cpp \
    features_n_label.cpp \
    ifeature_matrix.cpp \
    ivector_of_labels.cpp \
    hoeffding_inequality.cpp

HEADERS += \
    main.h \
    ../grostig_tools/cpp_headers.h \
    ../grostig_tools/boost_headers.h \
    ../grostig_tools/gnuplot-iostream.h \
    X_lcb_sample.h \
    bias_variance.h \
    utils.h \
    ml_utils.h \
    perceptron.h \
    test_cases.h \
    d_ucs.h \
    random_toolkit.h \
    perceptron_multi_layer.h \
    main_typedefs.h \
    main_eigen.h \
    main_gnuplot.h \
    main_boost.h \
    main_cppheaders.h \
    neural_network.h \
    lin_reg.h \
    reporting.h \
    program_options.h \
    logging.h \
    data_samples_matrix.h \
    data_sample.h \
    ai_model.h \
    features_n_label.h \
    ifeature_matrix.h \
    ivector_of_labels.h \
    hoeffding_inequality.h

DISTFILES += \
    LICENSE.md \
    README.md

