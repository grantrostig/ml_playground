/* .h

   Copyright (c) 2017 Grant Rostig. All rights reserved. Use by written permission only, evidenced with blue ink on paper or a verifyable (pgp) email interaction with Grant Rostig.
   Permission is granted to use these files during and in support of CppMSG meetup group events and projects.

   Inspired by: Caltech LFD

   Shows how to:
   -
   -
   -

// #include "../gsl/gsl"   // Standard C++ Foundation: C++14 Guidelines Support Library

// Variable naming convention to correspond to mathematical use of fonts.
// lc=Lower Case, uc=Upper Case, s=script (ie.not block characters), b=bold
*/
#ifndef PERCEPTRON_H
#define PERCEPTRON_H
//#include "main_cppheaders.h"
#include "utils.h"
#include "ml_utils.h"
#include "d_ucs.h"

//using D_ucs = std::vector<X_lcb_sample>;

std::tuple<W_lcb, unsigned long> perceptron( D_ucs& d_ucs,
                                             W_lcb const& w_lcb_weights_starting,
                                             size_t num_trials,
                                             Gnuplot& plot_canvas,
                                             const Program_options& program_options);

void perceptron_procedure(const Program_options& program_options);

#endif // PERCEPTRON_H
