#ifndef IFEATURES_N_LABEL_H
#define IFEATURES_N_LABEL_H
//#include "main_cppheaders.h"
#include "main_eigen.h"
#include "utils.h"

class Ifnl_features_vector    // fnl means: 'features and labels'
{
    std::string name;
    // derived class holds the features
    virtual void pure_virtural_sentinel()=0;
public:
    Ifnl_features_vector() {}
    ~Ifnl_features_vector() {}

    Ifnl_features_vector(Ifnl_features_vector const&)=delete;                 // guideline_TCPL ss3.3.4 else typically a disaster.
    Ifnl_features_vector(Ifnl_features_vector &&)=delete;                     // guideline_TCPL ss3.3.4 else typically a disaster.
    Ifnl_features_vector& operator=(Ifnl_features_vector const&)=delete;      // guideline_TCPL ss3.3.4 else typically a disaster.
    Ifnl_features_vector& operator=(Ifnl_features_vector &&)=delete;          // guideline_TCPL ss3.3.4 else typically a disaster.

    std::string getName() const;
    void setName(const std::string &value);
};

// features vector specialization
// float
class Ifloat_fnl_features_vector: public Ifnl_features_vector
{
    std::vector<float> features_vector;
public:
    Ifloat_fnl_features_vector() {}
    ~Ifloat_fnl_features_vector() {}

//    Iai_model_data(Iai_model_data const&)=delete;                 // guideline_TCPL ss3.3.4 else typically a disaster.
//    Iai_model_data(Iai_model_data &&)=delete;                     // guideline_TCPL ss3.3.4 else typically a disaster.
//    Iai_model_data& operator=(Iai_model_data const&)=delete;      // guideline_TCPL ss3.3.4 else typically a disaster.
//    Iai_model_data& operator=(Iai_model_data &&)=delete;          // guideline_TCPL ss3.3.4 else typically a disaster.

    virtual void set_model ();
    std::vector<float> getFeatures_vector() const;
    void setFeatures_vector(const std::vector<float> &value);

    friend std::ostream& operator<<(std::ostream & os, Ifloat_fnl_features_vector const& d);
};

// double
class Idouble_fnl_features_vector: public Ifnl_features_vector
{
    std::vector<double> features_vector;
public:
    Idouble_fnl_features_vector() {}
    ~Idouble_fnl_features_vector() {}

//    Iai_model_data(Iai_model_data const&)=delete;                 // guideline_TCPL ss3.3.4 else typically a disaster.
//    Iai_model_data(Iai_model_data &&)=delete;                     // guideline_TCPL ss3.3.4 else typically a disaster.
//    Iai_model_data& operator=(Iai_model_data const&)=delete;      // guideline_TCPL ss3.3.4 else typically a disaster.
//    Iai_model_data& operator=(Iai_model_data &&)=delete;          // guideline_TCPL ss3.3.4 else typically a disaster.

    virtual void set_model ();
    std::vector<double> getFeatures_vector() const;
    void setFeatures_vector(const std::vector<double> &value);
};

// ===========================================================================

class Ifnl_label_scalar  // fnl means: 'features and labels'
{
    std::string name;
    // derived class holds the scalar
    virtual void pure_virtural_sentinel()=0;
public:
    Ifnl_label_scalar() {};
    ~Ifnl_label_scalar() {}

//    Iai_model_data(Iai_model_data const&)=delete;                 // guideline_TCPL ss3.3.4 else typically a disaster.
//    Iai_model_data(Iai_model_data &&)=delete;                     // guideline_TCPL ss3.3.4 else typically a disaster.
//    Iai_model_data& operator=(Iai_model_data const&)=delete;      // guideline_TCPL ss3.3.4 else typically a disaster.
//    Iai_model_data& operator=(Iai_model_data &&)=delete;          // guideline_TCPL ss3.3.4 else typically a disaster.

    virtual void getLabel() const;  // todo label only exists in derived types, but I know all derived types must have these methods.
    virtual void setLabel();
    std::string getName() const;
    void setName(const std::string &value);
};

// scalar_label specializations, 3 numeric types so far....
// double
class Idouble_fnl_label_scalar: public Ifnl_label_scalar    // fnl means: 'features and labels'
{
    double label;
public:
    Idouble_fnl_label_scalar() {}
    ~Idouble_fnl_label_scalar() {}

//    Iai_model_data(Iai_model_data const&)=delete;                 // guideline_TCPL ss3.3.4 else typically a disaster.
//    Iai_model_data(Iai_model_data &&)=delete;                     // guideline_TCPL ss3.3.4 else typically a disaster.
//    Iai_model_data& operator=(Iai_model_data const&)=delete;      // guideline_TCPL ss3.3.4 else typically a disaster.
//    Iai_model_data& operator=(Iai_model_data &&)=delete;          // guideline_TCPL ss3.3.4 else typically a disaster.

//    double getLabel() const;
    void setLabel(double value);
};

// float
class Ifloat_fnl_label_scalar: public Ifnl_label_scalar    // fnl means: 'features and labels'
{
    float label;
public:
    Ifloat_fnl_label_scalar() {}
    ~Ifloat_fnl_label_scalar() {}
//    Iai_model_data(Iai_model_data const&)=delete;                 // guideline_TCPL ss3.3.4 else typically a disaster.
//    Iai_model_data(Iai_model_data &&)=delete;                     // guideline_TCPL ss3.3.4 else typically a disaster.
//    Iai_model_data& operator=(Iai_model_data const&)=delete;      // guideline_TCPL ss3.3.4 else typically a disaster.
//    Iai_model_data& operator=(Iai_model_data &&)=delete;          // guideline_TCPL ss3.3.4 else typically a disaster.

//    float getLabel() const;
    void setLabel(float value);
};

// int
class Iint_fnl_label_scalar: public Ifnl_label_scalar    // fnl means: 'features and labels'
{
    int label;
public:
    Iint_fnl_label_scalar() {}
    ~Iint_fnl_label_scalar() {}
//    Iai_model_data(Iai_model_data const&)=delete;                 // guideline_TCPL ss3.3.4 else typically a disaster.
//    Iai_model_data(Iai_model_data &&)=delete;                     // guideline_TCPL ss3.3.4 else typically a disaster.
//    Iai_model_data& operator=(Iai_model_data const&)=delete;      // guideline_TCPL ss3.3.4 else typically a disaster.
//    Iai_model_data& operator=(Iai_model_data &&)=delete;          // guideline_TCPL ss3.3.4 else typically a disaster.

//    int getLabel() const;
    void setLabel(int value);
};

// ===========================================================================

class Features_n_label  // memory organized for row wise operations
{
    std::string name;
    Ifnl_features_vector * features;  // one sample has one or more features
    Ifnl_label_scalar    * scalar;
public:
    Features_n_label/*<T>*/() {}  // todo : T needed or not?
    ~Features_n_label() {}
    std::string getName() const;
    void setName(const std::string &value);
//    Ifnl_features_vector getFeatures() const;
    void setFeatures(const Ifnl_features_vector &value);
//    Ifnl_label_scalar getScalar() const;
    void setScalar(const Ifnl_label_scalar &value);
};

// ===========================================================================

class Vector_of_samples  // memory organized for row wise operations
{
    std::string name;
    std::vector<Features_n_label> vector_of_samples;
public:
    Vector_of_samples/*<T>*/() {}  // todo : T needed or not?
    ~Vector_of_samples() {}
    std::string getName() const;
    void setName(const std::string &value);
    std::vector<Features_n_label> getVector_of_samples() const;
    void setVector_of_samples(const std::vector<Features_n_label> &value);
};

#endif // IFEATURES_N_LABEL_H
