#ifndef IFEATURE_MATRIX_H
#define IFEATURE_MATRIX_H
//#include "main_cppheaders.h"
#include <experimental/any>
#include "main_eigen.h"
#include "utils.h"

class Ifeature_matrix
{
    std::string name;  //element_numeric_type; // todo is this usefull or needed to denote float or double?
    // Ifeature_matrix  feature_matrix;
    virtual void pure_virtural_sentinel()=0;
public:
    Ifeature_matrix() {}
    ~Ifeature_matrix() {}

    Ifeature_matrix(Ifeature_matrix const&)=delete;                 // guideline_TCPL ss3.3.4 else typically a disaster.
    Ifeature_matrix(Ifeature_matrix &&)=delete;                     // guideline_TCPL ss3.3.4 else typically a disaster.
    Ifeature_matrix& operator=(Ifeature_matrix const&)=delete;      // guideline_TCPL ss3.3.4 else typically a disaster.
    Ifeature_matrix& operator=(Ifeature_matrix &&)=delete;          // guideline_TCPL ss3.3.4 else typically a disaster.

    std::string getName() const;
    void setName(const std::string &value);
};

template <typename Num_type>  // todo IMPORTANT double or float
class Ifeature_matrix_num_type : public Ifeature_matrix
{
    std::string name;  //element_numeric_type; // todo is this usefull or needed to denote float or double?
    virtual void pure_virtural_sentinel()=0;
public:
    Ifeature_matrix_num_type() {}
    ~Ifeature_matrix_num_type() {}

//    Ifeature_matrix(Ifeature_matrix const&)=delete;                 // guideline_TCPL ss3.3.4 else typically a disaster.
//    Ifeature_matrix(Ifeature_matrix &&)=delete;                     // guideline_TCPL ss3.3.4 else typically a disaster.
//    Ifeature_matrix& operator=(Ifeature_matrix const&)=delete;      // guideline_TCPL ss3.3.4 else typically a disaster.
//    Ifeature_matrix& operator=(Ifeature_matrix &&)=delete;          // guideline_TCPL ss3.3.4 else typically a disaster.

};

template <typename Num_type, typename Library_type>
class Ifeature_matrix_num_type_eigen : public Ifeature_matrix_num_type<Num_type>
{
    std::string name;  //element_numeric_type; // todo is this usefull or needed to denote float or double?
//    ifdef (Library_type == eigen)  // todo IMPORTANT
//        Eigen::MatrixXd  feature_matrix;
//    ifdef (Library_type == tfl)   // todo IMPORTANT
//        TFL::MatrixXf  feature_matrix;
public:
    Ifeature_matrix_num_type_eigen() {}
    ~Ifeature_matrix_num_type_eigen() {}

    std::experimental::any get_feature_matix() {
    }


};

#endif // IFEATURE_MATRIX_H


// create_feature_matrix_num_type_eigen<double,eigen>();
